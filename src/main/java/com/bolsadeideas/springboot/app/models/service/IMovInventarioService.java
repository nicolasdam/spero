package com.bolsadeideas.springboot.app.models.service;

import java.util.Date;
import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.EstadoMov;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.Proveedor;

public interface IMovInventarioService {

	public List<MovInventario> findAll();

	public void guardar(MovInventario movInventario);

	public MovInventario buscarUno(Long id);

	public void eliminar(Long id);
	
	public List<MovInventario> findMovBodProTipoFec(Bodega bodega, Proveedor proveedor, String tipoMov, Date fechaIni, Date fechaFin);
	
	public List<MovInventario> findMovBodProFec(Bodega bodega, Proveedor proveedor, Date fechaIni, Date fechaFin);
	
	public List<MovInventario> findMovBodTipoFec(Bodega bodega, String tipoMov, Date fechaIni, Date fechaFin);
	
	public List<MovInventario> findMovBodFec(Bodega bodega, Date fechaIni, Date fechaFin);
	
	public List<MovInventario> findEstado(EstadoMov estadoMov, String tipoMov);
	
	public List<MovInventario> findMovEstBod(EstadoMov estadoMov, Bodega bodega, String tipoMov);
	
	public List<MovInventario> findMovEstBodRec(EstadoMov estadoMov, Bodega bodega, String tipoMov);

}

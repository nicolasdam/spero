package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "consecutivos")
public class Consecutivo implements Serializable {

	@Id
	@Column(name = "consecutivo_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long consecutivoId;

	@NotEmpty
	private String tipo_doc;

	@NotEmpty
	private String prefijo;

	@NotNull
	private Long consecutivo;

	public Long getConsecutivoId() {
		return consecutivoId;
	}

	public void setConsecutivoId(Long consecutivoId) {
		this.consecutivoId = consecutivoId;
	}

	public String getTipo_doc() {
		return tipo_doc;
	}

	public void setTipo_doc(String tipo_doc) {
		this.tipo_doc = tipo_doc;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	public Long getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(Long consecutivo) {
		this.consecutivo = consecutivo;
	}

	private static final long serialVersionUID = 1L;

}

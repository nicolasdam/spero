package com.bolsadeideas.springboot.app.models.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IDatosEmpresaDao;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;

@Service
public class DatosEmpresaServiceImpl implements IDatosEmpresaService{

	@Autowired
	IDatosEmpresaDao datosEmpresaDao;
	
	@Override
	public void guardar(DatosEmpresa datosEmpresa) {
		datosEmpresaDao.save(datosEmpresa);
	}

	@Override
	public DatosEmpresa buscarUno(Long id) {
		return datosEmpresaDao.findById(id).orElse(null);
	}

}

package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IOrdenCompraDao;
import com.bolsadeideas.springboot.app.models.entity.OrdenCompra;
import com.bolsadeideas.springboot.app.models.service.IOrdenCompraService;

@Service
public class OrdenCompraServiceImpl implements IOrdenCompraService{

	@Autowired
	IOrdenCompraDao ordenCompraDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<OrdenCompra> findAll() {
		return (List<OrdenCompra>) ordenCompraDao.findAll();
		//return (List<OrdenCompra>) ordenCompraDao.findByordenId():
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<OrdenCompra> findAllOrden() {
		return (List<OrdenCompra>) ordenCompraDao.findOrdenDesc();
	}

	@Override
	@Transactional
	public void guardar(OrdenCompra ordenCompra) {
		ordenCompraDao.save(ordenCompra);
	}

	@Override
	@Transactional(readOnly = true)
	public OrdenCompra buscarUno(Long id) {
		return ordenCompraDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		ordenCompraDao.deleteById(id);
	}

}

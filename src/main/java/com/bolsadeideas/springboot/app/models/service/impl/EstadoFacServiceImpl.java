package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IEstadoFacDao;
import com.bolsadeideas.springboot.app.models.entity.EstadoFac;
import com.bolsadeideas.springboot.app.models.service.IEstadoFacService;

@Service
public class EstadoFacServiceImpl implements IEstadoFacService{

	@Autowired
	IEstadoFacDao estadoFacDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<EstadoFac> findAll() {
		return (List<EstadoFac>) estadoFacDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(EstadoFac estadoFac) {
		estadoFacDao.save(estadoFac);
	}

	@Override
	@Transactional(readOnly = true)
	public EstadoFac buscarUno(Long id) {
		return estadoFacDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		estadoFacDao.deleteById(id);
	}

}

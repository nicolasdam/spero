package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.EstadoFac;

public interface IEstadoFacDao extends CrudRepository<EstadoFac, Long>{

}

package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IBodegaDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;

@Service
public class BodegaServiceImpl implements IBodegaService{

	@Autowired
	IBodegaDao bodegaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Bodega> findAll() {
		return (List<Bodega>) bodegaDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Bodega bodega) {
		bodegaDao.save(bodega);
	}

	@Override
	@Transactional(readOnly = true)
	public Bodega buscarUno(Long id) {
		return bodegaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		bodegaDao.deleteById(id);
	}

	@Override
	public List<Bodega> findAllOrder() {
		return bodegaDao.findAllOrder();
	}

}

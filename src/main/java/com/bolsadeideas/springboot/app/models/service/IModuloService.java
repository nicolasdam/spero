package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Modulo;

public interface IModuloService {

	public List<Modulo> findAll();

	public void guardar(Modulo modulo);

	public Modulo buscarUno(Long id);

	public void eliminar(Long id);

}

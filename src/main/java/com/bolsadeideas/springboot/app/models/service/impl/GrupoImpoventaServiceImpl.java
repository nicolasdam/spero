package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IGrupoImpoventaDao;
import com.bolsadeideas.springboot.app.models.entity.GrupoImpoventa;
import com.bolsadeideas.springboot.app.models.service.IGrupoImporventaService;

@Service
public class GrupoImpoventaServiceImpl implements IGrupoImporventaService{
	
	@Autowired
	IGrupoImpoventaDao grupoImpoventaDao;

	@Override
	@Transactional(readOnly = true)
	public List<GrupoImpoventa> findAll() {
		return (List<GrupoImpoventa>) grupoImpoventaDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(GrupoImpoventa grupoImpoventa) {
		grupoImpoventaDao.save(grupoImpoventa);
	}

	@Override
	@Transactional(readOnly = true)
	public GrupoImpoventa buscarUno(Long id) {
		return grupoImpoventaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		grupoImpoventaDao.deleteById(id);
	}
	
	

}

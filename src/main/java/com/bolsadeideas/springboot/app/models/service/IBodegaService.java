package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Bodega;

public interface IBodegaService {
	
	public List<Bodega> findAll();

	public void guardar(Bodega bodega);

	public Bodega buscarUno(Long id);

	public void eliminar(Long id);
	
	public List<Bodega> findAllOrder();

}

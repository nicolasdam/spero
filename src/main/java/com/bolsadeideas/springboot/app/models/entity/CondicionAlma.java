package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "condicion_almacena")
public class CondicionAlma implements Serializable {

	@Id
	@Column(name = "con_almacena_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long conAlmacenaId;

	@NotEmpty
	private String descripcion;

	private static final long serialVersionUID = 1L;

	public Long getConAlmacenaId() {
		return conAlmacenaId;
	}

	public void setConAlmacenaId(Long conAlmacenaId) {
		this.conAlmacenaId = conAlmacenaId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}

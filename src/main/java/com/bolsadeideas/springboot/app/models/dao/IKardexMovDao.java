package com.bolsadeideas.springboot.app.models.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.KardexMov;
import com.bolsadeideas.springboot.app.models.entity.Maestra;

public interface IKardexMovDao extends CrudRepository<KardexMov, Long>{

	@Query("select km "
			+ "from KardexMov km "
			+ "where bodegaId = ?1 "
			+ "and km.maestra = ?2 "
			+ "and km.fecha between ?3 and ?4 "
			+ "order by km.kardexId")
	public List<KardexMov> findKardexByMaestra(Bodega bodega, Maestra maestra, Date fechaIni, Date fechaFin);
}

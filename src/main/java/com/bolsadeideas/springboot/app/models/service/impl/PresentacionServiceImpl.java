package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IPresentacionDao;
import com.bolsadeideas.springboot.app.models.entity.Presentacion;
import com.bolsadeideas.springboot.app.models.service.IPresentacionService;

@Service
public class PresentacionServiceImpl implements IPresentacionService{

	@Autowired
	IPresentacionDao presentacionDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Presentacion> findAll() {
		return (List<Presentacion>) presentacionDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Presentacion presentacion) {
		presentacionDao.save(presentacion);
	}

	@Override
	@Transactional(readOnly = true)
	public Presentacion buscarUno(Long id) {
		return presentacionDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		presentacionDao.deleteById(id);
	}

}

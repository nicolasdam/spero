package com.bolsadeideas.springboot.app.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class SaldosInventarioRep {

	@Id
	@Column(name = "saldo_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long saldoId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	private Bodega bodega;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	private Maestra maestra;

	private Boolean todos;

	public Long getSaldoId() {
		return saldoId;
	}

	public void setSaldoId(Long saldoId) {
		this.saldoId = saldoId;
	}

	public Bodega getBodega() {
		return bodega;
	}

	public void setBodega(Bodega bodega) {
		this.bodega = bodega;
	}

	public Maestra getMaestra() {
		return maestra;
	}

	public void setMaestra(Maestra maestra) {
		this.maestra = maestra;
	}

	public Boolean getTodos() {
		return todos;
	}

	public void setTodos(Boolean todos) {
		this.todos = todos;
	}

}

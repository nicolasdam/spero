package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.ISaldoBodegaDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.SaldoBodega;
import com.bolsadeideas.springboot.app.models.service.ISaldoBodegaService;

@Service
public class SaldoBodegaServiceImpl implements ISaldoBodegaService {

	@Autowired
	ISaldoBodegaDao saldoBodegaDao;

	@Override
	public List<SaldoBodega> findAll() {
		return (List<SaldoBodega>) saldoBodegaDao.findAll();
	}

	@Override
	public void guardar(SaldoBodega saldoBodega) {
		saldoBodegaDao.save(saldoBodega);
	}

	@Override
	public SaldoBodega buscarUno(Long id) {
		return saldoBodegaDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		saldoBodegaDao.deleteById(id);
	}

	@Override
	public SaldoBodega saldoBodegaMaestra(Bodega bodega, Maestra maestra) {
		return saldoBodegaDao.saldoBodegaMaestra(bodega, maestra);
	}

	@Override
	public List<SaldoBodega> saldoBodega(Bodega bodega) {
		return (List<SaldoBodega>) saldoBodegaDao.saldoBodega(bodega);
	}

	@Override
	public List<SaldoBodega> saldoMaestra(Maestra maestra) {
		return (List<SaldoBodega>) saldoBodegaDao.saldoMaestra(maestra);
	}

	@Override
	public List<SaldoBodega> saldoTotal() {
		return (List<SaldoBodega>) saldoBodegaDao.saldoTotal();
	}

}

package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.TipoMovimiento;

public interface ITipoMovService {
	
	public List<TipoMovimiento> findAll();
	
	public List<TipoMovimiento> buscarPorTipo(String tipo);

	public void guardar(TipoMovimiento tipoMovimiento);

	public TipoMovimiento buscarUno(Long id);

	public void eliminar(Long id);


}

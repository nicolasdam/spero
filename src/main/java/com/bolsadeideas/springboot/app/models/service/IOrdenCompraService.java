package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.OrdenCompra;

public interface IOrdenCompraService {
	
	public List<OrdenCompra> findAll();
	
	public List<OrdenCompra> findAllOrden();

	public void guardar(OrdenCompra ordenCompra);

	public OrdenCompra buscarUno(Long id);

	public void eliminar(Long id);

}

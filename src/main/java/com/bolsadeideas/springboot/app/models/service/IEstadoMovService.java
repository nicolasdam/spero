package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.EstadoMov;

public interface IEstadoMovService {
	
	public List<EstadoMov> findAll();

	public void guardar(EstadoMov estadoMov);

	public EstadoMov buscarUno(Long id);

	public void eliminar(Long id);

}

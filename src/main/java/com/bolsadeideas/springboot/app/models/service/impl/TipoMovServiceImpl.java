package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.ITipoMovDao;
import com.bolsadeideas.springboot.app.models.entity.TipoMovimiento;
import com.bolsadeideas.springboot.app.models.service.ITipoMovService;

@Service
public class TipoMovServiceImpl implements ITipoMovService{

	@Autowired
	ITipoMovDao tipoMovDao;
	
	@Override
	public List<TipoMovimiento> findAll() {
		return (List<TipoMovimiento>) tipoMovDao.findAll();
	}

	@Override
	public List<TipoMovimiento> buscarPorTipo(String tipo) {
		return (List<TipoMovimiento>) tipoMovDao.buscarPorTipo(tipo);
	}

	@Override
	public void guardar(TipoMovimiento tipoMovimiento) {
		tipoMovDao.save(tipoMovimiento);
		
	}

	@Override
	public TipoMovimiento buscarUno(Long id) {
		return tipoMovDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		tipoMovDao.deleteById(id);
	}

}

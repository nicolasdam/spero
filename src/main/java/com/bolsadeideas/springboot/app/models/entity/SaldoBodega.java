package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "saldos_bodega")
public class SaldoBodega implements Serializable {

	@Id
	@Column(name = "saldo_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long saldoId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bodega_id")
	private Bodega bodegaId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "maestra_id")
	private Maestra maestra;

	@NotNull
	private Double cantidad;

	@NotNull
	private Date fecha;

	public Long getSaldoId() {
		return saldoId;
	}

	public void setSaldoId(Long saldoId) {
		this.saldoId = saldoId;
	}

	public Bodega getBodegaId() {
		return bodegaId;
	}

	public void setBodegaId(Bodega bodegaId) {
		this.bodegaId = bodegaId;
	}

	public Maestra getMaestra() {
		return maestra;
	}

	public void setMaestra(Maestra maestra) {
		this.maestra = maestra;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public String costoTotal() {
		Double total = getCantidad() * getMaestra().getCostoPromedio();
		
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(total);
	}
	
	public String getCantidadFormat() {
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(cantidad);
	}

	private static final long serialVersionUID = 1L;

}

package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "modulos")
public class Modulo implements Serializable {

	@Id
	@Column(name = "modulo_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long moduloId;

	@NotEmpty
	private String descripcion;

	@NotEmpty
	private String icono;

	public Long getModuloId() {
		return moduloId;
	}

	public void setModuloId(Long moduloId) {
		this.moduloId = moduloId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	private static final long serialVersionUID = 1L;

}

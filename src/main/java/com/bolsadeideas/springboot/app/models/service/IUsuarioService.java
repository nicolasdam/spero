package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Usuario;

public interface IUsuarioService {
	
	public List<Usuario> findAll();

	public void guardar(Usuario usuario);

	public Usuario buscarUno(Long id);

	public void eliminar(Long id);
	
	public Usuario findByUsuario(String usuarioSis);
	
	public Bodega findByUsuarioBod(String usuarioSis);

}

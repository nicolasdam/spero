package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Presentacion;

public interface IPresentacionService {
	
	public List<Presentacion> findAll();

	public void guardar(Presentacion presentacion);

	public Presentacion buscarUno(Long id);

	public void eliminar(Long id);

}

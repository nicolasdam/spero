package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Fabricante;

public interface IFabricanteService {
	
	public List<Fabricante> findAll();

	public void guardar(Fabricante fabricante);

	public Fabricante buscarUno(Long id);

	public void eliminar(Long id);
}

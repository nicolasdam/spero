package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "unidades_medida")
public class UnidadMedida implements Serializable {

	@Id
	@Column(name = "unidad_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long unidadId;

	@NotEmpty
	private String descripcion;

	private static final long serialVersionUID = 1L;

	public Long getUnidadId() {
		return unidadId;
	}

	public void setUnidadId(Long unidadId) {
		this.unidadId = unidadId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}

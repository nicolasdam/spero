package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "maestra_productos")
public class MaestrasLista implements Serializable {

	@Id
	@Column(name = "maestra_id")
	private Long maestraId;

	@NotEmpty
	private String codigo;

	@NotEmpty
	private String nombre;

	@NotEmpty
	@Column(name = "codigo_barras")
	private String codigoBarras;

	@Column(name = "costo_promedio")
	private Double costoPromedio;
	
	@Column(name = "precio_venta")
	private Double precioVenta;

	private static final long serialVersionUID = 1L;

	public Long getMaestraId() {
		return maestraId;
	}

	public void setMaestraId(Long maestraId) {
		this.maestraId = maestraId;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public Double getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(Double costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}


}

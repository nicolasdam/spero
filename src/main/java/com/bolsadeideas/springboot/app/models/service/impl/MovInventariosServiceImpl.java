package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IMovInventarioDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.EstadoMov;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.Proveedor;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;

@Service
public class MovInventariosServiceImpl implements IMovInventarioService{

	@Autowired
	IMovInventarioDao movInventarioDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<MovInventario> findAll() {
		return (List<MovInventario>) movInventarioDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(MovInventario movInventario) {
		movInventarioDao.save(movInventario);
	}

	@Override
	@Transactional(readOnly = true)
	public MovInventario buscarUno(Long id) {
		return movInventarioDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		movInventarioDao.deleteById(id);
	}

	@Override
	public List<MovInventario> findMovBodProTipoFec(Bodega bodega, Proveedor proveedor, String tipoMov, Date fechaIni,
			Date fechaFin) {
		return (List<MovInventario>) movInventarioDao.findMovBodProTipoFec(bodega, proveedor, tipoMov, fechaIni, fechaFin);
	}

	@Override
	public List<MovInventario> findMovBodProFec(Bodega bodega, Proveedor proveedor, Date fechaIni, Date fechaFin) {
		return (List<MovInventario>) movInventarioDao.findMovBodProFec(bodega, proveedor, fechaIni, fechaFin);
	}

	@Override
	public List<MovInventario> findMovBodTipoFec(Bodega bodega, String tipoMov, Date fechaIni, Date fechaFin) {
		return (List<MovInventario>) movInventarioDao.findMovBodTipoFec(bodega, tipoMov, fechaIni, fechaFin);
	}

	@Override
	public List<MovInventario> findMovBodFec(Bodega bodega, Date fechaIni, Date fechaFin) {
		return (List<MovInventario>) movInventarioDao.findMovBodFec(bodega, fechaIni, fechaFin);
	}

	@Override
	public List<MovInventario> findEstado(EstadoMov estadoMov, String tipoMov) {
		return (List<MovInventario>) movInventarioDao.findEstado(estadoMov, tipoMov);
	}

	@Override
	public List<MovInventario> findMovEstBod(EstadoMov estadoMov, Bodega bodega, String tipoMov) {
		return (List<MovInventario>) movInventarioDao.findMovEstBod(estadoMov, bodega, tipoMov);
	}

	@Override
	public List<MovInventario> findMovEstBodRec(EstadoMov estadoMov, Bodega bodega, String tipoMov) {
		return (List<MovInventario>) movInventarioDao.findMovEstBodRec(estadoMov, bodega, tipoMov);
	}

	
	
}

package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Nivel;
import com.bolsadeideas.springboot.app.models.entity.Opcion;
import com.bolsadeideas.springboot.app.models.entity.OpcionNivel;

public interface IOpcionNivelDao extends CrudRepository<OpcionNivel, Long> {
	
	@Query("select op from OpcionNivel op where op.nivel = ?1 order by op.opcion.modulo.moduloId, op.opcion.ordenMenu, op.opcion.nombreOpcion")
	public List<OpcionNivel> findByNivel(Nivel nivelUsu);
	
	@Query("select op from OpcionNivel op where op.nivel = ?1 and op.opcion = ?2")
	public OpcionNivel findByOPNivel(Nivel nivelUsu, Opcion opcion);

}

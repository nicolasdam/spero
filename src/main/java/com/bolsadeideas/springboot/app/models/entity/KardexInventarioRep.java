package com.bolsadeideas.springboot.app.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class KardexInventarioRep {

	@Id
	@Column(name = "kardex_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long kardexId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	private Bodega bodega;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	private Maestra maestra;

	@NotNull
	private String fechaInicial;

	@NotNull
	private String fechaFinal;

	public Long getKardexId() {
		return kardexId;
	}

	public void setKardexId(Long kardexId) {
		this.kardexId = kardexId;
	}

	public Bodega getBodega() {
		return bodega;
	}

	public void setBodega(Bodega bodega) {
		this.bodega = bodega;
	}

	public Maestra getMaestra() {
		return maestra;
	}

	public void setMaestra(Maestra maestra) {
		this.maestra = maestra;
	}

	public String getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

}

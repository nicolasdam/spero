package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IModuloDao;
import com.bolsadeideas.springboot.app.models.entity.Modulo;
import com.bolsadeideas.springboot.app.models.service.IModuloService;

@Service
public class ModuloServiceImpl implements IModuloService{
	
	@Autowired
	private IModuloDao moduloDao;

	@Override
	@Transactional(readOnly = true)
	public List<Modulo> findAll() {
		// TODO Auto-generated method stub
		return (List<Modulo>) moduloDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Modulo modulo) {
		moduloDao.save(modulo);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Modulo buscarUno(Long id) {
		return moduloDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		moduloDao.deleteById(id);
		
	}
	

}

package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IOpcionNivelDao;
import com.bolsadeideas.springboot.app.models.entity.Nivel;
import com.bolsadeideas.springboot.app.models.entity.Opcion;
import com.bolsadeideas.springboot.app.models.entity.OpcionNivel;
import com.bolsadeideas.springboot.app.models.service.IOpcionNivelService;

@Service
public class OpcionNivelServiceImpl implements IOpcionNivelService{
	
	@Autowired
	private IOpcionNivelDao opcionNivelDao;

	@Override
	public List<OpcionNivel> findAll() {
		// TODO Auto-generated method stub
		return (List<OpcionNivel>) opcionNivelDao.findAll();
	}

	@Override
	public List<OpcionNivel> findByNivel(Nivel nivel) {
		// TODO Auto-generated method stub
		return (List<OpcionNivel>) opcionNivelDao.findByNivel(nivel);
	}

	@Override
	public void guardar(OpcionNivel opcionNivel) {
		// TODO Auto-generated method stub
		opcionNivelDao.save(opcionNivel);
	}

	@Override
	public OpcionNivel buscarUno(Long id) {
		// TODO Auto-generated method stub
		return opcionNivelDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		// TODO Auto-generated method stub
		opcionNivelDao.deleteById(id);
	}

	@Override
	public OpcionNivel findOpNivel(Nivel nivel, Opcion opcion) {
		return opcionNivelDao.findByOPNivel(nivel, opcion);
	}

}

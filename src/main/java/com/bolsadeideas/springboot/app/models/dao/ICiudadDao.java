package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Ciudad;

public interface ICiudadDao extends CrudRepository<Ciudad, Long>{
	

}

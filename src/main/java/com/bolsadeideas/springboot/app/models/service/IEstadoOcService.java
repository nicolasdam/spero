package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.EstadoOc;

public interface IEstadoOcService {
	
	public List<EstadoOc> findAll();

	public void guardar(EstadoOc estadoOc);

	public EstadoOc buscarUno(Long id);

	public void eliminar(Long id);

}

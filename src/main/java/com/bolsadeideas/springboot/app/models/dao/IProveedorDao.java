package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Proveedor;

public interface IProveedorDao extends CrudRepository<Proveedor, Long>{

}

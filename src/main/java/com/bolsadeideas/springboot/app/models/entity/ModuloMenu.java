package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class ModuloMenu implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modulo_id")
	private Modulo modulo;
	
	@NotNull
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "opcion_id")
	private List<Opcion> opcionItems;

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}
	
	public ModuloMenu() {
		this.opcionItems = new ArrayList<Opcion>();
	}
	
	public List<Opcion> getOpcionItems() {
		return opcionItems;
	}

	public void setOpcionItems(List<Opcion> opcionItems) {
		this.opcionItems = opcionItems;
	}

	public void addItemMov(Opcion opcion) {
		this.opcionItems.add(opcion);
	}

}

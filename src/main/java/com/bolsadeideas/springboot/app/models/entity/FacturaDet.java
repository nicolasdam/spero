package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "facturas_det")
public class FacturaDet implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "detalle_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long detalleId;

	@Column(name = "factura_id")
	private Long facturaId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "maestra_id")
	private Maestra maestra;

	@NotNull
	private Double precio;

	@NotNull
	private Double cantidad;
	
	public Double totalItem() {
		return cantidad.doubleValue() * precio;
	}

	public String totalItemForm() {
		DecimalFormat formateador = new DecimalFormat("###,###,###.##");
		return formateador.format(cantidad.doubleValue() * precio);
	}

	public String precioForm() {
		DecimalFormat formateador = new DecimalFormat("###,###,###.##");
		return formateador.format(precio);
	}

	public Long getDetalleId() {
		return detalleId;
	}

	public void setDetalleId(Long detalleId) {
		this.detalleId = detalleId;
	}

	public Long getFacturaId() {
		return facturaId;
	}

	public void setFacturaId(Long facturaId) {
		this.facturaId = facturaId;
	}

	public Maestra getMaestra() {
		return maestra;
	}

	public void setMaestra(Maestra maestra) {
		this.maestra = maestra;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

}

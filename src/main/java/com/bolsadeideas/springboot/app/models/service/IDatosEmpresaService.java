package com.bolsadeideas.springboot.app.models.service;

import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;

public interface IDatosEmpresaService {

	public void guardar(DatosEmpresa datosEmpresa);

	public DatosEmpresa buscarUno(Long id);
	
}

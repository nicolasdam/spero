package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.ICondicionAlmaDao;
import com.bolsadeideas.springboot.app.models.entity.CondicionAlma;
import com.bolsadeideas.springboot.app.models.service.ICondicionAlmaService;

@Service
public class CondicionAlmaServiceImpl implements ICondicionAlmaService{
	
	@Autowired
	ICondicionAlmaDao condicionAlmaDao;

	@Override
	@Transactional(readOnly = true)
	public List<CondicionAlma> findAll() {
		return (List<CondicionAlma>) condicionAlmaDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(CondicionAlma condicionAlma) {
		condicionAlmaDao.save(condicionAlma);
	}

	@Override
	@Transactional(readOnly = true)
	public CondicionAlma buscarUno(Long id) {
		return condicionAlmaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		condicionAlmaDao.deleteById(id);
	}

}

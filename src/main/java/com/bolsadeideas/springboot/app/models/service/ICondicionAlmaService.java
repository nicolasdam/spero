package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.CondicionAlma;

public interface ICondicionAlmaService {
	
	public List<CondicionAlma> findAll();

	public void guardar(CondicionAlma condicionAlma);

	public CondicionAlma buscarUno(Long id);

	public void eliminar(Long id);

}

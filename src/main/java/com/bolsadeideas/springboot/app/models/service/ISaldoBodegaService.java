package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.SaldoBodega;

public interface ISaldoBodegaService {
	
	public List<SaldoBodega> findAll();

	public void guardar(SaldoBodega saldoBodega);

	public SaldoBodega buscarUno(Long id);

	public void eliminar(Long id);
	
	public SaldoBodega saldoBodegaMaestra(Bodega bodega, Maestra maestra);
	
	public List<SaldoBodega> saldoBodega(Bodega bodega);
	
	public List<SaldoBodega> saldoMaestra(Maestra maestra);
	
	public List<SaldoBodega> saldoTotal();

}

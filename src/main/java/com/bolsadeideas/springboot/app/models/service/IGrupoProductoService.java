package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.GrupoProducto;

public interface IGrupoProductoService {

	public List<GrupoProducto> findAll();

	public void guardar(GrupoProducto grupoProducto);

	public GrupoProducto buscarUno(Long id);

	public void eliminar(Long id);
}

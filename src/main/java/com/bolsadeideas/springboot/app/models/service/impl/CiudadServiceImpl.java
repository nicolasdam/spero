package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.ICiudadDao;
import com.bolsadeideas.springboot.app.models.entity.Ciudad;
import com.bolsadeideas.springboot.app.models.service.ICiudadService;

@Service
public class CiudadServiceImpl implements ICiudadService{

	@Autowired
	ICiudadDao ciudadDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Ciudad> findAll() {
		return (List<Ciudad>) ciudadDao.findAll();
	}
	
	@Override
	@Transactional
	public void guardar(Ciudad ciudad) {
		ciudadDao.save(ciudad);
	}

	@Override
	@Transactional(readOnly = true)
	public Ciudad buscarUno(Long id) {
		return ciudadDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		ciudadDao.deleteById(id);
	}
	
}

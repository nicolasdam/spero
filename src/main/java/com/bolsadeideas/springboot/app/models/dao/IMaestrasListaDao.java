package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.MaestrasLista;

public interface IMaestrasListaDao extends CrudRepository<MaestrasLista, Long>{
	
	@Query("select lm from MaestrasLista lm where lm.nombre like %?1% or lm.codigoBarras like %?1%")
	public List<MaestrasLista> buscarPorNombreMaestra(String term);
	
	public List<MaestrasLista> findByNombreLikeIgnoreCase(String term);

}

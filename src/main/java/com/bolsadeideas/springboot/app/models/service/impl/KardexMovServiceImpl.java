package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IKardexMovDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.FacturaDet;
import com.bolsadeideas.springboot.app.models.entity.KardexMov;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioDet;
import com.bolsadeideas.springboot.app.models.entity.SaldoBodega;
import com.bolsadeideas.springboot.app.models.entity.TipoMovimiento;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IKardexMovService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;
import com.bolsadeideas.springboot.app.models.service.ISaldoBodegaService;
import com.bolsadeideas.springboot.app.models.service.ITipoMovService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Service
public class KardexMovServiceImpl implements IKardexMovService {

	@Autowired
	IKardexMovDao kardexMovDao;

	@Autowired
	IMovInventarioService movInventarioService;
	
	@Autowired
	ITipoMovService tipoMovService;

	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	ISaldoBodegaService saldoBodegaService;
	
	String entrada = "E";
	String salida = "S";
	String traslado = "T";

	@Override
	public List<KardexMov> findAll() {
		return (List<KardexMov>) kardexMovDao.findAll();
	}

	@Override
	public void guardar(KardexMov kardexMov) {
		kardexMovDao.save(kardexMov);
	}

	@Override
	public KardexMov buscarUno(Long id) {
		return kardexMovDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		kardexMovDao.deleteById(id);
	}

	@Override
	public List<KardexMov> findKardexByMaestra(Bodega bodega, Maestra maestra, Date fechaIni, Date fechaFin) {
		return kardexMovDao.findKardexByMaestra(bodega, maestra, fechaIni, fechaFin);
	}

	@Override
	public Boolean guardarKardexMov(MovInventario movInventario) {
		
		Date fechaAct = new Date();
		Long bodOrigen = movInventario.getBodegaOrigen().getBodegaId();
		Long bodDestino = null;
		Bodega bodegaOrigen = bodegaService.buscarUno(bodOrigen);
		String tipoMov = movInventario.getTipoMovimiento().getTipoMov();
		Long estadoMov = movInventario.getEstadoMov().getEstadoId();
		Bodega bodegaDestino = new Bodega();
		
		if(tipoMov.equals(traslado)) {
			bodDestino = movInventario.getBodegaDestino().getBodegaId();
			bodegaDestino = bodegaService.buscarUno(bodDestino);
		}

		try {

			for (MovInventarioDet movInventarioDet : movInventario.getItems()) {
				KardexMov kardexMov = new KardexMov();
				Bodega bodegaKardex = new Bodega();
				
				Double saldoNuevo = null;
				Double saldoAnteriorBod = null;
				Double saldoNuevoBod = null;
				Double nuevoCosto = null;
				String tipoMovimiento = null;
				Double cantidadMov = movInventarioDet.getCantidad();
				Double costoMov = movInventarioDet.getValorCosto();
				

				kardexMov.setMovimientoId(movInventario);
				kardexMov.setTipoMovimiento(movInventario.getTipoMovimiento());
				kardexMov.setDocumento(movInventario.getNumero());
				kardexMov.setMaestra(movInventarioDet.getMaestra());
				kardexMov.setUsuario(movInventario.getUsuario());

				Long maestraId = movInventarioDet.getMaestra().getMaestraId();
				Maestra maestra = maestraService.buscarUno(maestraId);
				Double saldoActual = maestra.getSaldoActual();
				Double costoPromedio = maestra.getCostoPromedio();
				
				if(tipoMov.equals(entrada)) {
					saldoNuevo = saldoActual + cantidadMov;
					Double costoTotal = (saldoActual * costoPromedio) + (cantidadMov * costoMov);
					nuevoCosto = costoTotal / saldoNuevo;
					
				}else if(tipoMov.equals(salida)) {
					saldoNuevo = saldoActual - cantidadMov;
					nuevoCosto = costoMov;
				}if(tipoMov.equals(traslado)) {
					nuevoCosto = costoMov;
					if(estadoMov == 1) {
						saldoNuevo = saldoActual - cantidadMov;
					}if(estadoMov == 3) {
						saldoNuevo = saldoActual + cantidadMov;
						cantidadMov = movInventarioDet.getCantidadRec();
					}
				}
				
				maestra.setSaldoActual(saldoNuevo);
				maestra.setCostoPromedio(nuevoCosto);
				maestra.setCostoUltima(costoMov);
				
				maestraService.guardar(maestra);
				
				kardexMov.setCostoPromedio(nuevoCosto);
				kardexMov.setCantidad(cantidadMov);
				
				SaldoBodega saldoBodega = new SaldoBodega();
				
				if(tipoMov.equals(traslado) && estadoMov == 3) {
					saldoBodega = saldoBodegaService.saldoBodegaMaestra(bodegaDestino, maestra);
					tipoMovimiento = entrada;
					bodegaKardex = movInventario.getBodegaDestino();
					if(saldoBodega == null) {
						saldoAnteriorBod = 0.0;
						saldoNuevoBod = cantidadMov;
						
						SaldoBodega saldoBodegaNew = new SaldoBodega();
						saldoBodegaNew.setBodegaId(bodegaDestino);
						saldoBodegaNew.setMaestra(maestra);
						saldoBodegaNew.setCantidad(saldoNuevoBod);
						saldoBodegaNew.setFecha(fechaAct);
						
						saldoBodegaService.guardar(saldoBodegaNew);

					} else {
						saldoAnteriorBod = saldoBodega.getCantidad();
						saldoNuevoBod = saldoAnteriorBod + cantidadMov;
						saldoBodega.setCantidad(saldoNuevoBod);
						saldoBodega.setFecha(fechaAct);
						
						saldoBodegaService.guardar(saldoBodega);
					}
					
				} else {
					saldoBodega = saldoBodegaService.saldoBodegaMaestra(bodegaOrigen, maestra);
					bodegaKardex = movInventario.getBodegaOrigen();
					if(saldoBodega == null) {
						saldoAnteriorBod = 0.0;
						if(tipoMov.equals(entrada)) {
							saldoNuevoBod = cantidadMov;
							tipoMovimiento = entrada;
						}else if(tipoMov.equals(salida) || tipoMov.equals(traslado)) {
							tipoMovimiento = salida;
							saldoNuevoBod = cantidadMov * -1;
						}
						
						SaldoBodega saldoBodegaNew = new SaldoBodega();
						saldoBodegaNew.setBodegaId(bodegaOrigen);
						saldoBodegaNew.setMaestra(maestra);
						saldoBodegaNew.setCantidad(saldoNuevoBod);
						saldoBodegaNew.setFecha(fechaAct);
						
						saldoBodegaService.guardar(saldoBodegaNew);
					} else {
						saldoAnteriorBod = saldoBodega.getCantidad();
						if(tipoMov.equals(entrada)) {
							tipoMovimiento = entrada;
							saldoNuevoBod = saldoAnteriorBod + cantidadMov;
						}else if(tipoMov.equals(salida) || tipoMov.equals(traslado)) {
							tipoMovimiento = salida;
							saldoNuevoBod = saldoAnteriorBod - cantidadMov;
						}
						
						saldoBodega.setCantidad(saldoNuevoBod);
						saldoBodega.setFecha(fechaAct);
						
						saldoBodegaService.guardar(saldoBodega);
					}
				}
				
				kardexMov.setTipoMov(tipoMovimiento);
				kardexMov.setSaldoAnterior(saldoAnteriorBod);
				kardexMov.setSaldoNuevo(saldoNuevoBod);
				kardexMov.setBodegaId(bodegaKardex);
				
				this.guardar(kardexMov);
				
			}
		} catch (Exception e) {
			return false;
		}

		return true;
	}
	
	//Inicio - Kardex Traslados
	@Override
	public Boolean guardarKardexMovtraslados(MovInventario movInventario) {
		
		Date fechaAct = new Date();
		Long bodTransito = (long) 99;
		Bodega bodegaSalida = new Bodega();
		Bodega bodegaEntrada = new Bodega();
		Long estadoMov = movInventario.getEstadoMov().getEstadoId();
		
		if(estadoMov == 1) {
			bodegaSalida = movInventario.getBodegaOrigen();
			bodegaEntrada = bodegaService.buscarUno(bodTransito);
		}if(estadoMov == 3) {
			bodegaSalida = bodegaService.buscarUno(bodTransito);
			bodegaEntrada = movInventario.getBodegaDestino();
		}
		

		try {

			for (MovInventarioDet movInventarioDet : movInventario.getItems()) {
				KardexMov kardexMovEnt = new KardexMov();
				KardexMov kardexMovSal = new KardexMov();
				
				SaldoBodega saldoBodegaEnt = new SaldoBodega();
				SaldoBodega saldoBodegaSal = new SaldoBodega();
				
				Double saldoAnteriorBod = null;
				Double saldoNuevoBod = null;
				Double cantidadMov = null;
				
				Double costoPromedio = movInventarioDet.getMaestra().getCostoPromedio();
				
				saldoBodegaSal = saldoBodegaService.saldoBodegaMaestra(bodegaSalida, movInventarioDet.getMaestra());
				saldoBodegaEnt = saldoBodegaService.saldoBodegaMaestra(bodegaEntrada, movInventarioDet.getMaestra());
				
				if(estadoMov == 1) {
					cantidadMov = movInventarioDet.getCantidad();
				}if(estadoMov == 3) {
					cantidadMov = movInventarioDet.getCantidadRec();
				}
				
				//SALDO BODEGA SALIDA
				if(saldoBodegaSal == null) {
					saldoAnteriorBod = 0.0;
					saldoNuevoBod = cantidadMov;
					
					SaldoBodega saldoBodegaNew = new SaldoBodega();
					saldoBodegaNew.setBodegaId(bodegaSalida);
					saldoBodegaNew.setMaestra(movInventarioDet.getMaestra());
					saldoBodegaNew.setCantidad(saldoNuevoBod);
					saldoBodegaNew.setFecha(fechaAct);
					
					saldoBodegaService.guardar(saldoBodegaNew);

				} else {
					saldoAnteriorBod = saldoBodegaSal.getCantidad();
					saldoNuevoBod = saldoAnteriorBod - cantidadMov;
					saldoBodegaSal.setCantidad(saldoNuevoBod);
					saldoBodegaSal.setFecha(fechaAct);
					
					saldoBodegaService.guardar(saldoBodegaSal);
				}
				
				kardexMovSal.setMovimientoId(movInventario);
				kardexMovSal.setTipoMovimiento(movInventario.getTipoMovimiento());
				kardexMovSal.setDocumento(movInventario.getNumero());
				kardexMovSal.setMaestra(movInventarioDet.getMaestra());
				kardexMovSal.setUsuario(movInventario.getUsuario());
				kardexMovSal.setCostoPromedio(costoPromedio);
				kardexMovSal.setCantidad(cantidadMov);
				kardexMovSal.setTipoMov(salida);
				kardexMovSal.setSaldoAnterior(saldoAnteriorBod);
				kardexMovSal.setSaldoNuevo(saldoNuevoBod);
				kardexMovSal.setBodegaId(bodegaSalida);
				
				//SALDO BODEGA ENTRADA
				if(saldoBodegaEnt == null) {
					saldoAnteriorBod = 0.0;
					saldoNuevoBod = cantidadMov;
					
					SaldoBodega saldoBodegaNew = new SaldoBodega();
					saldoBodegaNew.setBodegaId(bodegaEntrada);
					saldoBodegaNew.setMaestra(movInventarioDet.getMaestra());
					saldoBodegaNew.setCantidad(saldoNuevoBod);
					saldoBodegaNew.setFecha(fechaAct);
					
					saldoBodegaService.guardar(saldoBodegaNew);

				} else {
					saldoAnteriorBod = saldoBodegaEnt.getCantidad();
					saldoNuevoBod = saldoAnteriorBod + cantidadMov;
					saldoBodegaEnt.setCantidad(saldoNuevoBod);
					saldoBodegaEnt.setFecha(fechaAct);
					
					saldoBodegaService.guardar(saldoBodegaEnt);
				}
				
				kardexMovEnt.setMovimientoId(movInventario);
				kardexMovEnt.setTipoMovimiento(movInventario.getTipoMovimiento());
				kardexMovEnt.setDocumento(movInventario.getNumero());
				kardexMovEnt.setMaestra(movInventarioDet.getMaestra());
				kardexMovEnt.setUsuario(movInventario.getUsuario());
				kardexMovEnt.setCostoPromedio(costoPromedio);
				kardexMovEnt.setCantidad(cantidadMov);
				kardexMovEnt.setTipoMov(entrada);
				kardexMovEnt.setSaldoAnterior(saldoAnteriorBod);
				kardexMovEnt.setSaldoNuevo(saldoNuevoBod);
				kardexMovEnt.setBodegaId(bodegaEntrada);
				
				this.guardar(kardexMovSal);
				this.guardar(kardexMovEnt);
				
			}
		} catch (Exception e) {
			return false;
		}

		return true;
	}
	//Fin - Kardex Traslados

	@Override
	public Boolean guardarKardexFac(Factura factura) {
		Date fechaAct = new Date();
		Long bodOrigen = factura.getBodega().getBodegaId();
		Bodega bodegaOrigen = bodegaService.buscarUno(bodOrigen);
		Long lTipoMov = (long) 4;
		TipoMovimiento tipoMov = tipoMovService.buscarUno(lTipoMov);

		try {

			for (FacturaDet facturaDet : factura.getItems()) {
				KardexMov kardexMov = new KardexMov();
				
				Double saldoNuevo = null;
				Double saldoAnteriorBod = null;
				Double saldoNuevoBod = null;
				String tipoMovimiento = salida;
				Double cantidadMov = facturaDet.getCantidad();

				kardexMov.setFacturaId(factura);
				kardexMov.setTipoMovimiento(tipoMov);
				kardexMov.setDocumento(factura.getNumero());
				kardexMov.setMaestra(facturaDet.getMaestra());
				kardexMov.setCantidad(cantidadMov);
				kardexMov.setUsuario(factura.getUsuario());

				Long maestraId = facturaDet.getMaestra().getMaestraId();
				Maestra maestra = maestraService.buscarUno(maestraId);
				Double saldoActual = maestra.getSaldoActual();
				Double costoPromedio = maestra.getCostoPromedio();
				
				saldoNuevo = saldoActual - cantidadMov;
				
				maestra.setSaldoActual(saldoNuevo);
				
				maestraService.guardar(maestra);
				
				kardexMov.setCostoPromedio(costoPromedio);
				
				SaldoBodega saldoBodega = new SaldoBodega();
				saldoBodega = saldoBodegaService.saldoBodegaMaestra(bodegaOrigen, maestra);
				if(saldoBodega == null) {
					saldoAnteriorBod = 0.0;

					tipoMovimiento = salida;
					saldoNuevoBod = cantidadMov * -1;
					
					SaldoBodega saldoBodegaNew = new SaldoBodega();
					saldoBodegaNew.setBodegaId(bodegaOrigen);
					saldoBodegaNew.setMaestra(maestra);
					saldoBodegaNew.setCantidad(saldoNuevoBod);
					saldoBodegaNew.setFecha(fechaAct);
					
					saldoBodegaService.guardar(saldoBodegaNew);
				} else {
					saldoAnteriorBod = saldoBodega.getCantidad();
					saldoNuevoBod = saldoAnteriorBod - cantidadMov;
					saldoBodega.setCantidad(saldoNuevoBod);
					saldoBodega.setFecha(fechaAct);
					
					saldoBodegaService.guardar(saldoBodega);
				}

				
				kardexMov.setTipoMov(tipoMovimiento);
				kardexMov.setSaldoAnterior(saldoAnteriorBod);
				kardexMov.setSaldoNuevo(saldoNuevoBod);
				kardexMov.setBodegaId(bodegaOrigen);
				
				this.guardar(kardexMov);
				
			}
		} catch (Exception e) {
			return false;
		}

		return true;
	}

}

package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.MovInventarioDetNew;

public interface IMovInventarioDetDao extends CrudRepository<MovInventarioDetNew, Long>{

}

package com.bolsadeideas.springboot.app.models.service;

import java.util.Date;
import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.EstadoFac;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.Usuario;

public interface IFacturaService {
	
	public List<Factura> findAll();

	public void guardar(Factura factura);

	public Factura buscarUno(Long id);

	public void eliminar(Long id);
	
	public List<Factura> findEstado(EstadoFac estadoFac);
	
	public List<Factura> findEstadoBod(EstadoFac estadoFac, Bodega bodega);
	
	public List<Factura> findFacFec(Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	public List<Factura> findFacBodFec(Bodega bodega, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	public List<Factura> findFacCliFec(Cliente cliente, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	public List<Factura> findFacBodCliFec(Bodega bodega, Cliente cliente, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	public List<Factura> findFacCliUsuFec(Cliente cliente, Usuario usuario, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	public List<Factura> findFacBodUsuFec(Bodega bodega, Usuario usuario, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	public List<Factura> findFacUsuFec(Usuario usuario, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
}

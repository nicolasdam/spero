package com.bolsadeideas.springboot.app.models.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IConsecutivoDao;
import com.bolsadeideas.springboot.app.models.entity.Consecutivo;
import com.bolsadeideas.springboot.app.models.service.IConsecutivoService;

@Service
public class ConsecutivoServiceImpl implements IConsecutivoService{

	@Autowired
	IConsecutivoDao consecutivoDao;
	
	@Override
	public void guardar(Consecutivo consecutivo) {
		consecutivoDao.save(consecutivo);
	}

	@Override
	public Consecutivo buscarUno(Long id) {
		return consecutivoDao.findById(id).orElse(null);
	}

	@Override
	public String buscarConsecutivo(Long id) {

		Consecutivo consecutivo = new Consecutivo();
		consecutivo = this.buscarUno(id);
		String prefijo = consecutivo.getPrefijo();
		Long numConsecutivo = consecutivo.getConsecutivo();
		String numero = prefijo + '-' + String.format("%05d", numConsecutivo);
		
		numConsecutivo += 1;
		consecutivo.setConsecutivo(numConsecutivo);
		consecutivoDao.save(consecutivo);
		
		return numero;
	}

}

package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.UnidadMedida;

public interface IUnidadMedidaService {
	
	public List<UnidadMedida> findAll();

	public void guardar(UnidadMedida unidadMedida);

	public UnidadMedida buscarUno(Long id);

	public void eliminar(Long id);

}

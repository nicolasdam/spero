package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Modulo;

public interface IModuloDao extends CrudRepository<Modulo, Long> {
	
}

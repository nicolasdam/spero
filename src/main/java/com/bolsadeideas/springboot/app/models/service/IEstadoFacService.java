package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.EstadoFac;

public interface IEstadoFacService {

	public List<EstadoFac> findAll();

	public void guardar(EstadoFac estadoFac);

	public EstadoFac buscarUno(Long id);

	public void eliminar(Long id);
}

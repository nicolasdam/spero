package com.bolsadeideas.springboot.app.models.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.EstadoFac;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.Usuario;

public interface IFacturaDao extends CrudRepository<Factura, Long>{

	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.estadoFac = ?1 "
			+ "order by fa.facturaId desc")
	public List<Factura> findEstado(EstadoFac estadoFac);
	
	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.estadoFac = ?1 "
			+ "and fa.bodega = ?2 "
			+ "order by fa.facturaId desc")
	public List<Factura> findEstadoBod(EstadoFac estadoFac, Bodega bodega);
	
	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.bodega = ?1 "
			+ "and fa.cliente = ?2 "
			+ "and fa.fechaFactura between ?3 and ?4 "
			+ "and fa.estadoFac = ?5 "
			+ "order by fa.bodega, fa.facturaId")
	public List<Factura> findFacBodCliFec(Bodega bodega, Cliente cliente, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.bodega = ?1 "
			+ "and fa.fechaFactura between ?2 and ?3 "
			+ "and fa.estadoFac = ?4 "
			+ "order by fa.facturaId")
	public List<Factura> findFacBodFec(Bodega bodega, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.cliente = ?1 "
			+ "and fa.fechaFactura between ?2 and ?3 "
			+ "and fa.estadoFac = ?4 "
			+ "order by fa.bodega, fa.facturaId")
	public List<Factura> findFacCliFec(Cliente cliente, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.fechaFactura between ?1 and ?2 "
			+ "and fa.estadoFac = ?3 "
			+ "order by fa.bodega, fa.facturaId")
	public List<Factura> findFacFec(Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.cliente = ?1 "
			+ "and fa.usuario = ?2 "
			+ "and fa.fechaFactura between ?3 and ?4 "
			+ "and fa.estadoFac = ?5 "
			+ "order by fa.bodega, fa.facturaId")
	public List<Factura> findFacCliUsuFec(Cliente cliente, Usuario usuario, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.bodega = ?1 "
			+ "and fa.usuario = ?2 "
			+ "and fa.fechaFactura between ?3 and ?4 "
			+ "and fa.estadoFac = ?5 "
			+ "order by fa.bodega, fa.facturaId")
	public List<Factura> findFacBodUsuFec(Bodega bodega, Usuario usuario, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
	@Query("select fa "
			+ "from Factura fa "
			+ "where fa.usuario = ?1 "
			+ "and fa.fechaFactura between ?2 and ?3 "
			+ "and fa.estadoFac = ?4 "
			+ "order by fa.bodega, fa.facturaId")
	public List<Factura> findFacUsuFec(Usuario usuario, Date fechaIni, Date fechaFin, EstadoFac estadoFac);
	
}

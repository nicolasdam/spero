package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IOpcionDao;
import com.bolsadeideas.springboot.app.models.entity.Opcion;
import com.bolsadeideas.springboot.app.models.service.IOpcionService;

@Service
public class OpcionServiceImpl implements IOpcionService {

	@Autowired
	private IOpcionDao opcionDao;

	@Override
	@Transactional(readOnly = true)
	public List<Opcion> findAll() {
		return (List<Opcion>) opcionDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Opcion opcion) {
		opcionDao.save(opcion);
	}

	@Override
	@Transactional(readOnly = true)
	public Opcion buscarUno(Long id) {
		return opcionDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		opcionDao.deleteById(id);
	}

	@Override
	public List<Opcion> findAllOrder() {
		return (List<Opcion>) opcionDao.findAllOrder();
	}

}

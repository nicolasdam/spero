package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IFabricanteDao;
import com.bolsadeideas.springboot.app.models.entity.Fabricante;
import com.bolsadeideas.springboot.app.models.service.IFabricanteService;

@Service
public class FabricanteServiceImpl implements IFabricanteService {

	@Autowired
	IFabricanteDao fabricanteDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Fabricante> findAll() {
		// TODO Auto-generated method stub
		return (List<Fabricante>) fabricanteDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Fabricante fabricante) {
		fabricanteDao.save(fabricante);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Fabricante buscarUno(Long id) {
		return fabricanteDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		fabricanteDao.deleteById(id);
	}

}

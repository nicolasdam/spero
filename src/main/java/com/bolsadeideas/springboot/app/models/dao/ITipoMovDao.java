package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.TipoMovimiento;

public interface ITipoMovDao extends CrudRepository<TipoMovimiento, Long>{
	
	@Query("select tm from TipoMovimiento tm where tm.tipoMov = ?1 order by tm.descripcion")
	public List<TipoMovimiento> buscarPorTipo(String term);
	
}

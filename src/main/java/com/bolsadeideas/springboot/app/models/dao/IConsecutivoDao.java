package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Consecutivo;

public interface IConsecutivoDao extends CrudRepository<Consecutivo, Long>{

}

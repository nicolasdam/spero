package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "grupo_impoventa")
public class GrupoImpoventa implements Serializable {

	@Id
	@Column(name = "grupo_impoventa_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long grupoImpoventaId;

	@NotEmpty
	private String codigo;

	@NotEmpty
	private String nombre;

	@NotNull
	@Column(name = "por_impo")
	private Long porImpo;

	@NotNull
	private Long estado;

	private static final long serialVersionUID = 1L;

	public Long getGrupoImpoventaId() {
		return grupoImpoventaId;
	}

	public void setGrupoImpoventaId(Long grupoImpoventaId) {
		this.grupoImpoventaId = grupoImpoventaId;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getPorImpo() {
		return porImpo;
	}

	public void setPorImpo(Long porImpo) {
		this.porImpo = porImpo;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

}

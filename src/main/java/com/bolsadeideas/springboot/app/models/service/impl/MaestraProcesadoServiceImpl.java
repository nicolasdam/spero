package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IMaestraProcesadoDao;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MaestraProcesado;
import com.bolsadeideas.springboot.app.models.service.IMaestraProcesadoService;

@Service
public class MaestraProcesadoServiceImpl implements IMaestraProcesadoService{

	@Autowired
	IMaestraProcesadoDao maestraProcesadoDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<MaestraProcesado> findAll() {
		return (List<MaestraProcesado>) maestraProcesadoDao.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<MaestraProcesado> buscarPorMaestraId(Maestra id) {
		return (List<MaestraProcesado>) maestraProcesadoDao.buscarPorMaestraId(id);
	}

	@Override
	@Transactional
	public void guardar(MaestraProcesado maestraProcesado) {
		maestraProcesadoDao.save(maestraProcesado);
	}

	@Override
	@Transactional(readOnly = true)
	public MaestraProcesado buscarUno(Long id) {
		return maestraProcesadoDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		maestraProcesadoDao.deleteById(id);
	}

}

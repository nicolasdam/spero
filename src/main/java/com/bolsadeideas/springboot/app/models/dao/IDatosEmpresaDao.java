package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;

public interface IDatosEmpresaDao extends CrudRepository<DatosEmpresa, Long>{

}

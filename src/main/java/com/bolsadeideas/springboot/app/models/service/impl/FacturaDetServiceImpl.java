package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IFacturaDetDao;
import com.bolsadeideas.springboot.app.models.entity.FacturaDetNew;
import com.bolsadeideas.springboot.app.models.service.IFacturaDetService;

@Service
public class FacturaDetServiceImpl implements IFacturaDetService{
	
	@Autowired
	IFacturaDetDao facturaDetDao;

	@Override
	public List<FacturaDetNew> findAll() {
		return (List<FacturaDetNew>) facturaDetDao.findAll();
	}

	@Override
	public void guardar(FacturaDetNew facturaDetNew) {
		facturaDetDao.save(facturaDetNew);
	}

	@Override
	public FacturaDetNew buscarUno(Long id) {
		return facturaDetDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		facturaDetDao.deleteById(id);
	}

}

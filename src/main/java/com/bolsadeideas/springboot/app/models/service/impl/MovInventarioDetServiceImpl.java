package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IMovInventarioDetDao;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioDetNew;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioDetService;

@Service
public class MovInventarioDetServiceImpl implements IMovInventarioDetService{

	@Autowired
	IMovInventarioDetDao movInventarioDetDao;
	
	@Override
	public List<MovInventarioDetNew> findAll() {
		return (List<MovInventarioDetNew>) movInventarioDetDao.findAll();
	}

	@Override
	public void guardar(MovInventarioDetNew movInventarioDet) {
		movInventarioDetDao.save(movInventarioDet);
	}

	@Override
	public MovInventarioDetNew buscarUno(Long id) {
		return movInventarioDetDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		movInventarioDetDao.deleteById(id);
	}

}

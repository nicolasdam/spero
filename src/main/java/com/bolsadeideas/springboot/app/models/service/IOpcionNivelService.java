package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Nivel;
import com.bolsadeideas.springboot.app.models.entity.Opcion;
import com.bolsadeideas.springboot.app.models.entity.OpcionNivel;

public interface IOpcionNivelService {
	
	public List<OpcionNivel> findAll();
	
	public List<OpcionNivel> findByNivel(Nivel nivel);

	public void guardar(OpcionNivel opcionNivel);

	public OpcionNivel buscarUno(Long id);

	public void eliminar(Long id);
	
	public OpcionNivel findOpNivel(Nivel nivel, Opcion opcion);

}

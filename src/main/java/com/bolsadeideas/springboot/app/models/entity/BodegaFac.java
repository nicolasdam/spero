package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class BodegaFac implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bodega_id")
	private Bodega bodega;
	
	@NotNull
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "bodega_id")
	private List<Factura> facturaItems;

	public Bodega getBodega() {
		return bodega;
	}

	public void setBodega(Bodega bodega) {
		this.bodega = bodega;
	}
	
	public BodegaFac() {
		this.facturaItems = new ArrayList<Factura>();
	}
	
	public List<Factura> getFacturaItems() {
		return facturaItems;
	}

	public void setFacturaItems(List<Factura> facturaItems) {
		this.facturaItems = facturaItems;
	}

	public void addItemMov(Factura factura) {
		this.facturaItems.add(factura);
	}
	
	public String getTotalBodega() {
		Double total = 0.0;
		int size = facturaItems.size();
		for (int pos = 0; pos < size; pos++) {
			total += facturaItems.get(pos).totalFactura();
		}
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(total);
	}

}

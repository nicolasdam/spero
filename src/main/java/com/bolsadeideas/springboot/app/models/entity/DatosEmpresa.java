package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "datos_empresa")
public class DatosEmpresa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "empresa_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long empresaId;

	@NotEmpty
	@Column(name = "razon_social")
	private String razonSocial;

	@NotEmpty
	private String nit;

	@NotEmpty
	private String direccion;

	@NotEmpty
	private String telefono;

	@NotEmpty
	private String movil;

	@NotEmpty
	private String correo;

	@NotEmpty
	@Column(name = "pagina_web")
	private String paginaWeb;

	@NotEmpty
	private String proyecto;

	@NotEmpty
	@Column(name = "clave_defecto")
	private String claveDefecto;

	public Long getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(Long empresaId) {
		this.empresaId = empresaId;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPaginaWeb() {
		return paginaWeb;
	}

	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}

	public String getClaveDefecto() {
		return claveDefecto;
	}

	public void setClaveDefecto(String claveDefecto) {
		this.claveDefecto = claveDefecto;
	}

}

package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Usuario;

@Service("jpaUsersDetailsService")
public class JpaUsersDetailsService implements UserDetailsService{
	
	@Autowired
	private IUsuarioDao usuarioDao;
	
	private Logger logger = LoggerFactory.getLogger(JpaUsersDetailsService.class);

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Usuario usuario = usuarioDao.findByUsuario(username);
		
		if(usuario == null) {
			logger.error("Error login: No existe el usuario '" + username + "'");
			throw new UsernameNotFoundException("Usuario '"+ username + "', no existe");
		}else {
			
		}
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		return new User(usuario.getUsuario(), usuario.getClave(), usuario.getEstado(), true, true, true, authorities);
	}
	
	

}

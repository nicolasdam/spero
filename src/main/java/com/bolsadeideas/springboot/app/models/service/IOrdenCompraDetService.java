package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.OrdenCompraDetNew;

public interface IOrdenCompraDetService {

	public List<OrdenCompraDetNew> findAll();

	public void guardar(OrdenCompraDetNew ordenCompraDet);

	public OrdenCompraDetNew buscarUno(Long id);

	public void eliminar(Long id);
	
}

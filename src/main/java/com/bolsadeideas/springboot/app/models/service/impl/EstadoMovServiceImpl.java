package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IEstadoMovDao;
import com.bolsadeideas.springboot.app.models.entity.EstadoMov;
import com.bolsadeideas.springboot.app.models.service.IEstadoMovService;

@Service
public class EstadoMovServiceImpl implements IEstadoMovService{

	@Autowired
	IEstadoMovDao estadoMovDao;
	
	@Override
	public List<EstadoMov> findAll() {
		return (List<EstadoMov>) estadoMovDao.findAll();
	}

	@Override
	public void guardar(EstadoMov estadoMov) {
		estadoMovDao.save(estadoMov);
	}

	@Override
	public EstadoMov buscarUno(Long id) {
		return estadoMovDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		estadoMovDao.deleteById(id);
	}

}

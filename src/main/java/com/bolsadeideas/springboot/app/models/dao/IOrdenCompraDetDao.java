package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.OrdenCompraDetNew;

public interface IOrdenCompraDetDao extends CrudRepository<OrdenCompraDetNew, Long>{

}

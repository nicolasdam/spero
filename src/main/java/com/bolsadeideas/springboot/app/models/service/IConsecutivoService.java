package com.bolsadeideas.springboot.app.models.service;

import com.bolsadeideas.springboot.app.models.entity.Consecutivo;

public interface IConsecutivoService {
	
	public void guardar(Consecutivo consecutivo);

	public Consecutivo buscarUno(Long id);
	
	public String buscarConsecutivo(Long id);

}

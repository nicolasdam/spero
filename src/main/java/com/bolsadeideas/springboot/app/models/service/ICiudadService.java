package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Ciudad;

public interface ICiudadService {
	
	public List<Ciudad> findAll();

	public void guardar(Ciudad Ciudad);

	public Ciudad buscarUno(Long id);

	public void eliminar(Long id);

}

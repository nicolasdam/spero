package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Bodega;

public interface IBodegaDao extends CrudRepository<Bodega, Long>{
	
	@Query("select bod from Bodega bod order by bod.bodegaId")
	public List<Bodega> findAllOrder();

}

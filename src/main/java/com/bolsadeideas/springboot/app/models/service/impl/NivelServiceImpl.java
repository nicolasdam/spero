package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.INivelDao;
import com.bolsadeideas.springboot.app.models.entity.Nivel;
import com.bolsadeideas.springboot.app.models.service.INivelService;

@Service
public class NivelServiceImpl implements INivelService{
	
	@Autowired
	private INivelDao nivelDao;

	@Override
	@Transactional(readOnly = true)
	public List<Nivel> findAll() {
		// TODO Auto-generated method stub
		return (List<Nivel>) nivelDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Nivel nivel) {
		nivelDao.save(nivel);
	}

	@Override
	@Transactional(readOnly = true)
	public Nivel buscarUno(Long id) {
		return nivelDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		nivelDao.deleteById(id);
	}

}

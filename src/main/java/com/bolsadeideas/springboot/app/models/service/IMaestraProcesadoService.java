package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MaestraProcesado;

public interface IMaestraProcesadoService {

	public List<MaestraProcesado> findAll();

	public void guardar(MaestraProcesado maestraProcesado);

	public MaestraProcesado buscarUno(Long id);

	public void eliminar(Long id);

	List<MaestraProcesado> buscarPorMaestraId(Maestra id);
	
}

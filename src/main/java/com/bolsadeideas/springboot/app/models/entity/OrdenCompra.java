package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "orden_compras")
public class OrdenCompra implements Serializable {

	@Id
	@Column(name = "orden_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ordenId;

	@Column(name = "fecha_registro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	@Column(name = "fecha_recibe")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRecibe;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "proveedor_id")
	private Proveedor proveedor;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_recibe_id")
	private Usuario usuarioRecibe;

	private Long valor_total;

	private String observaciones;

	private String observaciones_recibe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estado")
	private EstadoOc estadoOc;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "orden_id")
	private List<OrdenCompraDet> items;

	@PrePersist
	public void prePersist() {
		fechaRegistro = new Date();
	}

	public OrdenCompra() {
		this.items = new ArrayList<OrdenCompraDet>();
	}

	public Long getOrdenId() {
		return ordenId;
	}

	public void setOrdenId(Long ordenId) {
		this.ordenId = ordenId;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaRecibe() {
		return fechaRecibe;
	}

	public void setFechaRecibe(Date fechaRecibe) {
		this.fechaRecibe = fechaRecibe;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioRecibe() {
		return usuarioRecibe;
	}

	public void setUsuarioRecibe(Usuario usuarioRecibe) {
		this.usuarioRecibe = usuarioRecibe;
	}

	public Long getValor_total() {
		return valor_total;
	}

	public void setValor_total(Long valor_total) {
		this.valor_total = valor_total;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getObservaciones_recibe() {
		return observaciones_recibe;
	}

	public void setObservaciones_recibe(String observaciones_recibe) {
		this.observaciones_recibe = observaciones_recibe;
	}

	public List<OrdenCompraDet> getItems() {
		return items;
	}

	public EstadoOc getEstadoOc() {
		return estadoOc;
	}

	public void setEstadoOc(EstadoOc estadoOc) {
		this.estadoOc = estadoOc;
	}

	public void setItems(List<OrdenCompraDet> items) {
		this.items = items;
	}
	
	public void addItemOrden(OrdenCompraDet item) {
		this.items.add(item);
	}
	
	public String getTotalOrden() {
		Double total = 0.0;
		int size = items.size();
		for (int pos = 0; pos < size; pos++) {
			total += items.get(pos).totalItem();
		}
		DecimalFormat formateador = new DecimalFormat("###,###");

		return formateador.format(total);
	}

	private static final long serialVersionUID = 1L;

}

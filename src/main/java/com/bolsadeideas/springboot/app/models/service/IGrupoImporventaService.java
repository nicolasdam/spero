package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.GrupoImpoventa;

public interface IGrupoImporventaService {
	
	public List<GrupoImpoventa> findAll();

	public void guardar(GrupoImpoventa grupoImpoventa);

	public GrupoImpoventa buscarUno(Long id);

	public void eliminar(Long id);

}

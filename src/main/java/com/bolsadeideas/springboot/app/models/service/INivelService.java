package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Nivel;

public interface INivelService {

	public List<Nivel> findAll();

	public void guardar(Nivel nivel);

	public Nivel buscarUno(Long id);

	public void eliminar(Long id);

}

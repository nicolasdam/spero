package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.EstadoOc;

public interface IEstadoOcDao extends CrudRepository<EstadoOc, Long>{

}

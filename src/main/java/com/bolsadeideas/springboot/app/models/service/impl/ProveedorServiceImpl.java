package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IProveedorDao;
import com.bolsadeideas.springboot.app.models.entity.Proveedor;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;

@Service
public class ProveedorServiceImpl implements IProveedorService{

	@Autowired
	IProveedorDao proveedorDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> findAll() {
		return (List<Proveedor>) proveedorDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Proveedor proveedor) {
		proveedorDao.save(proveedor);
	}

	@Override
	public Proveedor buscarUno(Long id) {
		return proveedorDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		proveedorDao.deleteById(id);
	}

}

package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Opcion;

public interface IOpcionDao extends CrudRepository<Opcion, Long>{
	
	@Query("select op from Opcion op order by op.modulo.moduloId, op.ordenMenu, op.nombreOpcion")
	public List<Opcion> findAllOrder();

}

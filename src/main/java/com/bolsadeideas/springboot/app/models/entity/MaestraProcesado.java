package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "maestra_procesado")
public class MaestraProcesado implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "consecutivo_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long consecutivoId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "maestra_id")
	private Maestra maestraId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "maestra_pro_id")
	private Maestra maestraProId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@NotNull
	private Long cantidad;

	public Long getConsecutivoId() {
		return consecutivoId;
	}

	public void setConsecutivoId(Long consecutivoId) {
		this.consecutivoId = consecutivoId;
	}

	public Maestra getMaestraId() {
		return maestraId;
	}

	public void setMaestraId(Maestra maestraId) {
		this.maestraId = maestraId;
	}

	public Maestra getMaestraProId() {
		return maestraProId;
	}

	public void setMaestraProId(Maestra maestraProId) {
		this.maestraProId = maestraProId;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

}

package com.bolsadeideas.springboot.app.models.service;

import java.util.Date;
import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.KardexMov;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;

public interface IKardexMovService {
	
	public List<KardexMov> findAll();

	public void guardar(KardexMov kardexMov);

	public KardexMov buscarUno(Long id);

	public void eliminar(Long id);
	
	public List<KardexMov> findKardexByMaestra(Bodega bodega, Maestra maestra, Date fechaIni, Date fechaFin);
	
	public Boolean guardarKardexMov(MovInventario movInventario);
	
	public Boolean guardarKardexMovtraslados(MovInventario movInventario);
	
	public Boolean guardarKardexFac(Factura factura);

}

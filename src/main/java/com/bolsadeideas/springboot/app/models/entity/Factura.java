package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "facturas")
public class Factura implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "factura_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long facturaId;

	@Column(name = "fecha_factura")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFactura;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bodega_id")
	private Bodega bodega;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@NotEmpty
	private String numero;

	private String observaciones;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estado")
	private EstadoFac estadoFac;

	@NotNull
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "factura_id")
	private List<FacturaDet> items;

	@PrePersist
	public void prePersist() {
		fechaFactura = new Date();
	}
	
	public Factura() {
		this.items = new ArrayList<FacturaDet>();
	}
	
	public Double totalFactura() {
		Double total = 0.0;
		int size = items.size();
		for (int pos = 0; pos < size; pos++) {
			total += items.get(pos).totalItem();
		}
		return total;
	}
	
	public String getTotalOrden() {
		Double total = 0.0;
		int size = items.size();
		for (int pos = 0; pos < size; pos++) {
			total += items.get(pos).totalItem();
		}
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(total);
	}

	public String getFechaFacturaFormat() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String fechaTexto = formatter.format(getFechaFactura());

		return fechaTexto;
	}

	public void addItemFac(FacturaDet item) {
		this.items.add(item);
	}

	public List<FacturaDet> getItems() {
		return items;
	}

	public Long getFacturaId() {
		return facturaId;
	}

	public void setFacturaId(Long facturaId) {
		this.facturaId = facturaId;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public Bodega getBodega() {
		return bodega;
	}

	public void setBodega(Bodega bodega) {
		this.bodega = bodega;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public EstadoFac getEstadoFac() {
		return estadoFac;
	}

	public void setEstadoFac(EstadoFac estadoFac) {
		this.estadoFac = estadoFac;
	}

	public void setItems(List<FacturaDet> items) {
		this.items = items;
	}

}

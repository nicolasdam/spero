package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Fabricante;

public interface IFabricanteDao extends CrudRepository<Fabricante, Long>{

}

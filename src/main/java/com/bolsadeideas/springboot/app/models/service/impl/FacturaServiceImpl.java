package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IFacturaDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.EstadoFac;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.IFacturaService;

@Service
public class FacturaServiceImpl implements IFacturaService{

	@Autowired
	IFacturaDao facturaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Factura> findAll() {
		return (List<Factura>) facturaDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Factura factura) {
		facturaDao.save(factura);
	}

	@Override
	@Transactional(readOnly = true)
	public Factura buscarUno(Long id) {
		return facturaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		facturaDao.deleteById(id);
	}

	@Override
	public List<Factura> findEstado(EstadoFac estadoFac) {
		return facturaDao.findEstado(estadoFac);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Factura> findEstadoBod(EstadoFac estadoFac, Bodega bodega) {
		return facturaDao.findEstadoBod(estadoFac, bodega);
	}

	@Override
	public List<Factura> findFacFec(Date fechaIni, Date fechaFin, EstadoFac estadoFac) {
		return facturaDao.findFacFec(fechaIni, fechaFin, estadoFac);
	}

	@Override
	public List<Factura> findFacBodFec(Bodega bodega, Date fechaIni, Date fechaFin, EstadoFac estadoFac) {
		return facturaDao.findFacBodFec(bodega, fechaIni, fechaFin, estadoFac);
	}

	@Override
	public List<Factura> findFacCliFec(Cliente cliente, Date fechaIni, Date fechaFin, EstadoFac estadoFac) {
		return facturaDao.findFacCliFec(cliente, fechaIni, fechaFin, estadoFac);
	}

	@Override
	public List<Factura> findFacBodCliFec(Bodega bodega, Cliente cliente, Date fechaIni, Date fechaFin,
			EstadoFac estadoFac) {
		return facturaDao.findFacBodCliFec(bodega, cliente, fechaIni, fechaFin, estadoFac);
	}

	@Override
	public List<Factura> findFacCliUsuFec(Cliente cliente, Usuario usuario, Date fechaIni, Date fechaFin,
			EstadoFac estadoFac) {
		return facturaDao.findFacCliUsuFec(cliente, usuario, fechaIni, fechaFin, estadoFac);
	}

	@Override
	public List<Factura> findFacBodUsuFec(Bodega bodega, Usuario usuario, Date fechaIni, Date fechaFin,
			EstadoFac estadoFac) {
		return facturaDao.findFacBodUsuFec(bodega, usuario, fechaIni, fechaFin, estadoFac);
	}

	@Override
	public List<Factura> findFacUsuFec(Usuario usuario, Date fechaIni, Date fechaFin, EstadoFac estadoFac) {
		return facturaDao.findFacUsuFec(usuario, fechaIni, fechaFin, estadoFac);
	}

}

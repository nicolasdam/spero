package com.bolsadeideas.springboot.app.models.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.EstadoMov;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.Proveedor;

public interface IMovInventarioDao extends CrudRepository<MovInventario, Long>{
	
	@Query("select mi "
			+ "from MovInventario mi "
			+ "where mi.estadoMov = ?1 "
			+ "and mi.tipoMovimiento.tipoMov = ?2 "
			+ "order by mi.movimientoId desc")
	public List<MovInventario> findEstado(EstadoMov estadoMov, String tipoMov);
	
	@Query("select mi "
			+ "from MovInventario mi "
			+ "where mi.estadoMov = ?1 "
			+ "and mi.bodegaOrigen = ?2 "
			+ "and mi.tipoMovimiento.tipoMov = ?3 "
			+ "order by mi.movimientoId desc")
	public List<MovInventario> findMovEstBod(EstadoMov estadoMov, Bodega bodega, String tipoMov);
	
	@Query("select mi "
			+ "from MovInventario mi "
			+ "where mi.estadoMov = ?1 "
			+ "and mi.bodegaDestino = ?2 "
			+ "and mi.tipoMovimiento.tipoMov = ?3 "
			+ "order by mi.movimientoId desc")
	public List<MovInventario> findMovEstBodRec(EstadoMov estadoMov, Bodega bodega, String tipoMov);
	
	@Query("select mi "
			+ "from MovInventario mi "
			+ "where mi.bodegaOrigen = ?1 "
			+ "and mi.proveedor = ?2 "
			+ "and mi.tipoMovimiento.tipoMov = ?3 "
			+ "and mi.fechaRegistro between ?4 and ?5 "
			+ "order by mi.movimientoId desc")
	public List<MovInventario> findMovBodProTipoFec(Bodega bodega, Proveedor proveedor, String tipoMov, Date fechaIni, Date fechaFin);
	
	@Query("select mi "
			+ "from MovInventario mi "
			+ "where mi.bodegaOrigen = ?1 "
			+ "and mi.proveedor = ?2 "
			+ "and mi.fechaRegistro between ?3 and ?4 "
			+ "order by mi.movimientoId desc")
	public List<MovInventario> findMovBodProFec(Bodega bodega, Proveedor proveedor, Date fechaIni, Date fechaFin);
	
	@Query("select mi "
			+ "from MovInventario mi "
			+ "where mi.bodegaOrigen = ?1 "
			+ "and mi.tipoMovimiento.tipoMov = ?2 "
			+ "and mi.fechaRegistro between ?3 and ?4 "
			+ "order by mi.movimientoId desc")
	public List<MovInventario> findMovBodTipoFec(Bodega bodega, String tipoMov, Date fechaIni, Date fechaFin);
	
	@Query("select mi "
			+ "from MovInventario mi "
			+ "where mi.bodegaOrigen = ?1 "
			+ "and mi.fechaRegistro between ?2 and ?3 "
			+ "order by mi.movimientoId desc")
	public List<MovInventario> findMovBodFec(Bodega bodega, Date fechaIni, Date fechaFin);
	
}

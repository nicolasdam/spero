package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.FacturaDetNew;

public interface IFacturaDetService {
	
	public List<FacturaDetNew> findAll();

	public void guardar(FacturaDetNew facturaDetNew);

	public FacturaDetNew buscarUno(Long id);

	public void eliminar(Long id);

}

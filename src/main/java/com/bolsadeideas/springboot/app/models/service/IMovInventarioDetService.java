package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.MovInventarioDetNew;

public interface IMovInventarioDetService {
	
	public List<MovInventarioDetNew> findAll();

	public void guardar(MovInventarioDetNew movInventarioDet);

	public MovInventarioDetNew buscarUno(Long id);

	public void eliminar(Long id);
}

package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MaestraProcesado;

public interface IMaestraProcesadoDao extends CrudRepository<MaestraProcesado, Long>{
	
	@Query("select mp from MaestraProcesado mp where mp.maestraId = ?1")
	public List<MaestraProcesado> buscarPorMaestraId(Maestra term);

}

package com.bolsadeideas.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.EstadoMov;

public interface IEstadoMovDao extends CrudRepository<EstadoMov, Long>{

}

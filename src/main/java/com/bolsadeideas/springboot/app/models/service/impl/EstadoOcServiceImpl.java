package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IEstadoOcDao;
import com.bolsadeideas.springboot.app.models.entity.EstadoOc;
import com.bolsadeideas.springboot.app.models.service.IEstadoOcService;

@Service
public class EstadoOcServiceImpl implements IEstadoOcService{

	@Autowired
	IEstadoOcDao estadoOcDao;
	
	@Override
	public List<EstadoOc> findAll() {
		return (List<EstadoOc>) estadoOcDao.findAll();
	}

	@Override
	public void guardar(EstadoOc estadoOc) {
		estadoOcDao.save(estadoOc);
	}

	@Override
	public EstadoOc buscarUno(Long id) {
		return estadoOcDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		estadoOcDao.deleteById(id);
	}

}

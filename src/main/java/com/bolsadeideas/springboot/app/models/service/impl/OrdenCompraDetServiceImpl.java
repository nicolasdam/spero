package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IOrdenCompraDetDao;
import com.bolsadeideas.springboot.app.models.entity.OrdenCompraDetNew;
import com.bolsadeideas.springboot.app.models.service.IOrdenCompraDetService;

@Service
public class OrdenCompraDetServiceImpl implements IOrdenCompraDetService{

	@Autowired
	IOrdenCompraDetDao ordenCompraDetDao;
	
	@Override
	public List<OrdenCompraDetNew> findAll() {
		return (List<OrdenCompraDetNew>) ordenCompraDetDao.findAll();
	}

	@Override
	public void guardar(OrdenCompraDetNew ordenCompraDet) {
		ordenCompraDetDao.save(ordenCompraDet);	
	}

	@Override
	public OrdenCompraDetNew buscarUno(Long id) {
		return ordenCompraDetDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		ordenCompraDetDao.deleteById(id);
	}

}

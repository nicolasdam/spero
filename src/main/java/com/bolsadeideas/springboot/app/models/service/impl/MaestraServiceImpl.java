package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IMaestrasListaDao;
import com.bolsadeideas.springboot.app.models.dao.IMaestraDao;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MaestrasLista;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;

@Service
public class MaestraServiceImpl implements IMaestraService{

	@Autowired
	IMaestraDao maestraDao;
	
	@Autowired
	IMaestrasListaDao maestrasListaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Maestra> findAll() {
		return (List<Maestra>) maestraDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(Maestra maestra) {
		maestraDao.save(maestra);
	}

	@Override
	@Transactional(readOnly = true)
	public Maestra buscarUno(Long id) {
		return maestraDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void eliminar(Long id) {
		maestraDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<MaestrasLista> findByNombreLikeIgnoreCase(String term) {
		return maestrasListaDao.findByNombreLikeIgnoreCase(term);
	}

	@Override
	@Transactional(readOnly = true)
	public List<MaestrasLista> buscarPorNombreMaestra(String term) {
		return maestrasListaDao.buscarPorNombreMaestra(term);
	}

	@Override
	public List<MaestrasLista> findAllTodos() {
		return (List<MaestrasLista>) maestrasListaDao.findAll();
	}

	@Override
	public List<Maestra> sortMaestraNombre() {
		return (List<Maestra>) maestraDao.sortMaestraNombre();
	}

}

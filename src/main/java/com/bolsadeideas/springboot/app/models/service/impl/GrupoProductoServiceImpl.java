package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bolsadeideas.springboot.app.models.dao.IGrupoProductoDao;
import com.bolsadeideas.springboot.app.models.entity.GrupoProducto;
import com.bolsadeideas.springboot.app.models.service.IGrupoProductoService;

@Service
public class GrupoProductoServiceImpl implements IGrupoProductoService{

	@Autowired
	IGrupoProductoDao grupoProductoDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<GrupoProducto> findAll() {
		return (List<GrupoProducto>) grupoProductoDao.findAll();
	}

	@Override
	@Transactional
	public void guardar(GrupoProducto grupoProducto) {
		grupoProductoDao.save(grupoProducto);
	}

	@Override
	public GrupoProducto buscarUno(Long id) {
		return grupoProductoDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		grupoProductoDao.deleteById(id);
	}
	
	

}

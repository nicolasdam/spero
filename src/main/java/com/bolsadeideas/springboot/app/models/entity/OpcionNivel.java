package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "opciones_nivel")
public class OpcionNivel implements Serializable {

	@Id
	@Column(name = "nivel_op_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long nivelOpId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nivel_id")
	private Nivel nivel;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "opcion_id")
	private Opcion opcion;

	@NotNull
	@Column(name = "tipo_acceso")
	private Long tipoAcceso;

	public Long getNivelOpId() {
		return nivelOpId;
	}

	public void setNivelOpId(Long nivelOpId) {
		this.nivelOpId = nivelOpId;
	}

	public Nivel getNivel() {
		return nivel;
	}

	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

	public Opcion getOpcion() {
		return opcion;
	}

	public void setOpcion(Opcion opcion) {
		this.opcion = opcion;
	}

	public Long getTipoAcceso() {
		return tipoAcceso;
	}

	public void setTipoAcceso(Long tipoAcceso) {
		this.tipoAcceso = tipoAcceso;
	}

	private static final long serialVersionUID = 1L;

}

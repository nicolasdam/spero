package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "mov_inventario")
public class MovInventario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "movimiento_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long movimientoId;

	@Column(name = "fecha_registro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;

	@Column(name = "fecha_recibe")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRecibe;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bodega_ori_id")
	private Bodega bodegaOrigen;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bodega_des_id")
	private Bodega bodegaDestino;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_movimiento_id")
	private TipoMovimiento tipoMovimiento;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "proveedor_id")
	private Proveedor proveedor;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuari_recibe_id")
	private Usuario usuarioRecibe;

	@NotEmpty
	private String numero;

	@NotEmpty
	@JoinColumn(name = "doc_soporte")
	private String docSoporte;

	@JoinColumn(name = "valor_mov")
	private Double valorMov;

	private String observaciones;

	@JoinColumn(name = "observaciones_recibe")
	private String observacionesRecibe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estado")
	private EstadoMov estadoMov;

	@NotNull
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "movimiento_id")
	private List<MovInventarioDet> items;

	@PrePersist
	public void prePersist() {
		fechaRegistro = new Date();
	}

	public MovInventario() {
		this.items = new ArrayList<MovInventarioDet>();
	}

	public Long getMovimientoId() {
		return movimientoId;
	}

	public void setMovimientoId(Long movimientoId) {
		this.movimientoId = movimientoId;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaRecibe() {
		return fechaRecibe;
	}

	public void setFechaRecibe(Date fechaRecibe) {
		this.fechaRecibe = fechaRecibe;
	}

	public Bodega getBodegaOrigen() {
		return bodegaOrigen;
	}

	public void setBodegaOrigen(Bodega bodegaOrigen) {
		this.bodegaOrigen = bodegaOrigen;
	}

	public Bodega getBodegaDestino() {
		return bodegaDestino;
	}

	public void setBodegaDestino(Bodega bodegaDestino) {
		this.bodegaDestino = bodegaDestino;
	}

	public TipoMovimiento getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioRecibe() {
		return usuarioRecibe;
	}

	public void setUsuarioRecibe(Usuario usuarioRecibe) {
		this.usuarioRecibe = usuarioRecibe;
	}

	public String getNumero() {
		return numero;
	}

	public String getDocSoporte() {
		return docSoporte;
	}

	public void setDocSoporte(String docSoporte) {
		this.docSoporte = docSoporte;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Double getValorMov() {
		return valorMov;
	}

	public void setValorMov(Double valorMov) {
		this.valorMov = valorMov;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getObservacionesRecibe() {
		return observacionesRecibe;
	}

	public void setObservacionesRecibe(String observacionesRecibe) {
		this.observacionesRecibe = observacionesRecibe;
	}

	public EstadoMov getEstadoMov() {
		return estadoMov;
	}

	public void setEstadoMov(EstadoMov estadoMov) {
		this.estadoMov = estadoMov;
	}

	public List<MovInventarioDet> getItems() {
		return items;
	}

	public void setItems(List<MovInventarioDet> items) {
		this.items = items;
	}

	public void addItemMov(MovInventarioDet item) {
		this.items.add(item);
	}

	public String getTotalOrden() {
		Double total = 0.0;
		int size = items.size();
		for (int pos = 0; pos < size; pos++) {
			total += items.get(pos).totalItem();
		}
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(total);
	}
	
	public String getFechaFormat() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String fechaTexto = formatter.format(getFechaRegistro());

		return fechaTexto;
	}
	
}

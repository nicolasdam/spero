package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "kardex_producto")
public class KardexMov implements Serializable {

	@Id
	@Column(name = "kardex_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long kardexId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "movimiento_id")
	private MovInventario movimientoId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "factura_id")
	private Factura facturaId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo_movimiento_id")
	private TipoMovimiento tipoMovimiento;

	@NotEmpty
	private String documento;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bodega_id")
	private Bodega bodegaId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "maestra_id")
	private Maestra maestra;

	@NotNull
	private Double cantidad;

	@NotNull
	@JoinColumn(name = "costo_promedio")
	private Double costoPromedio;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@NotNull
	@JoinColumn(name = "saldo_anterior")
	private Double saldoAnterior;

	@NotNull
	@JoinColumn(name = "saldo_nuevo")
	private Double saldoNuevo;

	@NotEmpty
	@JoinColumn(name = "tipo_mov")
	private String tipoMov;

	@PrePersist
	public void prePersist() {
		fecha = new Date();
	}

	public Long getKardexId() {
		return kardexId;
	}

	public void setKardexId(Long kardexId) {
		this.kardexId = kardexId;
	}

	public MovInventario getMovimientoId() {
		return movimientoId;
	}

	public void setMovimientoId(MovInventario movimientoId) {
		this.movimientoId = movimientoId;
	}
	
	public Factura getFacturaId() {
		return facturaId;
	}

	public void setFacturaId(Factura facturaId) {
		this.facturaId = facturaId;
	}

	public TipoMovimiento getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Bodega getBodegaId() {
		return bodegaId;
	}

	public void setBodegaId(Bodega bodegaId) {
		this.bodegaId = bodegaId;
	}

	public Maestra getMaestra() {
		return maestra;
	}

	public void setMaestra(Maestra maestra) {
		this.maestra = maestra;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public Double getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(Double costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Double getSaldoAnterior() {
		return saldoAnterior;
	}

	public void setSaldoAnterior(Double saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public Double getSaldoNuevo() {
		return saldoNuevo;
	}

	public void setSaldoNuevo(Double saldoNuevo) {
		this.saldoNuevo = saldoNuevo;
	}

	public String getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(String tipoMov) {
		this.tipoMov = tipoMov;
	}
	
	public String getSoporte() {
		String strSoporte = "";
		if (this.movimientoId != null) {
			strSoporte = this.movimientoId.getDocSoporte();
		}
		return strSoporte;
	}
	
	public String getFechaFormat() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		String fechaTexto = formatter.format(getFecha());

		return fechaTexto;
	}
	
	public String getCostoPromFormat() {
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(costoPromedio);
	}
	
	public String getCantidadFormat() {
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(cantidad);
	}
	
	public String getSaldoAnteriorFormat() {
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(saldoAnterior);
	}
	
	public String getSaldoNuevoFormat() {
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(saldoNuevo);
	}

	private static final long serialVersionUID = 1L;

}

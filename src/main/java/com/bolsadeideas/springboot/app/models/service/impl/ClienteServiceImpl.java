package com.bolsadeideas.springboot.app.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bolsadeideas.springboot.app.models.dao.IClienteDao;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.service.IClienteService;

@Service
public class ClienteServiceImpl implements IClienteService{

	@Autowired
	IClienteDao clienteDao;
	
	@Override
	public List<Cliente> findAll() {
		return (List<Cliente>) clienteDao.findAll();
	}
	
	@Override
	public void guardar(Cliente cliente) {
		clienteDao.save(cliente);
	}
	
	@Override
	public Cliente buscarUno(Long id) {
		return clienteDao.findById(id).orElse(null);
	}

	@Override
	public void eliminar(Long id) {
		clienteDao.deleteById(id);
	}

}

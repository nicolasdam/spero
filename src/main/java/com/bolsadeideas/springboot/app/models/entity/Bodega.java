package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bodegas")
public class Bodega implements Serializable {

	@Id
	@Column(name = "bodega_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bodegaId;

	@Column(name = "fecha_registro")
	@Temporal(TemporalType.DATE)
	private Date fechaRegistro;

	@NotEmpty
	private String codigo;

	@NotEmpty
	private String descripcion;

	@NotEmpty
	private String direccion;

	@NotEmpty
	private String telefono;

	@NotNull
	private Long estado;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ciudad_id")
	private Ciudad ciudad;

	@OneToMany(mappedBy = "bodegaId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<SaldoBodega> saldoBodega;

	@PrePersist
	public void prePersist() {
		fechaRegistro = new Date();
	}

	public Long getBodegaId() {
		return bodegaId;
	}

	public void setBodegaId(Long bodegaId) {
		this.bodegaId = bodegaId;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	public List<SaldoBodega> getSaldoBodega() {
		return saldoBodega;
	}

	public void setSaldoBodega(List<SaldoBodega> saldoBodega) {
		this.saldoBodega = saldoBodega;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	private static final long serialVersionUID = 1L;

}

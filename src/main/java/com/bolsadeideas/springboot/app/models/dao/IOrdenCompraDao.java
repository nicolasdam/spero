package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.OrdenCompra;

public interface IOrdenCompraDao extends CrudRepository<OrdenCompra, Long>{
	
	@Query("select oc from OrdenCompra oc order by oc.ordenId desc")
	public List<OrdenCompra> findOrdenDesc();

}

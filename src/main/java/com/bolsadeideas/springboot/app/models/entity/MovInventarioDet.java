package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "mov_inventerio_det")
public class MovInventarioDet implements Serializable {

	@Id
	@Column(name = "detalle_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long detalleId;

	@Column(name = "movimiento_id")
	private Long movimientoId;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "maestra_id")
	private Maestra maestra;

	@NotNull
	@Min(1)
	@Column(name = "valor_costo")
	private Double valorCosto;

	@NotNull
	private Double cantidad;

	@Column(name = "cantidad_rec")
	private Double cantidadRec;

	public Long getDetalleId() {
		return detalleId;
	}

	public void setDetalleId(Long detalleId) {
		this.detalleId = detalleId;
	}

	public Maestra getMaestra() {
		return maestra;
	}

	public void setMaestra(Maestra maestra) {
		this.maestra = maestra;
	}

	public Double getValorCosto() {
		return valorCosto;
	}

	public void setValorCosto(Double valorCosto) {
		this.valorCosto = valorCosto;
	}

	public Double getCantidad() {
		return cantidad;
	}

	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public String totalItemForm() {

		DecimalFormat formateador = new DecimalFormat("###,###.##");

		return formateador.format(cantidad.doubleValue() * valorCosto);
		// return (cantidad.doubleValue() * valorCosto);
	}

	public Double totalItem() {

		return cantidad.doubleValue() * valorCosto;
		// return (cantidad.doubleValue() * valorCosto);
	}

	public String costoItem() {
		DecimalFormat formateador = new DecimalFormat("###,###.##");
		return formateador.format(valorCosto);
	}

	public Long getMovimientoId() {
		return movimientoId;
	}

	public void setMovimientoId(Long movimientoId) {
		this.movimientoId = movimientoId;
	}

	public Double getCantidadRec() {
		return cantidadRec;
	}

	public void setCantidadRec(Double cantidadRec) {
		this.cantidadRec = cantidadRec;
	}

	private static final long serialVersionUID = 1L;

}

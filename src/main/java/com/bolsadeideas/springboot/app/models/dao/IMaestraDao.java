package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Maestra;

public interface IMaestraDao extends CrudRepository<Maestra, Long>{
	
	@Query("select mp from Maestra mp order by mp.nombre")
	public List<Maestra> sortMaestraNombre();
	
}

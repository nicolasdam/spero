package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Cliente;

public interface IClienteService {
	
	public List<Cliente> findAll();

	public void guardar(Cliente cliente);

	public Cliente buscarUno(Long id);

	public void eliminar(Long id);

}

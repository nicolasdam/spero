package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "opciones")
public class Opcion implements Serializable {

	@Id
	@Column(name = "opcion_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long opcionId;

	@NotEmpty
	@Column(name = "nombre_opcion")
	private String nombreOpcion;

	@NotEmpty
	private String objeto;

	@NotNull
	@Column(name = "tipo_opc")
	private Long tipoOpc;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modulo_id")
	private Modulo modulo;

	@NotNull
	@Column(name = "orden_menu")
	private Long ordenMenu;

	public Long getOpcionId() {
		return opcionId;
	}

	public void setOpcionId(Long opcionId) {
		this.opcionId = opcionId;
	}

	public String getNombreOpcion() {
		return nombreOpcion;
	}

	public void setNombreOpcion(String nombreOpcion) {
		this.nombreOpcion = nombreOpcion;
	}

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public Long getTipoOpc() {
		return tipoOpc;
	}

	public void setTipoOpc(Long tipoOpc) {
		this.tipoOpc = tipoOpc;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Long getOrdenMenu() {
		return ordenMenu;
	}

	public void setOrdenMenu(Long ordenMenu) {
		this.ordenMenu = ordenMenu;
	}

	private static final long serialVersionUID = 1L;

}

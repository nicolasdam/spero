package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Opcion;

public interface IOpcionService {

	public List<Opcion> findAll();

	public void guardar(Opcion opcion);

	public Opcion buscarUno(Long id);

	public void eliminar(Long id);
	
	public List<Opcion> findAllOrder();

}

package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "maestra_productos")
public class Maestra implements Serializable {

	@Id
	@Column(name = "maestra_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long maestraId;

	@NotEmpty
	private String codigo;

	@NotEmpty
	private String nombre;

	@NotEmpty
	@Column(name = "codigo_barras")
	private String codigoBarras;

	@Column(name = "fecha_registro")
	@Temporal(TemporalType.DATE)
	private Date fechaRegistro;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "presentacion_id")
	private Presentacion presentacion;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "proveedor_id")
	private Proveedor proveedor;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unidad_id")
	private UnidadMedida unidadMedida;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unidad_venta_id")
	private UnidadMedida unidadMedidaVenta;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@Column(name = "costo_promedio")
	private Double costoPromedio;

	@Column(name = "precio_venta")
	private Double precioVenta;

	@Column(name = "saldo_actual")
	private Double saldoActual;

	@Column(name = "fechaCosto")
	private Date fecha_costo;

	@Column(name = "costo_ultima")
	private Double costoUltima;

	@NotNull
	@Column(name = "cant_empaque")
	private Long cantEmpaque;

	@NotNull
	private Long estado;

	@NotNull
	private Boolean procesado;

	@PrePersist
	public void prePersist() {
		fechaRegistro = new Date();
	}

	private static final long serialVersionUID = 1L;

	public Long getMaestraId() {
		return maestraId;
	}

	public void setMaestraId(Long maestraId) {
		this.maestraId = maestraId;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Presentacion getPresentacion() {
		return presentacion;
	}

	public void setPresentacion(Presentacion presentacion) {
		this.presentacion = presentacion;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public UnidadMedida getUnidadMedida() {
		return unidadMedida;
	}

	public void setUnidadMedida(UnidadMedida unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public UnidadMedida getUnidadMedidaVenta() {
		return unidadMedidaVenta;
	}

	public void setUnidadMedidaVenta(UnidadMedida unidadMedidaVenta) {
		this.unidadMedidaVenta = unidadMedidaVenta;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Double getCostoPromedio() {
		return costoPromedio;
	}

	public void setCostoPromedio(Double costoPromedio) {
		this.costoPromedio = costoPromedio;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Double getSaldoActual() {
		return saldoActual;
	}

	public void setSaldoActual(Double saldoActual) {
		this.saldoActual = saldoActual;
	}

	public Date getFecha_costo() {
		return fecha_costo;
	}

	public void setFecha_costo(Date fecha_costo) {
		this.fecha_costo = fecha_costo;
	}

	public Double getCostoUltima() {
		return costoUltima;
	}

	public void setCostoUltima(Double costoUltima) {
		this.costoUltima = costoUltima;
	}

	public Long getCantEmpaque() {
		return cantEmpaque;
	}

	public void setCantEmpaque(Long cantEmpaque) {
		this.cantEmpaque = cantEmpaque;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	public Boolean getProcesado() {
		return procesado;
	}

	public void setProcesado(Boolean procesado) {
		this.procesado = procesado;
	}
	
	public String getCostoFormat() {
		DecimalFormat formateador = new DecimalFormat("###,###,###.00");
		return formateador.format(costoPromedio);
	}

}

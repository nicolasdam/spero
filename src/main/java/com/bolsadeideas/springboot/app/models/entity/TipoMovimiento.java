package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tipo_movimiento")
public class TipoMovimiento implements Serializable {

	@Id
	@Column(name = "tipo_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long tipoId;

	@NotEmpty
	private String descripcion;

	@NotNull
	@Column(name = "tipo_mov")
	private String tipoMov;

	public Long getTipoId() {
		return tipoId;
	}

	public void setTipoId(Long tipoId) {
		this.tipoId = tipoId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(String tipoMov) {
		this.tipoMov = tipoMov;
	}

	private static final long serialVersionUID = 1L;

}

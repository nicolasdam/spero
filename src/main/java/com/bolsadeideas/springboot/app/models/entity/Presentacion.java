package com.bolsadeideas.springboot.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "presentaciones")
public class Presentacion implements Serializable {

	@Id
	@Column(name = "presentacion_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long presentacionId;

	@NotEmpty
	@Column(name = "presentacion")
	private String presentacionDesc;

	@NotNull
	private Long estado;

	public Long getPresentacionId() {
		return presentacionId;
	}

	public void setPresentacionId(Long presentacionId) {
		this.presentacionId = presentacionId;
	}

	public String getPresentacionDesc() {
		return presentacionDesc;
	}

	public void setPresentacionDesc(String presentacionDesc) {
		this.presentacionDesc = presentacionDesc;
	}

	public Long getEstado() {
		return estado;
	}

	public void setEstado(Long estado) {
		this.estado = estado;
	}

	private static final long serialVersionUID = 1L;

}

package com.bolsadeideas.springboot.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.SaldoBodega;

public interface ISaldoBodegaDao extends CrudRepository<SaldoBodega, Long>{
	
	@Query("select sb "
			+ "from SaldoBodega sb "
			+ "order by sb.bodegaId.bodegaId, sb.maestra.nombre")
	public List<SaldoBodega> saldoTotal();
	
	@Query("select sb "
			+ "from SaldoBodega sb "
			+ "where sb.bodegaId = ?1 "
			+ "and sb.maestra = ?2 "
			+ "order by sb.bodegaId.bodegaId, sb.maestra.nombre")
	public SaldoBodega saldoBodegaMaestra(Bodega bodega, Maestra maestra);
	
	@Query("select sb "
			+ "from SaldoBodega sb "
			+ "where sb.bodegaId = ?1 "
			+ "and cantidad > 0 "
			+ "order by sb.bodegaId.bodegaId, sb.maestra.nombre")
	public List<SaldoBodega> saldoBodega(Bodega bodega);
	
	@Query("select sb "
			+ "from SaldoBodega sb "
			+ "where sb.maestra = ?1 "
			+ "order by sb.bodegaId.bodegaId, sb.maestra.nombre")
	public List<SaldoBodega> saldoMaestra(Maestra maestra);
	
}

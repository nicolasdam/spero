package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MaestrasLista;

public interface IMaestraService {
	
	public List<Maestra> findAll();

	public void guardar(Maestra maestra);

	public Maestra buscarUno(Long id);

	public void eliminar(Long id);
	
	public List<MaestrasLista> buscarPorNombreMaestra(String term);
	
	public List<MaestrasLista> findByNombreLikeIgnoreCase(String term);
	
	public List<MaestrasLista> findAllTodos();
	
	public List<Maestra> sortMaestraNombre();

}

package com.bolsadeideas.springboot.app.models.service;

import java.util.List;

import com.bolsadeideas.springboot.app.models.entity.Proveedor;

public interface IProveedorService {
	
	public List<Proveedor> findAll();

	public void guardar(Proveedor proveedor);

	public Proveedor buscarUno(Long id);

	public void eliminar(Long id);

}

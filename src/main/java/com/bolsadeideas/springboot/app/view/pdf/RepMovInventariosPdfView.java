package com.bolsadeideas.springboot.app.view.pdf;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioRep;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.ITipoMovService;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Controller("/mov_inventarios/reportes/rep-mov-pdf")
public class RepMovInventariosPdfView extends AbstractPdfView{
	
	@Autowired
	ITipoMovService tipoMovService;

	private static final BaseFont Currier = null;

	File file = null;
    BufferedInputStream input = null;
    BufferedOutputStream output = null;

	@SuppressWarnings("unchecked")
	@Override
	protected void  buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DatosEmpresa datosEmpresa = (DatosEmpresa) model.get("datosEmpresa");
		Usuario usuario = (Usuario) model.get("usuario");
		List<MovInventario> listMovimientos = new ArrayList<>();
		listMovimientos = (List<MovInventario>) model.get("listMovimientos");
		MovInventarioRep datosRep = (MovInventarioRep) model.get("movInventarioRep");
		//TipoMovimiento tipoMov = tipoMovService.buscarUno(datosRep.getTipoMov());
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String strFechaAct = sdf.format(cal.getTime());
		
		String strTitulo = "";
		
		Font fontH0 = new Font(Currier, 5, Font.NORMAL);
		Font fontH1 = new Font(Currier, 8, Font.NORMAL);
		Font fontH2 = new Font(Currier, 6, Font.BOLD);
		Font fontH3 = new Font(Currier, 6, Font.NORMAL);
		Font fontH4 = new Font(Currier, 10, Font.BOLD);
		
		PdfPTable Tenc = new PdfPTable(1);
		Tenc.setWidthPercentage(100);
		PdfPCell celdaEnc = new PdfPCell();
		
		//datos del encabezado
		celdaEnc.setPhrase(new Phrase(strFechaAct,fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		celdaEnc.setPhrase(new Phrase("Imprime: " + usuario.getNombres() + "" + usuario.getApellidos(),fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		//datos de la empresa
		PdfPTable TdatosEmpresa = new PdfPTable(1);
		PdfPCell celdaEmpresa = new PdfPCell();
		
		TdatosEmpresa.setWidthPercentage(50);
		TdatosEmpresa.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		TdatosEmpresa.setSpacingAfter(10);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getProyecto(),fontH4));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(1);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getRazonSocial(),fontH1));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getNit(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getDireccion(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getTelefono() + " - " + datosEmpresa.getMovil(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(1);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		//datos encabezado del movimiento de inventario
		PdfPTable TmovInventario = new PdfPTable(7);
		//PdfPCell celdaEncMov = new PdfPCell();
		TmovInventario.setWidthPercentage(100);
		TmovInventario.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		
		strTitulo = "REPORTE MOVIMIENTOS DE INVENTARIO";
		PdfPCell celdaTitEncMov = new PdfPCell();
		celdaTitEncMov.setPhrase(new Phrase(strTitulo,fontH4));
		celdaTitEncMov.setColspan(7);
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		celdaTitEncMov.setPhrase(new Phrase("Fecha Inicial: " + datosRep.getFechaInicial() + " - Fecha Final: " + datosRep.getFechaFinal(),fontH2));
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		celdaTitEncMov.setPhrase(new Phrase("Bodega: " + datosRep.getBodega().getDescripcion(),fontH2));
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		String tipoMov;
		switch(datosRep.getTipoMov()) {
		  case "0":
			  tipoMov = "Todos los Movimientos";
		    break;
		  case "E":
			  tipoMov = "Entradas";
		    break;
		  case "S":
			  tipoMov = "Salidas";
		    break;
		  case "T":
			  tipoMov = "Traslados";
		    break;
		  default:
			  tipoMov = "Todos los Movimientos";
		}
			
		celdaTitEncMov.setPhrase(new Phrase("Tipo Movimiento: " + tipoMov,fontH2));
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);

		if(datosRep.getProveedor() != null) {
			celdaTitEncMov.setPhrase(new Phrase("Proveedor: " + datosRep.getProveedor().getNombre(),fontH2));
			celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
			celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			TmovInventario.addCell(celdaTitEncMov);
		}
		
		//datos titulo detalle del movimiento de inventario
		PdfPTable TmovInventarioDet = new PdfPTable(7);
		TmovInventarioDet.setWidths(new float [] {1,1,3,1,2,3,1});
		TmovInventarioDet.setWidthPercentage(100);
		TmovInventarioDet.setSpacingAfter(20);
		
		PdfPCell celdaTitulo = new PdfPCell();
		PdfPCell celdaTituloItem = new PdfPCell();
		
		celdaTitulo.setPhrase(new Phrase("Detalle Movimiento",fontH2));
		celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		celdaTitulo.setBorder(Rectangle.NO_BORDER);
		celdaTitulo.setColspan(7);
		TmovInventarioDet.addCell(celdaTitulo);
		
		celdaTituloItem.setPhrase(new Phrase("Numero",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Soporte",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Proveedor",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Fecha Mov",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Creado por",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Observaciones",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Estado",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		//datos detalle del movimiento de inventario
		for(MovInventario item: listMovimientos) {
			PdfPCell celdaItem = new PdfPCell();
			
			celdaItem.setPhrase(new Phrase(item.getNumero(),fontH3));
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getDocSoporte(),fontH3));
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getProveedor().getNombre(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getFechaFormat(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getUsuario().getNombres() + " " + item.getUsuario().getApellidos(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getObservaciones(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getEstadoMov().getDescripcion(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			TmovInventarioDet.addCell(celdaItem);
		}
		
		PdfPTable TmovFirmas = new PdfPTable(1);
		TmovFirmas.setWidthPercentage(100);
		PdfPCell celdaFirma = new PdfPCell();
		celdaFirma.setBorder(Rectangle.NO_BORDER);
		celdaFirma.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		
		celdaFirma.setPhrase(new Phrase("Fin del reporte",fontH0));
		TmovFirmas.addCell(celdaFirma);
		
		document.add(Tenc);
		document.add(TdatosEmpresa);
		document.add(TmovInventario);
		//document.add(TcreadoPor);
		document.add(TmovInventarioDet);
		document.add(TmovFirmas);
		
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline");
		
		//String filename = movInventario.getNumero() + ".pdf";
	    //response.setContentType("application/pdf");
	    //response.setHeader("Content-Disposition", "attachment; filename=" + filename);
		
	}

}

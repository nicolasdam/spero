package com.bolsadeideas.springboot.app.view.pdf;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.bolsadeideas.springboot.app.models.entity.BodegaFac;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.FacturaRep;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Controller("/ventas/reportes/rep-ventas-dia-pdf")
public class RepVentasDiaPdfView extends AbstractPdfView{

	private static final BaseFont Currier = null;

	File file = null;
    BufferedInputStream input = null;
    BufferedOutputStream output = null;

	@Override
	@SuppressWarnings("unchecked")
	protected void  buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DatosEmpresa datosEmpresa = (DatosEmpresa) model.get("datosEmpresa");
		Usuario usuario = (Usuario) model.get("usuario");
		List<BodegaFac> listBodegaFac = (List<BodegaFac>) model.get("listBodegaFac");
		FacturaRep datosRep = (FacturaRep) model.get("facturaRep");

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strFechaAct = sdf.format(cal.getTime());
		
		String strTitulo = "REPORTE DE VENTAS DIARIA POR BODEGA";
		
		Font fontH0 = new Font(Currier, 5, Font.NORMAL);
		Font fontH1 = new Font(Currier, 8, Font.NORMAL);
		Font fontH11 = new Font(Currier, 8, Font.BOLD);
		Font fontH2 = new Font(Currier, 6, Font.BOLD);
		Font fontH3 = new Font(Currier, 6, Font.NORMAL);
		Font fontH4 = new Font(Currier, 10, Font.BOLD);
		
		PdfPTable Tenc = new PdfPTable(1);
		Tenc.setWidthPercentage(100);
		PdfPCell celdaEnc = new PdfPCell();
		
		//datos del encabezado
		celdaEnc.setPhrase(new Phrase(strFechaAct,fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		celdaEnc.setPhrase(new Phrase("Imprime: " + usuario.getNombres() + "" + usuario.getApellidos(),fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		//datos de la empresa
		PdfPTable TdatosEmpresa = new PdfPTable(1);
		PdfPCell celdaEmpresa = new PdfPCell();
		
		TdatosEmpresa.setWidthPercentage(50);
		TdatosEmpresa.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		TdatosEmpresa.setSpacingAfter(10);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getProyecto(),fontH4));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(1);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getRazonSocial(),fontH1));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getNit(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getDireccion(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getTelefono() + " - " + datosEmpresa.getMovil(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(1);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		//datos encabezado del movimiento de inventario
		PdfPTable TmovInventario = new PdfPTable(5);
		//PdfPCell celdaEncMov = new PdfPCell();
		TmovInventario.setWidthPercentage(100);
		TmovInventario.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		
		PdfPCell celdaTitEncMov = new PdfPCell();
		celdaTitEncMov.setPhrase(new Phrase(strTitulo,fontH4));
		celdaTitEncMov.setColspan(5);
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		celdaTitEncMov.setPhrase(new Phrase("Fecha reporte: " + datosRep.getFechaInicial(),fontH2));
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		//datos titulo detalle del movimiento de inventario
		PdfPTable TVentasBod = new PdfPTable(5);
		TVentasBod.setWidths(new float [] {1,4,1,3,1});
		TVentasBod.setWidthPercentage(100);
		TVentasBod.setSpacingAfter(5);
		
		for(BodegaFac bodegaFac: listBodegaFac) {
			
			//Bodega
			celdaTitEncMov.setPhrase(new Phrase("Bodega: " + bodegaFac.getBodega().getDescripcion(),fontH11));
			celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
			celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			TVentasBod.addCell(celdaTitEncMov);
			
			PdfPCell celdaTitulo = new PdfPCell();
			
			celdaTitulo.setPhrase(new Phrase("Numero",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TVentasBod.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase("Cliente",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TVentasBod.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase("Fecha",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TVentasBod.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase("Observaciones",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TVentasBod.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase("Valor Total",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TVentasBod.addCell(celdaTitulo);
			
			//datos detalle del movimiento de inventario
			for(Factura item: bodegaFac.getFacturaItems()) {
				PdfPCell celdaItem = new PdfPCell();
				
				celdaItem.setPhrase(new Phrase(item.getNumero(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
				TVentasBod.addCell(celdaItem);
				
				celdaItem.setPhrase(new Phrase(item.getCliente().getRazonSocial(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
				TVentasBod.addCell(celdaItem);
				
				celdaItem.setPhrase(new Phrase(item.getFechaFacturaFormat(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
				TVentasBod.addCell(celdaItem);
				
				celdaItem.setPhrase(new Phrase(item.getObservaciones(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
				TVentasBod.addCell(celdaItem);
				
				celdaItem.setPhrase(new Phrase(item.getTotalOrden(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
				TVentasBod.addCell(celdaItem);
				
			}
			
			celdaTitulo.setPhrase(new Phrase("Total Bodega: ",fontH2));
			celdaTitulo.setColspan(4);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TVentasBod.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase(bodegaFac.getTotalBodega(),fontH2));
			celdaTitulo.setColspan(1);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TVentasBod.addCell(celdaTitulo);
			
			PdfPCell celdaPaso = new PdfPCell();
			celdaPaso.setBorderWidthTop(0);
			celdaPaso.setBorderWidthBottom(0);
			celdaPaso.setBorderWidthLeft(0);
			celdaPaso.setBorderWidthRight(0);
			celdaPaso.setPhrase(new Phrase("",fontH2));
			celdaPaso.setColspan(5);
			TVentasBod.addCell(celdaPaso);
		}
		
		//datos titulo detalle del movimiento de inventario
		PdfPTable TkardexTotal = new PdfPTable(8);
		TkardexTotal.setWidths(new float [] {1,1,1,2,1,1,1,1});
		TkardexTotal.setWidthPercentage(100);
		TkardexTotal.setSpacingAfter(20);
		
		
		PdfPTable TmovFirmas = new PdfPTable(1);
		TmovFirmas.setWidthPercentage(100);
		PdfPCell celdaFirma = new PdfPCell();
		celdaFirma.setBorder(Rectangle.NO_BORDER);
		celdaFirma.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		
		celdaFirma.setPhrase(new Phrase("Fin del reporte",fontH0));
		TmovFirmas.addCell(celdaFirma);
		
		document.add(Tenc);
		document.add(TdatosEmpresa);
		document.add(TmovInventario);
		//document.add(TcreadoPor);
		document.add(TVentasBod);
		document.add(TmovFirmas);
		
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline");
		
		//String filename = movInventario.getNumero() + ".pdf";
	    //response.setContentType("application/pdf");
	    //response.setHeader("Content-Disposition", "attachment; filename=" + filename);
		
	}

}

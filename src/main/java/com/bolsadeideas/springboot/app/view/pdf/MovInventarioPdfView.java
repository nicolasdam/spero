package com.bolsadeideas.springboot.app.view.pdf;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioDet;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Controller("mov_inventarios/imp-pdf")
public class MovInventarioPdfView extends AbstractPdfView{

	private static final BaseFont Currier = null;

	File file = null;
    BufferedInputStream input = null;
    BufferedOutputStream output = null;

	@Override
	protected void  buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DatosEmpresa datosEmpresa = (DatosEmpresa) model.get("datosEmpresa");
		MovInventario movInventario = (MovInventario) model.get("movInventario");
		Usuario usuario = (Usuario) model.get("usuario");
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String strFechaAct = sdf.format(cal.getTime());
		
		String strTitulo = "";
		
		Font fontH0 = new Font(Currier, 5, Font.NORMAL);
		Font fontH1 = new Font(Currier, 8, Font.NORMAL);
		Font fontH2 = new Font(Currier, 6, Font.BOLD);
		Font fontH3 = new Font(Currier, 6, Font.NORMAL);
		Font fontH4 = new Font(Currier, 10, Font.BOLD);
		
		PdfPTable Tenc = new PdfPTable(1);
		Tenc.setWidthPercentage(100);
		PdfPCell celdaEnc = new PdfPCell();
		
		//datos del encabezado
		celdaEnc.setPhrase(new Phrase(strFechaAct,fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		celdaEnc.setPhrase(new Phrase("Imprime: " + usuario.getNombres() + "" + usuario.getApellidos(),fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		//datos de la empresa
		PdfPTable TdatosEmpresa = new PdfPTable(1);
		PdfPCell celdaEmpresa = new PdfPCell();
		
		TdatosEmpresa.setWidthPercentage(50);
		TdatosEmpresa.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		TdatosEmpresa.setSpacingAfter(10);
		
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getProyecto(),fontH4));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(1);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getRazonSocial(),fontH1));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getNit(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getDireccion(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getTelefono() + " - " + datosEmpresa.getMovil(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(1);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		//datos encabezado del movimiento de inventario
		PdfPTable TmovInventario = new PdfPTable(6);
		PdfPCell celdaEncMov = new PdfPCell();
		TmovInventario.setWidthPercentage(100);
		TmovInventario.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		
		//ENCABEZADO
		if(movInventario.getTipoMovimiento().getTipoMov().equals("E")) {
			strTitulo = "Entreda de Inventario";
		}else if(movInventario.getTipoMovimiento().getTipoMov().equals("S")) {
			strTitulo = "Salida de Inventario";
		}else if(movInventario.getTipoMovimiento().getTipoMov().equals("T")) {
			strTitulo = "Traslado de Inventario";
		}
		
		PdfPCell celdaTitEncMov = new PdfPCell();
		celdaTitEncMov.setPhrase(new Phrase(strTitulo + " No.: " + movInventario.getNumero(),fontH4));
		celdaTitEncMov.setColspan(6);
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		celdaEncMov.setColspan(1);
		celdaEncMov.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		
		//FILA 1
		celdaEncMov.setPhrase(new Phrase("Doc. Soporte: ",fontH1));
		TmovInventario.addCell(celdaEncMov);
		
		celdaEncMov.setPhrase(new Phrase(movInventario.getDocSoporte(),fontH1));
		celdaEncMov.setColspan(3);
		TmovInventario.addCell(celdaEncMov);
		
		celdaEncMov.setPhrase(new Phrase("Fecha: ",fontH1));
		celdaEncMov.setColspan(1);
		TmovInventario.addCell(celdaEncMov);
		
		String strFechaMov = sdf.format(movInventario.getFechaRegistro());
		celdaEncMov.setPhrase(new Phrase(strFechaMov,fontH1));
		TmovInventario.addCell(celdaEncMov);
		
		//FILA 2
		celdaEncMov.setPhrase(new Phrase("Proveedor: ",fontH1));
		TmovInventario.addCell(celdaEncMov);
		
		celdaEncMov.setPhrase(new Phrase(movInventario.getProveedor().getNombre(),fontH1));
		celdaEncMov.setColspan(3);
		TmovInventario.addCell(celdaEncMov);
		
		celdaEncMov.setPhrase(new Phrase("Estado:",fontH1));
		celdaEncMov.setColspan(1);
		TmovInventario.addCell(celdaEncMov);
		
		celdaEncMov.setPhrase(new Phrase(movInventario.getEstadoMov().getDescripcion(),fontH1));
		TmovInventario.addCell(celdaEncMov);
		
		//FILA 3
		celdaEncMov.setPhrase(new Phrase("Tipo Movimiento:",fontH1));
		TmovInventario.addCell(celdaEncMov);
		
		celdaEncMov.setPhrase(new Phrase(movInventario.getTipoMovimiento().getDescripcion(),fontH1));
		celdaEncMov.setColspan(3);
		TmovInventario.addCell(celdaEncMov);
		
		celdaEncMov.setPhrase(new Phrase("Fecha Recibo:",fontH1));
		celdaEncMov.setColspan(1);
		TmovInventario.addCell(celdaEncMov);
		
		String fechaRec = "";
		if(movInventario.getFechaRecibe() != null) {
			fechaRec = sdf.format(movInventario.getFechaRecibe());
		}
		celdaEncMov.setPhrase(new Phrase(fechaRec,fontH1));
		TmovInventario.addCell(celdaEncMov);
		
		if(movInventario.getTipoMovimiento().getTipoMov().equals("T")) {
			celdaEncMov.setPhrase(new Phrase("Bodega Origen:",fontH1));
			celdaEncMov.setColspan(1);
			TmovInventario.addCell(celdaEncMov);
			
			celdaEncMov.setPhrase(new Phrase(movInventario.getBodegaOrigen().getDescripcion(),fontH1));
			celdaEncMov.setColspan(2);
			TmovInventario.addCell(celdaEncMov);
			
			celdaEncMov.setPhrase(new Phrase("Bodega Destino:",fontH1));
			celdaEncMov.setColspan(1);
			TmovInventario.addCell(celdaEncMov);
			
			celdaEncMov.setPhrase(new Phrase(movInventario.getBodegaDestino().getDescripcion(),fontH1));
			celdaEncMov.setColspan(2);
			TmovInventario.addCell(celdaEncMov);
		}else {
			celdaEncMov.setPhrase(new Phrase("Bodega Destino:",fontH1));
			TmovInventario.addCell(celdaEncMov);
			
			celdaEncMov.setPhrase(new Phrase(movInventario.getBodegaOrigen().getDescripcion(),fontH1));
			celdaEncMov.setColspan(5);
			TmovInventario.addCell(celdaEncMov);
		}
		
		//FILA 4
		celdaEncMov.setPhrase(new Phrase("Observaciones:",fontH1));
		celdaEncMov.setColspan(1);
		TmovInventario.addCell(celdaEncMov);
		
		celdaEncMov.setPhrase(new Phrase(movInventario.getObservaciones(),fontH1));
		celdaEncMov.setColspan(5);
		TmovInventario.addCell(celdaEncMov);
		
		if(movInventario.getTipoMovimiento().getTipoMov().equals("T")) {
			celdaEncMov.setPhrase(new Phrase("Observaciones Rec:",fontH1));
			TmovInventario.addCell(celdaEncMov);
			
			celdaEncMov.setPhrase(new Phrase(movInventario.getObservacionesRecibe(),fontH1));
			celdaEncMov.setColspan(5);
			TmovInventario.addCell(celdaEncMov);
		}
		
		PdfPTable TcreadoPor = new PdfPTable(1);
		TcreadoPor.setWidthPercentage(100);
		TcreadoPor.setSpacingAfter(10);
		PdfPCell celdaCreadoPor = new PdfPCell();
		
		celdaCreadoPor.setPhrase(new Phrase("Creado por: " + movInventario.getUsuario().getNombres() +  " " + movInventario.getUsuario().getApellidos(),fontH0));
		celdaCreadoPor.setBorder(Rectangle.NO_BORDER);
		celdaCreadoPor.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		TcreadoPor.addCell(celdaCreadoPor);
		
		if(movInventario.getTipoMovimiento().getTipoMov().equals("T") && movInventario.getEstadoMov().getEstadoId() == 4) {
			celdaCreadoPor.setPhrase(new Phrase("Recibido por: " + movInventario.getUsuarioRecibe().getNombres() +  " " + movInventario.getUsuarioRecibe().getApellidos(),fontH0));
			celdaCreadoPor.setBorder(Rectangle.NO_BORDER);
			celdaCreadoPor.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TcreadoPor.addCell(celdaCreadoPor);
		}
		
		//datos titulo detalle del movimiento de inventario
		PdfPTable TmovInventarioDet = new PdfPTable(5);
		TmovInventarioDet.setWidths(new float [] {1,2.5f,1,1,1});
		TmovInventarioDet.setWidthPercentage(100);
		TmovInventarioDet.setSpacingAfter(60);
		
		PdfPCell celdaTitulo = new PdfPCell();
		PdfPCell celdaTituloItem = new PdfPCell();
		
		celdaTitulo.setPhrase(new Phrase("Detalle Movimiento",fontH2));
		celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		celdaTitulo.setBorder(Rectangle.NO_BORDER);
		celdaTitulo.setColspan(6);
		TmovInventarioDet.addCell(celdaTitulo);
		
		celdaTituloItem.setPhrase(new Phrase("Codigo",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Producto",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Costo",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Cantidad",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Total",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		//datos detalle del movimiento de inventario
		for(MovInventarioDet item: movInventario.getItems()) {
			PdfPCell celdaItem = new PdfPCell();
			
			celdaItem.setPhrase(new Phrase(item.getMaestra().getCodigo(),fontH3));
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getMaestra().getNombre(),fontH3));
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getValorCosto().toString(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getCantidad().toString(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.totalItemForm(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TmovInventarioDet.addCell(celdaItem);
		}
		
		PdfPCell celdaTotal = new PdfPCell(new Phrase("Total",fontH1));
		celdaTotal.setColspan(4);
		celdaTotal.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		TmovInventarioDet.addCell(celdaTotal);
		PdfPCell celdaValorTotal = new PdfPCell(new Phrase(movInventario.getTotalOrden(),fontH1));
		celdaValorTotal.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		TmovInventarioDet.addCell(celdaValorTotal);
		
		PdfPTable TmovFirmas = new PdfPTable(2);
		TmovFirmas.setWidthPercentage(100);
		PdfPCell celdaFirma = new PdfPCell();
		celdaFirma.setBorder(Rectangle.NO_BORDER);
		celdaFirma.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		
		celdaFirma.setPhrase(new Phrase("_______________________________________",fontH1));
		TmovFirmas.addCell(celdaFirma);
		celdaFirma.setPhrase(new Phrase("_______________________________________",fontH1));
		TmovFirmas.addCell(celdaFirma);
		celdaFirma.setPhrase(new Phrase("Autorizó",fontH1));
		TmovFirmas.addCell(celdaFirma);
		celdaFirma.setPhrase(new Phrase("Aprobó",fontH1));
		TmovFirmas.addCell(celdaFirma);
		
		document.add(Tenc);
		document.add(TdatosEmpresa);
		document.add(TmovInventario);
		document.add(TcreadoPor);
		document.add(TmovInventarioDet);
		document.add(TmovFirmas);
		
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline");
		
		//String filename = movInventario.getNumero() + ".pdf";
	    //response.setContentType("application/pdf");
	    //response.setHeader("Content-Disposition", "attachment; filename=" + filename);
		
	}

}

package com.bolsadeideas.springboot.app.view.pdf;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.SaldoBodega;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Controller("/mov_inventarios/reportes/rep-saldo-pdf")
public class RepSaldosPdfView extends AbstractPdfView{

	private static final BaseFont Currier = null;

	File file = null;
    BufferedInputStream input = null;
    BufferedOutputStream output = null;

	@Override
	@SuppressWarnings("unchecked")
	protected void  buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DatosEmpresa datosEmpresa = (DatosEmpresa) model.get("datosEmpresa");
		Usuario usuario = (Usuario) model.get("usuario");
		
		List<Bodega> listBodega = (List<Bodega>) model.get("listBodega");
		//SaldosInventarioRep datosRep = (SaldosInventarioRep) model.get("saldosInventarioRep");

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strFechaAct = sdf.format(cal.getTime());
		
		String strTitulo = "SALDOS POR BODEGA";
		
		Font fontH0 = new Font(Currier, 5, Font.NORMAL);
		Font fontH1 = new Font(Currier, 8, Font.NORMAL);
		Font fontH11 = new Font(Currier, 8, Font.BOLD);
		Font fontH2 = new Font(Currier, 6, Font.BOLD);
		Font fontH3 = new Font(Currier, 6, Font.NORMAL);
		Font fontH4 = new Font(Currier, 10, Font.BOLD);
		
		PdfPTable Tenc = new PdfPTable(1);
		Tenc.setWidthPercentage(100);
		PdfPCell celdaEnc = new PdfPCell();
		
		//datos del encabezado
		celdaEnc.setPhrase(new Phrase(strFechaAct,fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		celdaEnc.setPhrase(new Phrase("Imprime: " + usuario.getNombres() + "" + usuario.getApellidos(),fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		//datos de la empresa
		PdfPTable TdatosEmpresa = new PdfPTable(1);
		PdfPCell celdaEmpresa = new PdfPCell();
		
		TdatosEmpresa.setWidthPercentage(50);
		TdatosEmpresa.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		TdatosEmpresa.setSpacingAfter(10);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getProyecto(),fontH4));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(1);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getRazonSocial(),fontH1));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getNit(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getDireccion(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getTelefono() + " - " + datosEmpresa.getMovil(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(1);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		//datos encabezado del movimiento de inventario
		PdfPTable TmovInventario = new PdfPTable(8);
		//PdfPCell celdaEncMov = new PdfPCell();
		TmovInventario.setWidthPercentage(100);
		TmovInventario.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		
		PdfPCell celdaTitEncMov = new PdfPCell();
		celdaTitEncMov.setPhrase(new Phrase(strTitulo,fontH4));
		celdaTitEncMov.setColspan(8);
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		//datos titulo detalle del movimiento de inventario
		PdfPTable TsaldosInv = new PdfPTable(5);
		TsaldosInv.setWidths(new float [] {1,4,2,2,2});
		TsaldosInv.setWidthPercentage(100);
		TsaldosInv.setSpacingAfter(5);
		
		for(Bodega bodega: listBodega) {
			
			//Bodega
			celdaTitEncMov.setPhrase(new Phrase("Bodega: " + bodega.getDescripcion(),fontH11));
			celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
			celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
			TsaldosInv.addCell(celdaTitEncMov);
			
			PdfPCell celdaTitulo = new PdfPCell();
			
			celdaTitulo.setPhrase(new Phrase("Codigo",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TsaldosInv.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase("Descripción",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TsaldosInv.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase("Cantidad",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TsaldosInv.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase("Costo Prom.",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TsaldosInv.addCell(celdaTitulo);
			
			celdaTitulo.setPhrase(new Phrase("Costo Total",fontH2));
			celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celdaTitulo.setBackgroundColor(new Color(184,218,255));
			TsaldosInv.addCell(celdaTitulo);
			
			//datos detalle del movimiento de inventario
			for(SaldoBodega item: bodega.getSaldoBodega()) {
				PdfPCell celdaItem = new PdfPCell();
				
				celdaItem.setPhrase(new Phrase(item.getMaestra().getCodigo(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
				TsaldosInv.addCell(celdaItem);
				
				celdaItem.setPhrase(new Phrase(item.getMaestra().getNombre(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
				TsaldosInv.addCell(celdaItem);
				
				celdaItem.setPhrase(new Phrase(item.getCantidadFormat(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
				TsaldosInv.addCell(celdaItem);
				
				celdaItem.setPhrase(new Phrase(item.getMaestra().getCostoFormat(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
				TsaldosInv.addCell(celdaItem);
				
				celdaItem.setPhrase(new Phrase(item.costoTotal(),fontH3));
				celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
				TsaldosInv.addCell(celdaItem);
				
			}
		}
		
		//datos titulo detalle del movimiento de inventario
		PdfPTable TkardexTotal = new PdfPTable(8);
		TkardexTotal.setWidths(new float [] {1,1,1,2,1,1,1,1});
		TkardexTotal.setWidthPercentage(100);
		TkardexTotal.setSpacingAfter(20);
		
		
		PdfPTable TmovFirmas = new PdfPTable(1);
		TmovFirmas.setWidthPercentage(100);
		PdfPCell celdaFirma = new PdfPCell();
		celdaFirma.setBorder(Rectangle.NO_BORDER);
		celdaFirma.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		
		celdaFirma.setPhrase(new Phrase("Fin del reporte",fontH0));
		TmovFirmas.addCell(celdaFirma);
		
		document.add(Tenc);
		document.add(TdatosEmpresa);
		document.add(TmovInventario);
		//document.add(TcreadoPor);
		document.add(TsaldosInv);
		document.add(TkardexTotal);
		document.add(TmovFirmas);
		
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline");
		
		//String filename = movInventario.getNumero() + ".pdf";
	    //response.setContentType("application/pdf");
	    //response.setHeader("Content-Disposition", "attachment; filename=" + filename);
		
	}

}

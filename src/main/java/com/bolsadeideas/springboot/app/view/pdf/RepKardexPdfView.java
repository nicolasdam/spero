package com.bolsadeideas.springboot.app.view.pdf;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.KardexInventarioRep;
import com.bolsadeideas.springboot.app.models.entity.KardexMov;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Controller("/mov_inventarios/reportes/rep-kardex-pdf")
public class RepKardexPdfView extends AbstractPdfView{

	private static final BaseFont Currier = null;

	File file = null;
    BufferedInputStream input = null;
    BufferedOutputStream output = null;

	@Override
	@SuppressWarnings("unchecked")
	protected void  buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DatosEmpresa datosEmpresa = (DatosEmpresa) model.get("datosEmpresa");
		Usuario usuario = (Usuario) model.get("usuario");
		Bodega bodega = (Bodega) model.get("bodega");
		Maestra maestra = (Maestra) model.get("maestra");
		List<KardexMov> listKardexMov = (List<KardexMov>) model.get("listKardexMov");
		KardexInventarioRep datosRep = (KardexInventarioRep) model.get("kardexInventarioRep");
		KardexMov kardexFinal = (KardexMov) model.get("kardexFinal");

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String strFechaAct = sdf.format(cal.getTime());
		
		String strTitulo = "";
		String strEntrada = "";
		String strEnt = "E";
		String strSalida = "";
		
		Font fontH0 = new Font(Currier, 5, Font.NORMAL);
		Font fontH1 = new Font(Currier, 8, Font.NORMAL);
		Font fontH2 = new Font(Currier, 6, Font.BOLD);
		Font fontH3 = new Font(Currier, 6, Font.NORMAL);
		Font fontH4 = new Font(Currier, 10, Font.BOLD);
		
		PdfPTable Tenc = new PdfPTable(1);
		Tenc.setWidthPercentage(100);
		PdfPCell celdaEnc = new PdfPCell();
		
		//datos del encabezado
		celdaEnc.setPhrase(new Phrase(strFechaAct,fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		celdaEnc.setPhrase(new Phrase("Imprime: " + usuario.getNombres() + "" + usuario.getApellidos(),fontH0));
		celdaEnc.setBorder(Rectangle.NO_BORDER);
		celdaEnc.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		Tenc.addCell(celdaEnc);
		
		//datos de la empresa
		PdfPTable TdatosEmpresa = new PdfPTable(1);
		PdfPCell celdaEmpresa = new PdfPCell();
		
		TdatosEmpresa.setWidthPercentage(50);
		TdatosEmpresa.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		TdatosEmpresa.setSpacingAfter(10);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getProyecto(),fontH4));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(1);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getRazonSocial(),fontH1));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getNit(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getDireccion(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(0);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		celdaEmpresa.setPhrase(new Phrase(datosEmpresa.getTelefono() + " - " + datosEmpresa.getMovil(),fontH2));
		celdaEmpresa.setBackgroundColor(new Color(184,218,255));
		celdaEmpresa.setBorderWidthTop(0);
		celdaEmpresa.setBorderWidthBottom(1);
		celdaEmpresa.setBorderWidthLeft(1);
		celdaEmpresa.setBorderWidthRight(1);
		TdatosEmpresa.addCell(celdaEmpresa);
		
		//datos encabezado del movimiento de inventario
		PdfPTable TmovInventario = new PdfPTable(8);
		//PdfPCell celdaEncMov = new PdfPCell();
		TmovInventario.setWidthPercentage(100);
		TmovInventario.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
		
		strTitulo = "REPORTE KARDEX DE INVENTARIO";
		PdfPCell celdaTitEncMov = new PdfPCell();
		celdaTitEncMov.setPhrase(new Phrase(strTitulo,fontH4));
		celdaTitEncMov.setColspan(8);
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		celdaTitEncMov.setPhrase(new Phrase("Fecha Inicial: " + datosRep.getFechaInicial() + " - Fecha Final: " + datosRep.getFechaFinal(),fontH3));
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		TmovInventario.addCell(celdaTitEncMov);
		
		//Bodega
		celdaTitEncMov.setPhrase(new Phrase("Bodega: " + bodega.getDescripcion(),fontH2));
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		TmovInventario.addCell(celdaTitEncMov);
		//Maestra
		celdaTitEncMov.setPhrase(new Phrase("Maestra: " + maestra.getNombre(),fontH2));
		celdaTitEncMov.setBackgroundColor(new Color(184,218,255));
		celdaTitEncMov.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		TmovInventario.addCell(celdaTitEncMov);
		
		//datos titulo detalle del movimiento de inventario
		PdfPTable TmovInventarioDet = new PdfPTable(9);
		TmovInventarioDet.setWidths(new float [] {1,1,1,2,1,1,1,1,1});
		TmovInventarioDet.setWidthPercentage(100);
		TmovInventarioDet.setSpacingAfter(5);
		
		PdfPCell celdaTitulo = new PdfPCell();
		PdfPCell celdaTituloItem = new PdfPCell();
		
		celdaTitulo.setPhrase(new Phrase("Detalle Movimiento",fontH2));
		celdaTitulo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		celdaTitulo.setBorder(Rectangle.NO_BORDER);
		celdaTitulo.setColspan(9);
		TmovInventarioDet.addCell(celdaTitulo);
		
		
		celdaTituloItem.setPhrase(new Phrase("Documento",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Soporte",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Tipo",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Fecha",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);

		celdaTituloItem.setPhrase(new Phrase("Costo",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("S. Anterior",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Entrada",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("Salida",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		celdaTituloItem.setPhrase(new Phrase("S. Nuevo",fontH2));
		celdaTituloItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
		celdaTituloItem.setBackgroundColor(new Color(184,218,255));
		TmovInventarioDet.addCell(celdaTituloItem);
		
		//datos detalle del movimiento de inventario
		for(KardexMov item: listKardexMov) {
			PdfPCell celdaItem = new PdfPCell();
			
			celdaItem.setPhrase(new Phrase(item.getDocumento(),fontH3));
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getSoporte(),fontH3));
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getTipoMov(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getFechaFormat(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getCostoPromFormat(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getSaldoAnteriorFormat(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TmovInventarioDet.addCell(celdaItem);
			
			if(item.getTipoMov().equals(strEnt)) {
				strEntrada = item.getCantidadFormat();
				strSalida = "";
			}else {
				strEntrada = "";
				strSalida = item.getCantidadFormat();
			}
			
			celdaItem.setPhrase(new Phrase(strEntrada,fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(strSalida,fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TmovInventarioDet.addCell(celdaItem);
			
			celdaItem.setPhrase(new Phrase(item.getSaldoNuevoFormat(),fontH3));
			celdaItem.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
			TmovInventarioDet.addCell(celdaItem);
		}
		
		//datos titulo detalle del movimiento de inventario
		PdfPTable TkardexTotal = new PdfPTable(9);
		TkardexTotal.setWidths(new float [] {1,1,1,2,1,1,1,1,1});
		TkardexTotal.setWidthPercentage(100);
		TkardexTotal.setSpacingAfter(20);
		
		PdfPCell CkardexTotal = new PdfPCell();
		CkardexTotal.setPhrase(new Phrase("Saldo Final: ",fontH2));
		CkardexTotal.setColspan(8);
		CkardexTotal.setBackgroundColor(new Color(184,218,255));
		CkardexTotal.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		TkardexTotal.addCell(CkardexTotal);
		
		CkardexTotal.setPhrase(new Phrase(kardexFinal.getSaldoNuevoFormat(),fontH2));
		CkardexTotal.setColspan(1);
		CkardexTotal.setBackgroundColor(new Color(184,218,255));
		CkardexTotal.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
		TkardexTotal.addCell(CkardexTotal);
		
		PdfPTable TmovFirmas = new PdfPTable(1);
		TmovFirmas.setWidthPercentage(100);
		PdfPCell celdaFirma = new PdfPCell();
		celdaFirma.setBorder(Rectangle.NO_BORDER);
		celdaFirma.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
		
		celdaFirma.setPhrase(new Phrase("Fin del reporte",fontH0));
		TmovFirmas.addCell(celdaFirma);
		
		document.add(Tenc);
		document.add(TdatosEmpresa);
		document.add(TmovInventario);
		//document.add(TcreadoPor);
		document.add(TmovInventarioDet);
		document.add(TkardexTotal);
		document.add(TmovFirmas);
		
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline");
		
		//String filename = movInventario.getNumero() + ".pdf";
	    //response.setContentType("application/pdf");
	    //response.setHeader("Content-Disposition", "attachment; filename=" + filename);
		
	}

}

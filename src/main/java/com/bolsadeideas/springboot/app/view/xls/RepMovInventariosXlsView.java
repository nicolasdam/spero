package com.bolsadeideas.springboot.app.view.xls;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioRep;
import com.bolsadeideas.springboot.app.models.entity.Usuario;

@Controller("/mov_inventarios/reportes/rep-mov.xlsx")
public class RepMovInventariosXlsView extends AbstractXlsxView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		response.setHeader("Content-Disposition", "attachment; filename=\"Reporte_Mov_Inventarios.xlsx\"");
		DatosEmpresa datosEmpresa = (DatosEmpresa) model.get("datosEmpresa");
		Usuario usuario = (Usuario) model.get("usuario");
		List<MovInventario> listMovimientos = new ArrayList<>();
		listMovimientos = (List<MovInventario>) model.get("listMovimientos");
		MovInventarioRep datosRep = (MovInventarioRep) model.get("movInventarioRep");
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String strFechaAct = sdf.format(cal.getTime());
		
		Sheet sheet = workbook.createSheet("Movimientos");
		
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue(datosEmpresa.getProyecto());
		cell = row.createCell(6);
		cell.setCellValue(strFechaAct);
		
		row = sheet.createRow(1);
		cell = row.createCell(0);
		cell.setCellValue(datosEmpresa.getRazonSocial());
		cell = row.createCell(6);
		cell.setCellValue("Imprime: " + usuario.getNombres() + "" + usuario.getApellidos());
		
		sheet.createRow(2).createCell(0).setCellValue(datosEmpresa.getNit());
		sheet.createRow(3).createCell(0).setCellValue(datosEmpresa.getDireccion());
		sheet.createRow(4).createCell(0).setCellValue(datosEmpresa.getTelefono() + " - " + datosEmpresa.getMovil());
		
		int numRow = 6;
		sheet.createRow(numRow ++).createCell(0).setCellValue("REPORTE MOVIMIENTOS DE INVENTARIO");
		
		Row reporteBodega = sheet.createRow(numRow ++);
		reporteBodega.createCell(0).setCellValue("Bodega:");
		reporteBodega.createCell(1).setCellValue(datosRep.getBodega().getDescripcion());
		
		String tipoMov;
		switch(datosRep.getTipoMov()) {
		  case "0":
			  tipoMov = "Todos los Movimientos";
		    break;
		  case "E":
			  tipoMov = "Entradas";
		    break;
		  case "S":
			  tipoMov = "Salidas";
		    break;
		  case "T":
			  tipoMov = "Traslados";
		    break;
		  default:
			  tipoMov = "Todos los Movimientos";
		}
		Row reporteTipo = sheet.createRow(numRow ++);
		reporteTipo.createCell(0).setCellValue("Tipo de Movimiento:");
		reporteTipo.createCell(1).setCellValue(tipoMov);
		
		if(datosRep.getProveedor() != null) {
			Row reportePro = sheet.createRow(numRow ++);
			reportePro.createCell(0).setCellValue("Proveedor:");
			reportePro.createCell(1).setCellValue(datosRep.getProveedor().getNombre());
		}
		
		Row reportDateIni = sheet.createRow(numRow ++);
		reportDateIni.createCell(0).setCellValue("Fecha Inicial:");
		reportDateIni.createCell(1).setCellValue(datosRep.getFechaInicial());
		
		Row reportDateFin = sheet.createRow(numRow ++);
		reportDateFin.createCell(0).setCellValue("Fecha Final:");
		reportDateFin.createCell(1).setCellValue(datosRep.getFechaFinal());
		
		numRow ++;
		sheet.createRow(numRow ++).createCell(0).setCellValue("Detalle Movimiento");
		
		CellStyle tHeaderStyle = workbook.createCellStyle();
		tHeaderStyle.setBorderBottom(BorderStyle.MEDIUM);
		tHeaderStyle.setBorderTop(BorderStyle.MEDIUM);
		tHeaderStyle.setBorderRight(BorderStyle.MEDIUM);
		tHeaderStyle.setBorderLeft(BorderStyle.MEDIUM);
		tHeaderStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.index);
		tHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		CellStyle tBodyStyle = workbook.createCellStyle();
		tBodyStyle.setBorderBottom(BorderStyle.THIN);
		tBodyStyle.setBorderTop(BorderStyle.THIN);
		tBodyStyle.setBorderRight(BorderStyle.THIN);
		tBodyStyle.setBorderLeft(BorderStyle.THIN);
		
		Row header = sheet.createRow(numRow ++);
		header.createCell(0).setCellValue("Numero");
		header.createCell(1).setCellValue("Soporte");
		header.createCell(2).setCellValue("Proveedor");
		header.createCell(3).setCellValue("Fecha Mov");
		header.createCell(4).setCellValue("Creado por");
		header.createCell(5).setCellValue("Observaciones");
		header.createCell(6).setCellValue("Estado");
		
		header.getCell(0).setCellStyle(tHeaderStyle);
		header.getCell(1).setCellStyle(tHeaderStyle);
		header.getCell(2).setCellStyle(tHeaderStyle);
		header.getCell(3).setCellStyle(tHeaderStyle);
		header.getCell(4).setCellStyle(tHeaderStyle);
		header.getCell(5).setCellStyle(tHeaderStyle);
		header.getCell(6).setCellStyle(tHeaderStyle);
		
		//datos detalle del movimiento de inventario
		for(MovInventario item: listMovimientos) {
			Row detail = sheet.createRow(numRow ++);
			cell = detail.createCell(0);
			cell.setCellValue(item.getNumero());
			cell.setCellStyle(tBodyStyle);
			
			cell = detail.createCell(1);
			cell.setCellValue(item.getDocSoporte());
			cell.setCellStyle(tBodyStyle);
			
			cell = detail.createCell(2);
			cell.setCellValue(item.getProveedor().getNombre());
			cell.setCellStyle(tBodyStyle);
			
			cell = detail.createCell(3);
			cell.setCellValue(item.getFechaFormat());
			cell.setCellStyle(tBodyStyle);
			
			cell = detail.createCell(4);
			cell.setCellValue(item.getUsuario().getNombres() + " " + item.getUsuario().getApellidos());
			cell.setCellStyle(tBodyStyle);
			
			cell = detail.createCell(4);
			cell.setCellValue(item.getUsuario().getNombres() + " " + item.getUsuario().getApellidos());
			cell.setCellStyle(tBodyStyle);
			
			cell = detail.createCell(5);
			cell.setCellValue(item.getObservaciones());
			cell.setCellStyle(tBodyStyle);
			
			cell = detail.createCell(6);
			cell.setCellValue(item.getEstadoMov().getDescripcion());
			cell.setCellStyle(tBodyStyle);

		}
		Row endReport = sheet.createRow(numRow ++);
		endReport.createCell(0).setCellValue("Fin del reporte");
	}

}

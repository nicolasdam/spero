package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Modulo;
import com.bolsadeideas.springboot.app.models.entity.Opcion;
import com.bolsadeideas.springboot.app.models.service.IModuloService;
import com.bolsadeideas.springboot.app.models.service.IOpcionService;

@Controller
@SessionAttributes("opcion")
public class OpcionController {
	
	@Autowired
	IOpcionService opcionService;
	
	@Autowired
	IModuloService moduloService;
	
	@RequestMapping(value = "/opciones/opciones-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		model.addAttribute("moduloMenu", "Administracion");
		model.addAttribute("opcionMenu", "Opciones");
		model.addAttribute("titulo", "Lista de Opciones");
		model.addAttribute("opciones", opcionService.findAllOrder());
		return "opciones/opciones-listar";
	}
	
	@RequestMapping(value = "/opciones/opciones-form")
	public String crear(Map<String, Object> model) {

		List<Modulo> modulos = moduloService.findAll();
		
		Opcion opcion = new Opcion();
		
		model.put("moduloMenu", "Administracion");
		model.put("opcionMenu", "Opciones");
		model.put("opcion", opcion);
		model.put("mod", modulos);
		model.put("titulo", "Registrar Opción");

		return "opciones/opciones-form";
	}
	
	@RequestMapping(value = "/opciones/opciones-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		List<Modulo> modulos = moduloService.findAll();
		
		model.put("moduloMenu", "Administracion");
		model.put("opcionMenu", "Opciones");

		Opcion opcion = null;
		if (id > 0) {
			opcion = opcionService.buscarUno(id);
		} else {
			return "redirect:/opciones/opciones-listar";
		}

		model.put("mod", modulos);
		model.put("opcion", opcion);
		model.put("titulo", "Editar Opción");

		return "opciones/opciones-form";
	}
	
	@RequestMapping(value = "/opciones/opciones-form", method = RequestMethod.POST)
	public String guardar(@Valid Opcion opcion, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		List<Modulo> modulos = moduloService.findAll();
		
		model.addAttribute("moduloMenu", "Administracion");
		model.addAttribute("opcionMenu", "Opciones");
		
		if (validacion.hasErrors()) {

			model.addAttribute("mod", modulos);
			model.addAttribute("titulo", "Registrar Opción");
			return "opciones/opciones-form";
		}

		model.addAttribute("titulo", "Lista de Opciones");
		opcionService.guardar(opcion);
		status.setComplete();
		flash.addFlashAttribute("success","Opción actualizado con éxito");
		return "redirect:/opciones/opciones-listar";
	}
	
	@RequestMapping(value = "/opciones/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		if (id > 0) {
			opcionService.eliminar(id);
		}
		
		model.addAttribute("titulo", "Lista de Opciones");
		model.addAttribute("moduloMenu", "Administracion");
		model.addAttribute("opcionMenu", "Opciones");
		flash.addFlashAttribute("success","Opción eliminada con éxito");
		return "redirect:/opciones/opciones-listar";
	}

}

package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.OrdenCompra;
import com.bolsadeideas.springboot.app.models.entity.OrdenCompraDet;
import com.bolsadeideas.springboot.app.models.entity.Proveedor;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.EstadoOc;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.service.IEstadoOcService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IOrdenCompraService;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;

@Controller
@SessionAttributes("ordenCompra")
public class OrdenCompraControlle {

	@Autowired
	IOrdenCompraService ordenCompraService;

	@Autowired
	IProveedorService proveedorService;

	@Autowired
	IUsuarioDao usuarioDao;

	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IEstadoOcService estadoOcService;

	@RequestMapping(value = "/ordencompra/ordencompra-listar", method = RequestMethod.GET)
	public String listar(Model model) {

		model.addAttribute("moduloMenu", "Compras");
		model.addAttribute("opcionMenu", "Orden de Compra");
		model.addAttribute("titulo", "Lista de Ordenes de Compra");
		model.addAttribute("listOrdenC", ordenCompraService.findAllOrden());
		return "ordencompra/ordencompra-listar";
	}

	@RequestMapping(value = "/ordencompra/ordencompra-form")
	public String crear(Map<String, Object> model, Authentication authentication) {

		List<Proveedor> proveedor = proveedorService.findAll();

		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioDao.findByUsuario(usuarioLog);

		OrdenCompra ordenCompra = new OrdenCompra();
		ordenCompra.setUsuario(usuario);

		model.put("moduloMenu", "Compras");
		model.put("opcionMenu", "Orden de Compra");
		model.put("ordenCompra", ordenCompra);
		model.put("listProveedor", proveedor);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Registrar Orden de Compra");

		return "ordencompra/ordencompra-form";
	}

	@RequestMapping(value = "/ordencompra/ordencompra-ver/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		List<Proveedor> proveedor = proveedorService.findAll();

		OrdenCompra ordenCompra = null;
		if (id > 0) {
			ordenCompra = ordenCompraService.buscarUno(id);
		} else {
			return "redirect:/ordencompra/ordencompra-listar";
		}

		model.put("moduloMenu", "Compras");
		model.put("opcionMenu", "Orden de Compra");
		model.put("listProveedor", proveedor);
		model.put("ordenCompra", ordenCompra);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Editar Orden de Compra");

		return "ordencompra/ordencompra-ver";
	}

	@RequestMapping(value = "/ordencompra/ordencompra-form", method = RequestMethod.POST)
	public String guardar(@Valid OrdenCompra ordenCompra, BindingResult validacion, Model model,
			@RequestParam(name = "maestraID[]", required = false) Long[] maestraFormId,
			@RequestParam(name = "valorCosto[]", required = false) Double[] valorFormCosto,
			@RequestParam(name = "cantidad[]", required = false) Long[] cantidadForm, RedirectAttributes flash,
			SessionStatus status) {

		
		List<Proveedor> proveedor = proveedorService.findAll();
		try {
			if (maestraFormId != null && maestraFormId.length > 0) {
				for (int i = 0; i < maestraFormId.length; i++) {
					Maestra maestraItem = maestraService.buscarUno(maestraFormId[i]);

					OrdenCompraDet linea = new OrdenCompraDet();
					linea.setMaestra(maestraItem);
					linea.setCantidad(cantidadForm[i]);
					linea.setValorCosto(valorFormCosto[i]);

					ordenCompra.addItemOrden(linea);
				}
			}

			model.addAttribute("moduloMenu", "Compras");
			model.addAttribute("opcionMenu", "Orden de Compra");
			model.addAttribute("listProveedor", proveedor);

			if (validacion.hasErrors()) {
				model.addAttribute("ordenCompra", ordenCompra);
				model.addAttribute("titulo", "Registrar Orden de Compra");
				return "ordencompra/ordencompra-form";
			}

			if (maestraFormId == null || maestraFormId.length == 0) {
				model.addAttribute("error", "No registra productos");
				return "ordencompra/ordencompra-form";
			}
			
			Long estadoReg = (long) 1;
			EstadoOc estado = new EstadoOc();
			estado = estadoOcService.buscarUno(estadoReg);
			ordenCompra.setEstadoOc(estado);
			
			model.addAttribute("titulo", "Lista de Ordenes de Compra");
			ordenCompraService.guardar(ordenCompra);
			Long ocId = ordenCompra.getOrdenId();
			status.setComplete();
			flash.addFlashAttribute("success", "Orden de Compra registrada con éxito. No. " + ocId);
			return "redirect:/ordencompra/ordencompra-listar";
		} catch (Exception e) {
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("ordenCompra", ordenCompra);
			model.addAttribute("titulo", "Registrar Orden de Compra");
			model.addAttribute("error", "No fue posible registrar la Orden de Compra, Verifique los costos y cantidades de los productos, recuerde que no pueden ser nulos ni cero(0).");
			return "ordencompra/ordencompra-form";
		}
	}

	@RequestMapping(value = "/ordencompra/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		if (id > 0) {
			ordenCompraService.eliminar(id);
		}

		model.addAttribute("moduloMenu", "Compras");
		model.addAttribute("opcionMenu", "Orden de Compra");
		flash.addFlashAttribute("success", "Orden de Compra eliminada con éxito");
		return "redirect:/ordencompra/ordencompra-listar";
	}
	
	@RequestMapping(value = "/ordencompra/ordencompra-form_id", method = RequestMethod.POST)
	public String guardarId(@Valid OrdenCompra ordenCompra, BindingResult validacion, Model model, RedirectAttributes flash,
			SessionStatus status) {
		
		List<Proveedor> proveedor = proveedorService.findAll();
		try {

			model.addAttribute("moduloMenu", "Compras");
			model.addAttribute("opcionMenu", "Orden de Compra");
			model.addAttribute("listProveedor", proveedor);

			if (validacion.hasErrors()) {
				model.addAttribute("ordenCompra", ordenCompra);
				model.addAttribute("titulo", "Registrar Orden de Compra");
				return "ordencompra/ordencompra-form";
			}

			model.addAttribute("titulo", "Lista de Ordenes de Compra");
			ordenCompraService.guardar(ordenCompra);
			status.setComplete();
			flash.addFlashAttribute("success", "Orden de Compra actualizado con éxito");
			return "redirect:/ordencompra/ordencompra-listar";
		} catch (Exception e) {
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("ordenCompra", ordenCompra);
			model.addAttribute("titulo", "Registrar Orden de Compra");
			model.addAttribute("error", "No fue posible registrar la Orden de Compra, Verifique los costos y cantidades de los productos, recuerde que no pueden ser nulos ni cero(0).");
			return "ordencompra/ordencompra-form";
		}
	}
	
	@RequestMapping(value = "/ordencompra/imprimir/{id}")
	public String imprimir(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		OrdenCompra ordenCompra = null;
		if (id > 0) {
			ordenCompra = ordenCompraService.buscarUno(id);
		} else {
			return "redirect:/ordencompra/ordencompra-listar";
		}
		
		Long estadoReg = (long) 2;
		EstadoOc estado = estadoOcService.buscarUno(estadoReg);
		ordenCompra.setEstadoOc(estado);
		
		model.addAttribute("titulo", "Lista de Ordenes de Compra");
		ordenCompraService.guardar(ordenCompra);
		flash.addFlashAttribute("success", "Orden de Compra impresa con éxito.");
		return "redirect:/ordencompra/ordencompra-listar";
	}
	
	
	
	
	
	

}

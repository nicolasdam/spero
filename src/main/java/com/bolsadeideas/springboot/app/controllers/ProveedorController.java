package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Proveedor;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;

@Controller
@SessionAttributes("proveedor")
public class ProveedorController {
	
	@Autowired
	IProveedorService proveedorService;
	
	@RequestMapping(value = "/proveedor/proveedor-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Proveedores");
		model.addAttribute("titulo", "Lista de proveedores");
		model.addAttribute("proveedores", proveedorService.findAll());
		return "proveedor/proveedor-listar";
	}

	@RequestMapping(value = "/proveedor/proveedor-form")
	public String crear(Map<String, Object> model) {

		Proveedor proveedor = new Proveedor();

		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Proveedores");
		model.put("proveedor", proveedor);
		model.put("titulo", "Registrar Proveedor");

		return "proveedor/proveedor-form";
	}

	@RequestMapping(value = "/proveedor/proveedor-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Proveedor proveedor = null;
		if (id > 0) {
			proveedor = proveedorService.buscarUno(id);
		} else {
			return "redirect:/proveedor/proveedor-listar";
		}
		
		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Proveedores");
		model.put("proveedor", proveedor);
		model.put("titulo", "Editar Proveedor");

		return "proveedor/proveedor-form";
	}

	@RequestMapping(value = "/proveedor/proveedor-form", method = RequestMethod.POST)
	public String guardar(@Valid Proveedor proveedor, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Proveedor");
			return "proveedor/proveedor-form";
		}
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Proveedores");
		proveedorService.guardar(proveedor);
		status.setComplete();
		flash.addFlashAttribute("success","Proveedor actualizado con éxito");
		return "redirect:/proveedor/proveedor-listar";
	}

	@RequestMapping(value = "/proveedor/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash,  Model model) {
		if (id > 0) {
			proveedorService.eliminar(id);
		}
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Proveedores");
		flash.addFlashAttribute("success","Proveedor eliminado con éxito");
		return "redirect:/proveedor/proveedor-listar";
	}

}

package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.GrupoProducto;
import com.bolsadeideas.springboot.app.models.service.IGrupoProductoService;

@Controller
@SessionAttributes("grupoProducto")
public class GrupoProductoController {

	@Autowired
	private IGrupoProductoService grupoProductoService;
	
	@RequestMapping(value = "/grupoproducto/grupoproducto-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Grupo Producto");
		model.addAttribute("titulo", "Lista Grupo Producto");
		model.addAttribute("grupoProductos", grupoProductoService.findAll());
		return "grupoproducto/grupoproducto-listar";
	}

	@RequestMapping(value = "/grupoproducto/grupoproducto-form")
	public String crear(Map<String, Object> model) {

		GrupoProducto grupoProducto = new GrupoProducto();

		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Grupo Producto");
		model.put("grupoProducto", grupoProducto);
		model.put("titulo", "Registrar Grupo Producto");

		return "grupoproducto/grupoproducto-form";
	}

	@RequestMapping(value = "/grupoproducto/grupoproducto-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		GrupoProducto grupoProducto = null;
		if (id > 0) {
			grupoProducto = grupoProductoService.buscarUno(id);
		} else {
			return "redirect:/grupoproducto/grupoproducto-listar";
		}
		
		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Grupo Producto");
		model.put("grupoProducto", grupoProducto);
		model.put("titulo", "Editar Grupo Producto");

		return "grupoproducto/grupoproducto-form";
	}

	@RequestMapping(value = "/grupoproducto/grupoproducto-form", method = RequestMethod.POST)
	public String guardar(@Valid GrupoProducto grupoProducto, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Grupo Producto");
			return "grupoproducto/grupoproducto-form";
		}
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Grupo Producto");
		grupoProductoService.guardar(grupoProducto);
		status.setComplete();
		flash.addFlashAttribute("success","Grupo Producto actualizado con éxito");
		return "redirect:/grupoproducto/grupoproducto-listar";
	}

	@RequestMapping(value = "/grupoproducto/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash,  Model model) {
		if (id > 0) {
			grupoProductoService.eliminar(id);
		}
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Grupo Producto");
		flash.addFlashAttribute("success","Grupo Producto eliminado con éxito");
		return "redirect:/grupoproducto/grupoproducto-listar";
	}
	
}

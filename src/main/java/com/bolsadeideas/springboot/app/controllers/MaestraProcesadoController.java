package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MaestraProcesado;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.IMaestraProcesadoService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("maestraProcesado")
public class MaestraProcesadoController {
	
	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IUsuarioService usuarioService;
	
	@Autowired
	IMaestraProcesadoService maestraProcesadoService;
	
	@Autowired
	IUsuarioDao usuarioDao;
	
	@RequestMapping(value = "/maestra/configuracion/{id}", method = RequestMethod.GET)
	public String listar(@PathVariable(value = "id") Long id, Model model) {
		
		Maestra maestra = null;
		if (id > 0) {
			maestra = maestraService.buscarUno(id);
		} else {
			return "redirect:/maestra/maestra-conf-listar";
		}
		
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Maestra Productos");
		model.addAttribute("titulo", "Lista de Maestras");
		model.addAttribute("maestra", maestra);
		model.addAttribute("maestrasProcesado", maestraProcesadoService.buscarPorMaestraId(maestra));
		return "maestra/maestra-conf-listar";
	}
	
	@RequestMapping(value = "/maestra/maestra-conf-form/{id}")
	public String crear(@PathVariable(value = "id") Long id, Map<String, Object> model, Authentication authentication) {
		
		MaestraProcesado maestraProcesado = new MaestraProcesado();
		
		Maestra maestra = null;
		if (id > 0) {
			maestra = maestraService.buscarUno(id);
		} else {
			return "redirect:/maestra/maestra-conf-listar";
		}
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioDao.findByUsuario(usuarioLog);
		maestraProcesado.setUsuario(usuario);
		maestraProcesado.setMaestraId(maestra);

		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Maestra Productos");
		model.put("maestra", maestra);
		model.put("maestraProcesado", maestraProcesado);
		model.put("titulo", "Registrar Maestra");

		return "maestra/maestra-conf-form";
	}
	
	
	@RequestMapping(value = "/maestra/maestra-conf-form", method = RequestMethod.POST)
	public String guardar(@Valid MaestraProcesado maestraProcesado, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name = "maestraProId") String maestraFormId) {
		
		Long id = maestraProcesado.getMaestraId().getMaestraId();
		
		if (validacion.hasErrors()) {
			
			Maestra maestra = new Maestra();
			maestra = maestraService.buscarUno(id);
			model.addAttribute("moduloMenu", "Inventarios");
			model.addAttribute("opcionMenu", "Configuracion Maestra");
			model.addAttribute("maestra", maestra);
			model.addAttribute("titulo", "Registrar configurar maestra");
			return "maestra/maestra-conf-form";
		}
		
		Maestra maestraNew = new Maestra();
		Long mFrom = null;
		
		if(!maestraFormId.equals("") || maestraFormId != null) {
			mFrom = Long.parseLong(maestraFormId);
		}
		maestraNew = maestraService.buscarUno(mFrom);
		maestraProcesado.setMaestraProId(maestraNew);
		
		maestraProcesadoService.guardar(maestraProcesado);
		
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Maestra Productos");
		status.setComplete();
		flash.addFlashAttribute("success","Configuracion actualizada con éxito");
		return "redirect:/maestra/configuracion/"+ id;
	}
	
	@RequestMapping(value = "/maestra/conf-eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		MaestraProcesado maestraProcesado = maestraProcesadoService.buscarUno(id);
		Long maestraId = maestraProcesado.getMaestraId().getMaestraId();
		
		if (id > 0) {
			maestraProcesadoService.eliminar(id);
		}
		
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Maestra Productos");
		flash.addFlashAttribute("success","Maestra eliminada con éxito");
		return "redirect:/maestra/configuracion/" + maestraId;
	}
	
}

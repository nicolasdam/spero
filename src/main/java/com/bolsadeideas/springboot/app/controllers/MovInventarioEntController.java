package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Proveedor;
import com.bolsadeideas.springboot.app.models.entity.TipoMovimiento;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.EstadoMov;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioDet;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IConsecutivoService;
import com.bolsadeideas.springboot.app.models.service.IEstadoMovService;
import com.bolsadeideas.springboot.app.models.service.IKardexMovService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;
import com.bolsadeideas.springboot.app.models.service.ITipoMovService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("movInventario")
public class MovInventarioEntController {

	@Autowired
	IMovInventarioService movInventarioService;

	@Autowired
	IProveedorService proveedorService;

	@Autowired
	IUsuarioDao usuarioDao;
	
	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IEstadoMovService estadoMovService;
	
	@Autowired
	ITipoMovService tipoMovService;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	IConsecutivoService consecutivoService;
	
	@Autowired
	IKardexMovService kardexMovService;
	
	String moduloMenu = "Inventarios";
	String opcionMenu = "Inventarios Entradas";
	
	String redirect = null;
	String entrada = "E";
	String salida = "S";

	@RequestMapping(value = "/mov_inventarios/entradas/entradas-listar", method = RequestMethod.GET)
	public String listar(Model model, Authentication authentication) {
		
		Long estadoEnt = (long) 1;
		EstadoMov estado = estadoMovService.buscarUno(estadoEnt);
		String tipoMov = "E";
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<MovInventario> movInventarios = new ArrayList<>();
		if(bodegaUsuario == null) {
			movInventarios = movInventarioService.findEstado(estado, tipoMov);
		} else {
			movInventarios = movInventarioService.findMovEstBod(estado, bodegaUsuario, tipoMov);
		}

		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de Entradas de Inventario");
		model.addAttribute("listMovimientos", movInventarios);
		return "mov_inventarios/entradas/entradas-listar";
	}
	
	@RequestMapping(value = "/mov_inventarios/entradas/entradas-form")
	public String crear(Map<String, Object> model, Authentication authentication) {

		MovInventario movInventario = new MovInventario();
		
		List<Proveedor> ListProveedor = proveedorService.findAll();
		String tipoMov = "E";
		List<TipoMovimiento> tipoMovimiento = tipoMovService.buscarPorTipo(tipoMov);
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		List<Bodega> listBodegas = new ArrayList<>();
		if(bodegaUsuario == null) {
			listBodegas = bodegaService.findAll();
		} else {
			listBodegas.add(bodegaUsuario);
			movInventario.setBodegaOrigen(bodegaUsuario);
		}

		movInventario.setUsuario(usuario);

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("movInventario", movInventario);
		model.put("listProveedor", ListProveedor);
		model.put("tipoMovimiento", tipoMovimiento);
		model.put("listBodegas", listBodegas);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Registrar Entrada de Inventario");

		return "mov_inventarios/entradas/entradas-form";
	}
	
	@RequestMapping(value = "/mov_inventarios/entradas/entradas-ver/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, Authentication authentication) {
		
		List<Proveedor> ListProveedor = proveedorService.findAll();
		String tipoMov = "E";
		List<TipoMovimiento> tipoMovimiento = tipoMovService.buscarPorTipo(tipoMov);
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<Bodega> listBodegas = new ArrayList<>();
		if(bodegaUsuario == null) {
			listBodegas = bodegaService.findAll();
		} else {
			listBodegas.add(bodegaUsuario);
		}

		MovInventario movInventario = null;
		if (id > 0) {
			movInventario = movInventarioService.buscarUno(id);
		} else {
			return "redirect:/mov_inventarios/entradas/entradas-listar";
		}
		
		if(movInventario.getEstadoMov().getEstadoId() != 1) {
			return "redirect:/mov_inventarios/entradas/entradas-listar";
		}
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("movInventario", movInventario);
		model.put("listProveedor", ListProveedor);
		model.put("tipoMovimiento", tipoMovimiento);
		model.put("listBodegas", listBodegas);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Editar Entrada de Inventario");
		
		String tipoMovi = movInventario.getTipoMovimiento().getTipoMov();
		
		if (tipoMovi.equals(entrada)) {
			redirect = "mov_inventarios/entradas/entradas-ver";
		}else if (tipoMovi.equals(salida)) {
			redirect = "mov_inventarios/salidas/salidas-ver";
		}
		return redirect;
		
		//return "mov_inventarios/entradas/entradas-ver";
	}
	
	@RequestMapping(value = "/mov_inventarios/entradas/entradas-imp/{id}")
	public String mostrar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		List<Proveedor> ListProveedor = proveedorService.findAll();
		String tipoMov = "E";
		List<TipoMovimiento> tipoMovimiento = tipoMovService.buscarPorTipo(tipoMov);
		List<Bodega> listBodegas = bodegaService.findAll();

		MovInventario movInventario = null;
		if (id > 0) {
			movInventario = movInventarioService.buscarUno(id);
		} else {
			return "redirect:/ordencompra/ordencompra-listar";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("movInventario", movInventario);
		model.put("listProveedor", ListProveedor);
		model.put("tipoMovimiento", tipoMovimiento);
		model.put("listBodegas", listBodegas);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Editar Entrada de Inventario");
		
		String tipoMovi = movInventario.getTipoMovimiento().getTipoMov();
		
		if (tipoMovi.equals(entrada)) {
			redirect = "mov_inventarios/entradas/entradas-imp";
		}else if (tipoMovi.equals(salida)) {
			redirect = "mov_inventarios/salidas/salidas-imp";
		}
		return redirect;
		
		//return "mov_inventarios/entradas/entradas-ver";
	}
	
	@RequestMapping(value = "/mov_inventarios/entradas/entradas-form", method = RequestMethod.POST)
	public String guardar(@Valid MovInventario movInventario, BindingResult validacion, Model model,
			@RequestParam(name = "maestraID[]", required = false) Long[] maestraFormId,
			@RequestParam(name = "valorCosto[]", required = false) Double[] valorFormCosto,
			@RequestParam(name = "cantidad[]", required = false) Double[] cantidadForm, RedirectAttributes flash,
			SessionStatus status) {
		
		List<Proveedor> proveedor = proveedorService.findAll();
		String tipoMov = "E";
		List<TipoMovimiento> tipoMovimiento = tipoMovService.buscarPorTipo(tipoMov);
		List<Bodega> listBodegas = bodegaService.findAll();
		
		try {
			
			if (maestraFormId != null && maestraFormId.length > 0) {
				for (int i = 0; i < maestraFormId.length; i++) {
					Maestra maestraItem = maestraService.buscarUno(maestraFormId[i]);

					MovInventarioDet linea = new MovInventarioDet();
					linea.setMaestra(maestraItem);
					linea.setCantidad(cantidadForm[i]);
					linea.setValorCosto(valorFormCosto[i]);

					movInventario.addItemMov(linea);
				}
			}
			
			Long conEntrada = (long) 1;
			movInventario.setNumero(consecutivoService.buscarConsecutivo(conEntrada));

			model.addAttribute("moduloMenu", moduloMenu);
			model.addAttribute("opcionMenu", opcionMenu);
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("tipoMovimiento", tipoMovimiento);
			model.addAttribute("listBodegas", listBodegas);
			
			Long estadoReg = (long) 1;
			EstadoMov estado = new EstadoMov();
			estado = estadoMovService.buscarUno(estadoReg);
			movInventario.setEstadoMov(estado);
			
			/*
			if (validacion.hasErrors()) {
				model.addAttribute("movInventario", movInventario);
				model.addAttribute("titulo", "Registrar Entredas de Inventario");
				return "mov_inventarios/entradas/entradas-form";
			}
			*/
			if (maestraFormId == null || maestraFormId.length == 0) {
				model.addAttribute("error", "No registra productos");
				return "mov_inventarios/entradas/entradas-form";
			}
			
			model.addAttribute("titulo", "Lista de Entradas de Inventario");
			movInventarioService.guardar(movInventario);
			String numEnt = movInventario.getNumero();
			status.setComplete();
			flash.addFlashAttribute("success", "Movimiento de inventario registrado con éxito. No. " + numEnt);
			return "redirect:/mov_inventarios/entradas/entradas-listar";
			
		} catch (Exception e) {
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("ordenCompra", movInventario);
			model.addAttribute("titulo", "Registrar Entradas de Inventario");
			model.addAttribute("error", "No fue posible registrar la Orden de Compra, Verifique los costos y cantidades de los productos, recuerde que no pueden ser nulos ni cero(0).");
			return "mov_inventarios/entradas/entradas-form";
		}
	}
	
	@RequestMapping(value = "/mov_inventarios/entradas/anular/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		String numEnt = "";
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		if (id > 0) {
			
			MovInventario movInventario = movInventarioService.buscarUno(id);
			Long estadoReg = (long) 5;
			EstadoMov estado = new EstadoMov();
			estado = estadoMovService.buscarUno(estadoReg);
			movInventario.setEstadoMov(estado);
			numEnt = movInventario.getNumero();
			movInventarioService.guardar(movInventario);
			
			flash.addFlashAttribute("success", "Entradas de Inventario " + numEnt + " fue Anulada con éxito");
			
		}else {
			model.addAttribute("error", "No fue posible Anular el movimiento de inventario.");
		}
		
		return "redirect:/mov_inventarios/entradas/entradas-listar";
		
	}
	
	@RequestMapping(value = "/mov_inventarios/entradas/entradas-form_id", method = RequestMethod.POST)
	public String guardarId(@Valid MovInventario movInventario, BindingResult validacion, Model model, RedirectAttributes flash,
			SessionStatus status) {
		
		List<Proveedor> proveedor = proveedorService.findAll();
		String tipoMov = "E";
		List<TipoMovimiento> tipoMovimiento = tipoMovService.buscarPorTipo(tipoMov);
		List<Bodega> listBodegas = bodegaService.findAll();
		
		try {

			model.addAttribute("moduloMenu", moduloMenu);
			model.addAttribute("opcionMenu", opcionMenu);
			model.addAttribute("movInventario", movInventario);
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("tipoMovimiento", tipoMovimiento);
			model.addAttribute("listBodegas", listBodegas);
			
			/*
			if (validacion.hasErrors()) {
				model.addAttribute("ordenCompra", ordenCompra);
				model.addAttribute("titulo", "Registrar Orden de Compra");
				return "ordencompra/ordencompra-form";
			}
			*/
			
			model.addAttribute("titulo", "Lista de Entradas de Inventario");
			movInventarioService.guardar(movInventario);
			String numEnt = movInventario.getNumero();
			
			status.setComplete();
			flash.addFlashAttribute("success", "Movimiento de inventario " + numEnt + " actualizado con éxito");
			return "redirect:/mov_inventarios/entradas/entradas-listar";
		} catch (Exception e) {
			model.addAttribute("moduloMenu", moduloMenu);
			model.addAttribute("opcionMenu", opcionMenu);
			model.addAttribute("movInventario", movInventario);
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("tipoMovimiento", tipoMovimiento);
			model.addAttribute("listBodegas", listBodegas);
			model.addAttribute("titulo", "Editar Entrada de Inventario");
			model.addAttribute("error", "No fue posible actualizar el movimiento de inventario");
			
			Long id = movInventario.getMovimientoId();
			
			return "mov_inventarios/entradas/entradas-ver/" + id;
		}
	}
	
	@RequestMapping(value = "/mov_inventarios/entradas/imprimir/{id}")
	public String imprimir(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		MovInventario movInventario = null;
		if (id > 0) {
			movInventario = movInventarioService.buscarUno(id);
		} else {
			model.addAttribute("titulo", "Editar Salida de Inventario");
			model.addAttribute("error", "No fue posible actualizar el movimiento de inventario");
			
			return "mov_inventarios/entradas/entradas-ver/" + id;
		}
		
		if(movInventario.getEstadoMov().getEstadoId() == 1) {
			if(kardexMovService.guardarKardexMov(movInventario)) {
				Long estadoReg = (long) 2;
				EstadoMov estado = estadoMovService.buscarUno(estadoReg);
				movInventario.setEstadoMov(estado);
				
				String numMov = movInventario.getNumero();
				
				movInventarioService.guardar(movInventario);
				
				model.addAttribute("titulo", "Lista de Entradas de Inventario");
				flash.addFlashAttribute("success", "Movimiento de Inventario " + numMov + " impreso con éxito.");
				
				return "redirect:/mov_inventarios/impresion/pdf/" + id + "?format=pdf";
				
			}else {
				model.addAttribute("titulo", "Editar Entrada de Inventario");
				model.addAttribute("error", "No fue posible actualizar el movimiento de inventario");
				
				return "/mov_inventarios/entradas/entradas-ver/" + id;
			}
		}else {
			return "redirect:/mov_inventarios/impresion/pdf/" + id + "?format=pdf";
		}
		
	}

}

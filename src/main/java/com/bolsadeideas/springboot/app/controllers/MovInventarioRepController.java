package com.bolsadeideas.springboot.app.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Proveedor;
import com.bolsadeideas.springboot.app.models.entity.TipoMovimiento;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioRep;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IConsecutivoService;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;
import com.bolsadeideas.springboot.app.models.service.IEstadoMovService;
import com.bolsadeideas.springboot.app.models.service.IKardexMovService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;
import com.bolsadeideas.springboot.app.models.service.ISaldoBodegaService;
import com.bolsadeideas.springboot.app.models.service.ITipoMovService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("movInventarioRep")
public class MovInventarioRepController {

	@Autowired
	IMovInventarioService movInventarioService;

	@Autowired
	IProveedorService proveedorService;

	@Autowired
	IUsuarioDao usuarioDao;
	
	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IEstadoMovService estadoMovService;
	
	@Autowired
	ITipoMovService tipoMovService;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	IConsecutivoService consecutivoService;
	
	@Autowired
	IKardexMovService kardexMovService;
	
	@Autowired
	ISaldoBodegaService saldoBodegaService;
	
	@Autowired
	private IDatosEmpresaService datosEmpresaService;
	
	String moduloMenu = "Inventarios";
	String opcionMenu = "Reportes Inventarios";

	@RequestMapping(value = "/mov_inventarios/reportes/rep-mov-inventario-list")
	public String listar(Model model, Authentication authentication) throws ParseException {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de Entradas de Inventario");
		return "/mov_inventarios/reportes/rep-mov-inventarios-list";
	}
	
	@RequestMapping(value = "/mov_inventarios/reportes/rep-mov-inventario-form")
	public String formMov(Model model, Authentication authentication) throws ParseException {
		
		MovInventarioRep movInventarioRep = new MovInventarioRep();
		
		List<Proveedor> ListProveedor = proveedorService.findAll();
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<Bodega> bodegas = new ArrayList<>();
		
		if(bodegaUsuario == null) {
			bodegas = bodegaService.findAll();
		} else {
			bodegas.add(bodegaUsuario);
			movInventarioRep.setBodega(bodegaUsuario);
		}
		
		java.util.Date fechaAct = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		String fechaActS = dateFormat.format(fechaAct);
		String sComplemento = fechaActS.substring(2);
		String fechaUno = "01" + sComplemento; 
	    Date fechaIni = new SimpleDateFormat("dd/MM/yyyy").parse(fechaUno);  
	    
	    movInventarioRep.setFechaInicial(fechaUno);
	    movInventarioRep.setFechaFinal(fechaActS);
	    
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de Entradas de Inventario");
		model.addAttribute("movInventarioRep", movInventarioRep);
		model.addAttribute("listProveedor", ListProveedor);
		model.addAttribute("listBodegas", bodegas);
		model.addAttribute("fechaInicial", fechaIni);
		model.addAttribute("fechaFinal", fechaAct);
		return "/mov_inventarios/reportes/rep-mov-inventarios";
	}

	@RequestMapping(value = "/mov_inventarios/reportes/rep-mov-inventario-ver", method = RequestMethod.POST)
	public String verMov(@Valid MovInventarioRep movInventarioRep, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) throws ParseException {
		
		Bodega bodega = movInventarioRep.getBodega();
		Proveedor proveedor = movInventarioRep.getProveedor();
		String tipoMov = movInventarioRep.getTipoMov();
		String fechaInicialS = movInventarioRep.getFechaInicial() + " 00:00:00";
		String fechaFinalS = movInventarioRep.getFechaFinal() + " 23:59:59";
		
		Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
		Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
		
		List<MovInventario> movInventarios = new ArrayList<>();
		
		if(proveedor == null && tipoMov.equals("0")) {
			movInventarios = movInventarioService.findMovBodFec(bodega, fechaInicial, fechaFinal);
		}else if(proveedor == null) {
			movInventarios = movInventarioService.findMovBodTipoFec(bodega, tipoMov, fechaInicial, fechaFinal);
		}else if(tipoMov.equals("0")) {
			movInventarios = movInventarioService.findMovBodProFec(bodega, proveedor, fechaInicial, fechaFinal);
		}else {
			movInventarios = movInventarioService.findMovBodProTipoFec(bodega, proveedor, tipoMov, fechaInicial, fechaFinal);
		}
		
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de Movimientos de Inventario");
		model.addAttribute("listMovimientos", movInventarios);
		
		return "/mov_inventarios/reportes/rep-mov-inventario-ver";
	}
	
	@RequestMapping(value = "/mov_inventarios/reportes/movimiento-ver/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		List<Proveedor> ListProveedor = proveedorService.findAll();
		List<TipoMovimiento> tipoMovimiento = tipoMovService.findAll();
		List<Bodega> listBodegas = bodegaService.findAll();
		
		MovInventario movInventario = null;
		if (id > 0) {
			movInventario = movInventarioService.buscarUno(id);
		} else {
			return "redirect:/mov_inventarios/reportes/rep-mov-inventario-ver";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("movInventario", movInventario);
		model.put("listProveedor", ListProveedor);
		model.put("tipoMovimiento", tipoMovimiento);
		model.put("listBodegas", listBodegas);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Editar Entrada de Inventario");
		
		return "/mov_inventarios/reportes/movimiento-ver";
		
	}
	
	//IMPRESION PDF MOVIMIENTOS DE INVENTARIO
	@RequestMapping(value = "/mov_inventarios/reportes/rep-mov-inventario-ver-pdf", method = RequestMethod.POST)
	public String verMovPdf(@Valid MovInventarioRep movInventarioRep, BindingResult validacion, Model model, 
			RedirectAttributes flash, SessionStatus status, 
			Authentication authentication) throws ParseException {
		
		Long idEmpresa = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		
		Bodega bodega = movInventarioRep.getBodega();
		Proveedor proveedor = movInventarioRep.getProveedor();
		String tipoMov = movInventarioRep.getTipoMov();
		String fechaInicialS = movInventarioRep.getFechaInicial() + " 00:00:00";
		String fechaFinalS = movInventarioRep.getFechaFinal() + " 23:59:59";
		
		Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
		Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
		
		List<MovInventario> movInventarios = new ArrayList<>();
		
		if(proveedor == null && tipoMov.equals("0")) {
			movInventarios = movInventarioService.findMovBodFec(bodega, fechaInicial, fechaFinal);
		}else if(proveedor == null) {
			movInventarios = movInventarioService.findMovBodTipoFec(bodega, tipoMov, fechaInicial, fechaFinal);
		}else if(tipoMov.equals("0")) {
			movInventarios = movInventarioService.findMovBodProFec(bodega, proveedor, fechaInicial, fechaFinal);
		}else {
			movInventarios = movInventarioService.findMovBodProTipoFec(bodega, proveedor, tipoMov, fechaInicial, fechaFinal);
		}
		
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de Movimientos de Inventario");
		model.addAttribute("listMovimientos", movInventarios);
		model.addAttribute("datosEmpresa", datosEmpresa);
		model.addAttribute("usuario", usuario);
		model.addAttribute("movInventarioRep", movInventarioRep);
		
		return "/mov_inventarios/reportes/rep-mov-pdf";
	}
	
	//IMPRESION PDF MOVIMIENTOS DE INVENTARIO
		@RequestMapping(value = "/mov_inventarios/reportes/rep-mov-inventario-ver-xlsx", method = RequestMethod.POST)
		public String verMovXlsx(@Valid MovInventarioRep movInventarioRep, BindingResult validacion, Model model, 
				RedirectAttributes flash, SessionStatus status, 
				Authentication authentication) throws ParseException {
			
			Long idEmpresa = (long) 1;
			DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
			
			String usuarioLog = authentication.getName();
			Usuario usuario = usuarioService.findByUsuario(usuarioLog);
			
			Bodega bodega = movInventarioRep.getBodega();
			Proveedor proveedor = movInventarioRep.getProveedor();
			String tipoMov = movInventarioRep.getTipoMov();
			String fechaInicialS = movInventarioRep.getFechaInicial() + " 00:00:00";
			String fechaFinalS = movInventarioRep.getFechaFinal() + " 23:59:59";
			
			Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
			Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
			
			List<MovInventario> movInventarios = new ArrayList<>();
			
			if(proveedor == null && tipoMov.equals("0")) {
				movInventarios = movInventarioService.findMovBodFec(bodega, fechaInicial, fechaFinal);
			}else if(proveedor == null) {
				movInventarios = movInventarioService.findMovBodTipoFec(bodega, tipoMov, fechaInicial, fechaFinal);
			}else if(tipoMov.equals("0")) {
				movInventarios = movInventarioService.findMovBodProFec(bodega, proveedor, fechaInicial, fechaFinal);
			}else {
				movInventarios = movInventarioService.findMovBodProTipoFec(bodega, proveedor, tipoMov, fechaInicial, fechaFinal);
			}
			
			
			model.addAttribute("moduloMenu", moduloMenu);
			model.addAttribute("opcionMenu", opcionMenu);
			model.addAttribute("titulo", "Reporte de Movimientos de Inventario");
			model.addAttribute("listMovimientos", movInventarios);
			model.addAttribute("datosEmpresa", datosEmpresa);
			model.addAttribute("usuario", usuario);
			model.addAttribute("movInventarioRep", movInventarioRep);
			
			return "/mov_inventarios/reportes/rep-mov.xlsx";
		}
	
}

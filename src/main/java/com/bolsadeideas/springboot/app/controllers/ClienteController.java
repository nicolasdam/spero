package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Ciudad;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.service.ICiudadService;
import com.bolsadeideas.springboot.app.models.service.IClienteService;

@Controller
@SessionAttributes("cliente")
public class ClienteController {
	
	@Autowired
	IClienteService clienteService;
	
	@Autowired
	private ICiudadService ciudadService;
	
	String moduloMenu = "Ventas";
	String opcionMenu = "Clientes";
	
	@RequestMapping(value = "/cliente/cliente-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de Clientes");
		model.addAttribute("listClientes", clienteService.findAll());
		return "cliente/cliente-listar";
	}

	@RequestMapping(value = "/cliente/cliente-form")
	public String crear(Map<String, Object> model) {

		Long est = (long) 1;
		Cliente cliente = new Cliente();
		cliente.setEstado(est);
		
		List<Ciudad> listCiudades = ciudadService.findAll();
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		
		model.put("cliente", cliente);
		model.put("listCiudades", listCiudades);
		model.put("titulo", "Registrar Cliente");

		return "cliente/cliente-form";
	}

	@RequestMapping(value = "/cliente/cliente-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		List<Ciudad> listCiudades = ciudadService.findAll();
		
		Cliente cliente = null;
		if (id > 0) {
			cliente = clienteService.buscarUno(id);
		} else {
			return "redirect:/cliente/cliente-listar";
		}
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		
		model.put("cliente", cliente);
		model.put("listCiudades", listCiudades);
		model.put("titulo", "Editar Cliente");

		return "cliente/cliente-form";
	}

	@RequestMapping(value = "/cliente/cliente-form", method = RequestMethod.POST)
	public String guardar(@Valid Cliente cliente, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		List<Ciudad> listCiudades = ciudadService.findAll();
		
		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Clientes");
			model.addAttribute("listCiudades", listCiudades);
			
			return "cliente/cliente-form";
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		clienteService.guardar(cliente);
		status.setComplete();
		flash.addFlashAttribute("success","Cliente actualizado con éxito");
		return "redirect:/cliente/cliente-listar";
	}

	@RequestMapping(value = "/cliente/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash,  Model model) {
		if (id > 0) {
			clienteService.eliminar(id);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		flash.addFlashAttribute("success","Cliente eliminado con éxito");
		return "redirect:/cliente/cliente-listar";
	}

}

package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Nivel;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;
import com.bolsadeideas.springboot.app.models.service.INivelService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("usuario")
public class UsuarioController {

	@Autowired
	private IUsuarioService usuarioService;
	
	@Autowired
	private INivelService nivelService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	IUsuarioDao usuarioDao;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	IDatosEmpresaService datosEmpresaService;
	
	String moduloMenu = "Administracion";
	String opcionMenu = "Usuarios";

	@RequestMapping(value = "/usuarios/usuarios-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de usuarios");
		model.addAttribute("usuarios", usuarioService.findAll());
		return "usuarios/usuarios-listar";
	}

	@RequestMapping(value = "/usuarios/usuarios-form")
	public String crear(Map<String, Object> model) {
		
		List<Nivel> niveles = new ArrayList<Nivel>();
		niveles = nivelService.findAll();
		
		List<Bodega> bodegas = new ArrayList<Bodega>();
		bodegas = bodegaService.findAll();
		
		Usuario usuario = new Usuario();

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("usuario", usuario);
		model.put("niv", niveles);
		model.put("bodegas", bodegas);
		model.put("titulo", "Registrar Usuario");

		return "usuarios/usuarios-form";
	}

	@RequestMapping(value = "/usuarios/usuarios-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		List<Nivel> niveles = new ArrayList<Nivel>();
		niveles = nivelService.findAll();
		List<Bodega> bodegas = new ArrayList<Bodega>();
		bodegas = bodegaService.findAll();
		
		Usuario usuario = null;
		if (id > 0) {
			usuario = usuarioService.buscarUno(id);
		} else {
			return "redirect:/usuarios/usuarios-listar";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("usuario", usuario);
		model.put("niv", niveles);
		model.put("bodegas", bodegas);
		model.put("titulo", "Editar Usuario");

		return "usuarios/usuarios-form";
	}

	@RequestMapping(value = "/usuarios/usuarios-form", method = RequestMethod.POST)
	public String guardar(@Valid Usuario usuario, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {
		
		List<Nivel> niveles = new ArrayList<Nivel>();
		niveles = nivelService.findAll();
		
		if(usuario.getUsuarioId() == null || usuario.getUsuarioId() == 0) {
			String usuarioNew = usuario.getUsuario();
			Usuario Usu = usuarioService.findByUsuario(usuarioNew);
			if(Usu != null) {
				model.addAttribute("moduloMenu", moduloMenu);
				model.addAttribute("opcionMenu", opcionMenu);
				model.addAttribute("niv", niveles);
				model.addAttribute("titulo", "Registro de usuario");
				
				model.addAttribute("error", "Usuario del sistema ya existe");
				//flash.addFlashAttribute("error", "Usuario del sistema ya existe");
				return "usuarios/usuarios-form";
			}
		}
		
		if (validacion.hasErrors()) {
			model.addAttribute("niv", niveles);
			model.addAttribute("titulo", "Registro de usuario");
			return "usuarios/usuarios-form";
		}
		
		if(usuario.getClave() == null || usuario.getClave().equals("")) {
			
			Long empId = (long) 1;
			DatosEmpresa empresa = datosEmpresaService.buscarUno(empId);
			
			String pass = empresa.getClaveDefecto();
			String bcrypPass = passwordEncoder.encode(pass);
			usuario.setClave(bcrypPass);
		}

		usuarioService.guardar(usuario);
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		status.setComplete();
		flash.addFlashAttribute("success", "Usuario actualizado con éxito");
		return "redirect:/usuarios/usuarios-listar";
	}

	@RequestMapping(value = "/usuarios/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		if (id > 0) {
			usuarioService.eliminar(id);
		}
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		flash.addFlashAttribute("success", "Usuario eliminado con éxito");
		return "redirect:/usuarios/usuarios-listar";
	}
	
	@RequestMapping(value = "/usuarios/cambioclave")
	public String cambioclave(Map<String, Object> model, Authentication authentication) {
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioDao.findByUsuario(usuarioLog);
		
		model.put("moduloMenu", "");
		model.put("opcionMenu", "");
		model.put("usuario", usuario);
		model.put("titulo", "Cambio de Clave");

		return "usuarios/cambioclave";
	}
	
	@RequestMapping(value = "/usuarios/cambioclaveG", method = RequestMethod.POST)
	public String guardarClave(@Valid Usuario usuario, BindingResult validacion, Model model, RedirectAttributes flash,
			SessionStatus status, HttpServletRequest request) {
		
		String clave1 = request.getParameter("clave1");
		String clave2 = request.getParameter("clave2");
		
		if(!clave1.equals(clave2)) {
			flash.addFlashAttribute("error", "Se digitaron dos valores diferentes");
			return "redirect:/usuarios/cambioclave";
		}
		
		String bcrypPass = passwordEncoder.encode(clave1);
		usuario.setClave(bcrypPass);
		
		usuarioService.guardar(usuario);
		
		model.addAttribute("moduloMenu", "");
		model.addAttribute("opcionMenu", "");
		status.setComplete();
		flash.addFlashAttribute("success", "Clave de usuario cambiada con exito");
		return "redirect:/index";
	}
	
	@RequestMapping(value = "/usuarios/restaurar/{id}")
	public String restaurarClave(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		Usuario usuario = null;
		
		if (id > 0) {
			usuario = usuarioService.buscarUno(id);
		}
		
		Long empId = (long) 1;
		DatosEmpresa empresa = datosEmpresaService.buscarUno(empId);
		
		String pass = empresa.getClaveDefecto();
		String bcrypPass = passwordEncoder.encode(pass);
		usuario.setClave(bcrypPass);
		
		usuarioService.guardar(usuario);
		
		model.addAttribute("moduloMenu", "");
		model.addAttribute("opcionMenu", "");
		//status.setComplete();
		flash.addFlashAttribute("success", "Clave de usuario restaurada con exito");
		return "redirect:/usuarios/usuarios-listar";
	}
}

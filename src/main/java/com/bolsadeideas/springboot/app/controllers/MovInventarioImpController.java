package com.bolsadeideas.springboot.app.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("movInventario")
public class MovInventarioImpController {

	@Autowired
	IMovInventarioService movInventarioService;
	
	@Autowired
	private IDatosEmpresaService datosEmpresaService;
	
	@Autowired
	IUsuarioService usuarioService;
	
	String moduloMenu = "Inventarios";
	String opcionMenu = "Inventarios Entradas";
	
	String redirect = null;
	String entrada = "E";
	String salida = "S";
	
	@RequestMapping(value = "/mov_inventarios/impresion/pdf/{id}")
	public String pdf(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model, Authentication authentication) {
		
		Long idEmpresa = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		
		MovInventario movInventario = null;
		if (id > 0) {
			movInventario = movInventarioService.buscarUno(id);
		} else {
			model.addAttribute("titulo", "Editar Salida de Inventario");
			model.addAttribute("error", "No fue posible actualizar el movimiento de inventario");
			
			return "mov_inventarios/entradas/entradas-ver/" + id;
		}
		
		model.addAttribute("movInventario", movInventario);
		model.addAttribute("datosEmpresa", datosEmpresa);
		model.addAttribute("usuario", usuario);
		
		return "mov_inventarios/imp-pdf";
	}

}

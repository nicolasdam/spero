package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Presentacion;
import com.bolsadeideas.springboot.app.models.service.IPresentacionService;

@Controller
@SessionAttributes("presentacion")
public class PresentacionController {
	
	@Autowired
	private IPresentacionService presentacionService;
	
	@RequestMapping(value = "/presentacion/presentacion-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Presentaciones");
		model.addAttribute("titulo", "Lista de presentaciones");
		model.addAttribute("presentaciones", presentacionService.findAll());
		
		return "presentacion/presentacion-listar";
	}

	//////////////////////////////
	@RequestMapping(value = "/presentacion/presentacion-form")
	public String crear(Map<String, Object> model) {

		Presentacion presentacion = new Presentacion();

		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Presentaciones");
		model.put("presentacion", presentacion);
		model.put("titulo", "Registrar Presentación");

		return "presentacion/presentacion-form";
	}

	@RequestMapping(value = "/presentacion/presentacion-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Presentacion presentacion = null;
		if (id > 0) {
			presentacion = presentacionService.buscarUno(id);
		} else {
			return "redirect:/presentacion/presentacion-listar";
		}
		
		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Presentaciones");
		model.put("presentacion", presentacion);
		model.put("titulo", "Editar presentación");

		return "presentacion/presentacion-form";
	}

	@RequestMapping(value = "/presentacion/presentacion-form", method = RequestMethod.POST)
	public String guardar(@Valid Presentacion presentacion, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Presentaciones");
			return "presentacion/presentacion-form";
		}
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Presentaciones");
		presentacionService.guardar(presentacion);
		status.setComplete();
		flash.addFlashAttribute("success","Presentación actualizado con éxito");
		return "redirect:/presentacion/presentacion-listar";
	}

	@RequestMapping(value = "/presentacion/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash,  Model model) {
		if (id > 0) {
			presentacionService.eliminar(id);
		}
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Presentaciones");
		flash.addFlashAttribute("success","Presentación eliminado con éxito");
		return "redirect:/presentacion/presentacion-listar";
	}

}

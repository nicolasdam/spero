package com.bolsadeideas.springboot.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;
import com.bolsadeideas.springboot.app.models.service.IFacturaService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("factura")
public class FacturaImpController {
	
	@Autowired
	IFacturaService facturaService;
	
	@Autowired
	private IDatosEmpresaService datosEmpresaService;
	
	@Autowired
	IUsuarioService usuarioService;
	
	String moduloMenu = "Ventas";
	String opcionMenu = "Ventas";
	
	@RequestMapping(value = "/ventas/impresion/pdf/{id}")
	public String pdf(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model, Authentication authentication) {
		
		Long idEmpresa = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		
		Factura factura = null;
		if (id > 0) {
			factura = facturaService.buscarUno(id);
		} else {
			model.addAttribute("titulo", "Editar Salida de Inventario");
			model.addAttribute("error", "No fue posible actualizar el movimiento de inventario");
			
			return "ventas/ventas-imp/" + id;
		}
		
		model.addAttribute("factura", factura);
		model.addAttribute("datosEmpresa", datosEmpresa);
		model.addAttribute("usuario", usuario);
		
		return "ventas/imp-pdf";
	}

}

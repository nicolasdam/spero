package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Proveedor;
import com.bolsadeideas.springboot.app.models.entity.TipoMovimiento;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.EstadoMov;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioDet;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioDetNew;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IConsecutivoService;
import com.bolsadeideas.springboot.app.models.service.IEstadoMovService;
import com.bolsadeideas.springboot.app.models.service.IKardexMovService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioDetService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;
import com.bolsadeideas.springboot.app.models.service.ITipoMovService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("movInventario")
public class MovInventarioRecController {

	@Autowired
	IMovInventarioService movInventarioService;

	@Autowired
	IProveedorService proveedorService;

	@Autowired
	IUsuarioDao usuarioDao;
	
	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IEstadoMovService estadoMovService;
	
	@Autowired
	ITipoMovService tipoMovService;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	IConsecutivoService consecutivoService;
	
	@Autowired
	IKardexMovService kardexMovService;
	
	@Autowired
	IMovInventarioDetService movInventarioDetService;
	
	String moduloMenu = "Inventarios";
	String opcionMenu = "Inventarios Recibir";
	
	@RequestMapping(value = "/mov_inventarios/traslados-recibo/recibir-listar", method = RequestMethod.GET)
	public String listar(Model model, Authentication authentication) {
		
		Long estadoEnt = (long) 3;
		EstadoMov estado = estadoMovService.buscarUno(estadoEnt);
		String tipoMov = "T";
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<MovInventario> movInventarios = new ArrayList<>();
		if(bodegaUsuario == null) {
			movInventarios = movInventarioService.findEstado(estado, tipoMov);
		} else {
			//movInventarios = movInventarioService.findMovEstBod(estado, bodegaUsuario, tipoMov);
			movInventarios = movInventarioService.findMovEstBodRec(estado, bodegaUsuario, tipoMov);
		}

		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de Traslados de Inventario");
		model.addAttribute("listMovimientos", movInventarios);
		return "mov_inventarios/traslados-recibo/recibir-listar";
	}
	
	@RequestMapping(value = "/mov_inventarios/traslados-recibo/recibir-ver/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		List<Proveedor> ListProveedor = proveedorService.findAll();
		String tipoMov = "T";
		List<TipoMovimiento> tipoMovimiento = tipoMovService.buscarPorTipo(tipoMov);
		List<Bodega> listBodegas = bodegaService.findAll();

		MovInventario movInventario = null;
		if (id > 0) {
			movInventario = movInventarioService.buscarUno(id);
			for (MovInventarioDet movDet : movInventario.getItems()) {
				if(movDet.getCantidadRec() == null) {
					movDet.setCantidadRec(movDet.getCantidad());
				}
			}
		} else {
			return "redirect:/mov_inventarios/traslados/traslados-listar";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("movInventario", movInventario);
		model.put("listProveedor", ListProveedor);
		model.put("tipoMovimiento", tipoMovimiento);
		model.put("listBodegas", listBodegas);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Ver Traslado de Inventario");
		
		return "mov_inventarios/traslados-recibo/recibir-ver";
	}
	
	@RequestMapping(value = "/mov_inventarios/traslados-recibo/recibir-form", method = RequestMethod.POST)
	public String guardar(@Valid MovInventario movInventario, BindingResult validacion, Model model,
			@RequestParam(name = "maestraID[]", required = false) Long[] maestraFormId,
			@RequestParam(name = "valorCosto[]", required = false) Double[] valorFormCosto,
			@RequestParam(name = "cantidad[]", required = false) Double[] cantidadForm, RedirectAttributes flash,
			SessionStatus status) {
		
		List<Proveedor> proveedor = proveedorService.findAll();
		String tipoMov = "T";
		List<TipoMovimiento> tipoMovimiento = tipoMovService.buscarPorTipo(tipoMov);
		List<Bodega> listBodegas = bodegaService.findAll();
		
		try {
			
			if (maestraFormId != null && maestraFormId.length > 0) {
				for (int i = 0; i < maestraFormId.length; i++) {
					Maestra maestraItem = maestraService.buscarUno(maestraFormId[i]);

					MovInventarioDet linea = new MovInventarioDet();
					linea.setMaestra(maestraItem);
					linea.setCantidad(cantidadForm[i]);
					linea.setValorCosto(valorFormCosto[i]);

					movInventario.addItemMov(linea);
				}
			}
			
			Long conSalida = (long) 3;
			movInventario.setNumero(consecutivoService.buscarConsecutivo(conSalida));

			model.addAttribute("moduloMenu", moduloMenu);
			model.addAttribute("opcionMenu", opcionMenu);
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("tipoMovimiento", tipoMovimiento);
			model.addAttribute("listBodegas", listBodegas);
			
			Long estadoReg = (long) 1;
			EstadoMov estado = new EstadoMov();
			estado = estadoMovService.buscarUno(estadoReg);
			movInventario.setEstadoMov(estado);
			
			/*
			if (validacion.hasErrors()) {
				model.addAttribute("movInventario", movInventario);
				model.addAttribute("titulo", "Registrar Entredas de Inventario");
				return "mov_inventarios/entradas/entradas-form";
			}
			*/
			if (maestraFormId == null || maestraFormId.length == 0) {
				model.addAttribute("error", "No registra productos");
				return "mov_inventarios/traslados/traslados-form";
			}
			
			model.addAttribute("titulo", "Lista de Traslados de Inventario");
			movInventarioService.guardar(movInventario);
			String numEnt = movInventario.getNumero();
			status.setComplete();
			flash.addFlashAttribute("success", "Movimiento de Inventario registrada con éxito. No. " + numEnt);
			return "redirect:/mov_inventarios/traslados/traslados-listar";
			
		} catch (Exception e) {
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("ordenCompra", movInventario);
			model.addAttribute("titulo", "Registrar Traslado de Inventario");
			model.addAttribute("error", "No fue posible registrar el movimiento de inventario, Verifique los costos y cantidades de los productos, recuerde que no pueden ser nulos ni cero(0).");
			return "mov_inventarios/traslados/traslados-form";
		}
	}
	
	@RequestMapping(value = "/mov_inventarios/traslados-recibo/imprimir", method = RequestMethod.POST)
	public String imprimir(@Valid MovInventario movInventario, RedirectAttributes flash, Model model, Authentication authentication) {
		
		if(kardexMovService.guardarKardexMovtraslados(movInventario)) {
			Long estadoReg = (long) 4;
			EstadoMov estado = estadoMovService.buscarUno(estadoReg);
			
			String usuarioLog = authentication.getName();
			Usuario usuarioRec = usuarioDao.findByUsuario(usuarioLog);
			java.util.Date fechaAct = new Date();
			
			movInventario.setEstadoMov(estado);
			movInventario.setUsuarioRecibe(usuarioRec);
			movInventario.setFechaRecibe(fechaAct);
			
			String numMov = movInventario.getNumero();
			
			movInventarioService.guardar(movInventario);
			
			model.addAttribute("titulo", "Lista de Entradas de Inventario");
			flash.addFlashAttribute("success", "Movimiento de Inventario " + numMov + " impreso con éxito.");
			
			return "redirect:/mov_inventarios/traslados-recibo/recibir-listar";
		}else {
			model.addAttribute("titulo", "Editar Entrada de Inventario");
			model.addAttribute("error", "No fue posible actualizar el movimiento de inventario");
			
			Long id = movInventario.getMovimientoId();
			
			return "mov_inventarios/entradas/entradas-ver/" + id;
		}
	}
	
	@RequestMapping(value = "/mov_inventarios/traslados-recibo/detalle-ver/{id}")
	public String editarDet(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		MovInventarioDetNew movInventarioDet = null;
		
		if (id > 0) {
			movInventarioDet = movInventarioDetService.buscarUno(id);
		} else {
			return "redirect:/ordencompra/ordencompra-ver/{id}";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("movInventarioDet", movInventarioDet);
		model.put("titulo", "Editar Detalle Movimiento de inventario");

		return "mov_inventarios/traslados-recibo/det-mov-form";
		
	}
	
	@RequestMapping(value = "/mov_inventarios/traslados-recibo/det-mov-form", method = RequestMethod.POST)
	public String guardarDet(@Valid MovInventarioDetNew movInventarioDet, BindingResult validacion, Model model, 
			RedirectAttributes flash, SessionStatus status) {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		Long maId = movInventarioDet.getDetalleId();
		
		if (validacion.hasErrors()) {
			model.addAttribute("movInventarioDet", movInventarioDet);
			model.addAttribute("titulo", "Editar Detalle Movimiento");
			return "mov_inventarios/traslados-recibo/det-mov-form/" + maId;
		}
		
		Long id = movInventarioDet.getMovInventario().getMovimientoId();

		model.addAttribute("titulo", "Ver Traslado de Inventario");
		movInventarioDetService.guardar(movInventarioDet);
		status.setComplete();
		flash.addFlashAttribute("success", "Detalle de Movimiento de Inventario actualizado con éxito");
		
		return "redirect:/mov_inventarios/traslados-recibo/recibir-ver/" + id;
	}
	
	
}

package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MovInventario;
import com.bolsadeideas.springboot.app.models.entity.MovInventarioDetNew;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioDetService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;

@Controller
@SessionAttributes("movInventarioDet")
public class MovInventarioDetController {
	
	@Autowired
	IMovInventarioDetService movInventarioDetService;
	
	@Autowired
	IMovInventarioService movInventarioService;
	
	@Autowired
	IMaestraService maestraService;
	
	String moduloMenu = "Inventarios";
	String opcionMenu = "Inventarios Entradas";
	
	String redirect = null;
	String entrada = "E";
	String salida = "S";
	String traslado = "T";
	
	@RequestMapping(value = "/mov_inventarios/det-movimiento-new/{id}")
	public String crear(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		MovInventarioDetNew movInventarioDet = new MovInventarioDetNew();
		MovInventario movInventario = movInventarioService.buscarUno(id);
		movInventarioDet.setMovInventario(movInventario);

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("movInventarioDet", movInventarioDet);
		model.put("titulo", "Registrar Detalle Movimiento de inventario");

		return "mov_inventarios/det-movimiento-new";
	}
	
	@RequestMapping(value = "/mov_inventarios/det-movimiento-form/{id}")
	public String editarDet(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		MovInventarioDetNew movInventarioDet = null;
		
		if (id > 0) {
			movInventarioDet = movInventarioDetService.buscarUno(id);
		} else {
			return "redirect:/ordencompra/ordencompra-ver/{id}";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("movInventarioDet", movInventarioDet);
		model.put("titulo", "Editar Detalle Movimiento de inventario");

		return "mov_inventarios/det-movimiento-form";
		
	}
	
	@RequestMapping(value = "/mov_inventarios/det-movimiento-form", method = RequestMethod.POST)
	public String guardarDet(@Valid MovInventarioDetNew movInventarioDet, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name = "maestra", required = false) Long maestraFormId) {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		Maestra maestra = new Maestra();
		Long IdReg = movInventarioDet.getDetalleId();
		maestra = movInventarioDet.getMaestra();
		Long maId = (long) 1;
		
		if(maestra == null) {
			maestra = maestraService.buscarUno(maId);
			movInventarioDet.setMaestra(maestra);
		}
		
		if (validacion.hasErrors()) {
			model.addAttribute("movInventarioDet", movInventarioDet);
			model.addAttribute("titulo", "Editar Detalle Orden de Compra");
			return "redirect:/mov_inventarios/det-movimiento-form/" + IdReg;
		}
		
		Long id = movInventarioDet.getMovInventario().getMovimientoId();
		MovInventario movInventario = movInventarioService.buscarUno(id);
		String tipoMov = movInventario.getTipoMovimiento().getTipoMov();

		model.addAttribute("titulo", "Lista de Ordenes de Compra");
		movInventarioDetService.guardar(movInventarioDet);
		status.setComplete();
		flash.addFlashAttribute("success", "Detalle de Movimiento de Inventario actualizado con éxito");
		
		if (tipoMov.equals(entrada)) {
			redirect = "redirect:/mov_inventarios/entradas/entradas-ver/";
		}else if (tipoMov.equals(salida)) {
			redirect = "redirect:/mov_inventarios/salidas/salidas-ver/";
		}else if (tipoMov.equals(traslado)) {
			redirect = "redirect:/mov_inventarios/traslados/traslados-ver/";
		}
		return redirect + id;
	}
	
	@RequestMapping(value = "/mov_inventarios/det-movimiento-new", method = RequestMethod.POST)
	public String guardarNew(@Valid MovInventarioDetNew movInventarioDet, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name = "maestraId") String maestraFormId) {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		Maestra maestraNew = new Maestra();
		maestraNew = movInventarioDet.getMaestra();
		Long mFrom = null;
		
		if(maestraNew == null) {
			if(!maestraFormId.equals("") || maestraFormId != null) {
				//String cortado = maestraFormId.substring(1);
				mFrom = Long.parseLong(maestraFormId);
				//System.out.println("la maestra selecionada es: " + cortado);
			}
			maestraNew = maestraService.buscarUno(mFrom);
			movInventarioDet.setMaestra(maestraNew);
		}
		/*
		if (validacion.hasErrors()) {
			model.addAttribute("listMaestras", maestraService.findAll());
			model.addAttribute("ordenCompraDet", ordenCompraDet);
			model.addAttribute("titulo", "Editar Detalle Orden de Compra");
			return "ordencompra/det-ordencompra-new";
		}
		*/
		Long id = movInventarioDet.getMovInventario().getMovimientoId();
		MovInventario movInventario = movInventarioService.buscarUno(id);
		String tipoMov = movInventario.getTipoMovimiento().getTipoMov();

		model.addAttribute("titulo", "Consultar Movimiento de Inventario");
		movInventarioDetService.guardar(movInventarioDet);
		status.setComplete();
		flash.addFlashAttribute("success", "Detalle de Movimiento de Inventario actualizado con éxito");
		
		if (tipoMov.equals(entrada)) {
			redirect = "redirect:/mov_inventarios/entradas/entradas-ver/";
		}else if (tipoMov.equals(salida)) {
			redirect = "redirect:/mov_inventarios/salidas/salidas-ver/";
		}else if (tipoMov.equals(traslado)) {
			redirect = "redirect:/mov_inventarios/traslados/traslados-ver/";
		}
		
		return redirect + id;
	}
	
	@RequestMapping(value = "/mov_inventarios/det-movimiento-eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		Long movId = null;
		String tipoMov = null;
		
		if (id > 0) {
			MovInventarioDetNew movInventarioDet = movInventarioDetService.buscarUno(id);
			movId = movInventarioDet.getMovInventario().getMovimientoId();
			tipoMov = movInventarioDet.getMovInventario().getTipoMovimiento().getTipoMov();
			movInventarioDetService.eliminar(id);
		}

		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		flash.addFlashAttribute("success", "Detalle de Movimiento de Inventario eliminado con éxito");
		
		if (tipoMov.equals(entrada)) {
			redirect = "redirect:/mov_inventarios/entradas/entradas-ver/";
		}else if (tipoMov.equals(salida)) {
			redirect = "redirect:/mov_inventarios/salidas/salidas-ver/";
		}else if (tipoMov.equals(traslado)) {
			redirect = "redirect:/mov_inventarios/traslados/traslados-ver/";
		}
		return redirect + movId;
	}
	
	@RequestMapping(value = "/mov_inventarios/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		MovInventario movInventario = movInventarioService.buscarUno(id);
		String tipoMov = movInventario.getTipoMovimiento().getTipoMov();
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		//flash.addFlashAttribute("success", "Detalle de Movimiento de Inventario eliminado con éxito");
		
		if (tipoMov.equals(entrada)) {
			redirect = "redirect:/mov_inventarios/entradas/entradas-ver/";
		}else if (tipoMov.equals(salida)) {
			redirect = "redirect:/mov_inventarios/salidas/salidas-ver/";
		}else if (tipoMov.equals(traslado)) {
			redirect = "redirect:/mov_inventarios/traslados/traslados-ver/";
		}
		
		return redirect + id;
	}

}

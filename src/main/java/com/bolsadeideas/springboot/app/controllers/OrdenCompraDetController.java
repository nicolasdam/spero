package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.OrdenCompra;
import com.bolsadeideas.springboot.app.models.entity.OrdenCompraDetNew;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IOrdenCompraDetService;
import com.bolsadeideas.springboot.app.models.service.IOrdenCompraService;

@Controller
@SessionAttributes("ordenCompraDet")
public class OrdenCompraDetController {
	
	@Autowired
	IOrdenCompraDetService ordenCompraDetService;
	
	@Autowired
	IOrdenCompraService ordenCompraService;
	
	@Autowired
	IMaestraService maestraService;
	
	@RequestMapping(value = "/ordencompra/det-ordencompra-new/{id}")
	public String crear(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		OrdenCompraDetNew ordenCompraDet = new OrdenCompraDetNew();
		OrdenCompra ordenCompra = ordenCompraService.buscarUno(id);
		ordenCompraDet.setOrdenCompra(ordenCompra);

		model.put("moduloMenu", "Compras");
		model.put("opcionMenu", "Orden de Compra");
		model.put("ordenCompraDet", ordenCompraDet);
		model.put("titulo", "Registrar Orden de Compra");

		return "ordencompra/det-ordencompra-new";
	}
	
	@RequestMapping(value = "/ordencompra/det-ordencompra-form/{id}")
	public String editarDet(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		OrdenCompraDetNew ordenCompraDet = null;
		
		if (id > 0) {
			ordenCompraDet = ordenCompraDetService.buscarUno(id);
		} else {
			return "redirect:/ordencompra/ordencompra-ver/{id}";
		}

		model.put("moduloMenu", "Compras");
		model.put("opcionMenu", "Orden de Compra");
		model.put("ordenCompraDet", ordenCompraDet);
		model.put("titulo", "Editar Detalle Orden de Compra");

		return "ordencompra/det-ordencompra-form";
		
	}
	
	@RequestMapping(value = "/ordencompra/det-ordencompra-form", method = RequestMethod.POST)
	public String guardarDet(@Valid OrdenCompraDetNew ordenCompraDet, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name = "maestra", required = false) Long maestraFormId) {
		
		model.addAttribute("moduloMenu", "Compras");
		model.addAttribute("opcionMenu", "Orden de Compra");
		
		Maestra maestra = new Maestra();
		maestra = ordenCompraDet.getMaestra();
		Long maId = (long) 1;
		
		if(maestra == null) {
			maestra = maestraService.buscarUno(maId);
			ordenCompraDet.setMaestra(maestra);
		}
		
		if (validacion.hasErrors()) {

			model.addAttribute("ordenCompraDet", ordenCompraDet);
			model.addAttribute("titulo", "Editar Detalle Orden de Compra");
			return "ordencompra/det-ordencompra-form";
		}
		
		Long id = null;
		id = ordenCompraDet.getOrdenCompra().getOrdenId();

		model.addAttribute("titulo", "Lista de Ordenes de Compra");
		ordenCompraDetService.guardar(ordenCompraDet);
		status.setComplete();
		flash.addFlashAttribute("success", "Detalle Orden de Compra actualizado con éxito");
		return "redirect:/ordencompra/ordencompra-ver/" + id;
	}
	
	@RequestMapping(value = "/ordencompra/det-ordencompra-new", method = RequestMethod.POST)
	public String guardarNew(@Valid OrdenCompraDetNew ordenCompraDet, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name = "maestraId") String maestraFormId) {
		
		model.addAttribute("moduloMenu", "Compras");
		model.addAttribute("opcionMenu", "Orden de Compra");
		
		Maestra maestraNew = new Maestra();
		maestraNew = ordenCompraDet.getMaestra();
		Long mFrom = null;
		
		if(maestraNew == null) {
			if(!maestraFormId.equals("") || maestraFormId != null) {
				//String cortado = maestraFormId.substring(1);
				mFrom = Long.parseLong(maestraFormId);
				//System.out.println("la maestra selecionada es: " + cortado);
			}
			maestraNew = maestraService.buscarUno(mFrom);
			ordenCompraDet.setMaestra(maestraNew);
		}
		/*
		if (validacion.hasErrors()) {
			model.addAttribute("listMaestras", maestraService.findAll());
			model.addAttribute("ordenCompraDet", ordenCompraDet);
			model.addAttribute("titulo", "Editar Detalle Orden de Compra");
			return "ordencompra/det-ordencompra-new";
		}
		*/
		Long id = ordenCompraDet.getOrdenCompra().getOrdenId();

		model.addAttribute("titulo", "Lista de Ordenes de Compra");
		ordenCompraDetService.guardar(ordenCompraDet);
		status.setComplete();
		flash.addFlashAttribute("success", "Detalle Orden de Compra actualizado con éxito");
		return "redirect:/ordencompra/ordencompra-ver/" + id;
	}
	
	@RequestMapping(value = "/ordencompra/det-eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		Long ordenId = null;
		
		if (id > 0) {
			OrdenCompraDetNew ordenCompraDet = null;
			ordenCompraDet = ordenCompraDetService.buscarUno(id);
			ordenId = ordenCompraDet.getOrdenCompra().getOrdenId();
			ordenCompraDetService.eliminar(id);
		}

		model.addAttribute("moduloMenu", "Compras");
		model.addAttribute("opcionMenu", "Orden de Compra");
		flash.addFlashAttribute("success", "Orden de Compra eliminada con éxito");
		return "redirect:/ordencompra/ordencompra-ver/" + ordenId;
	}

}

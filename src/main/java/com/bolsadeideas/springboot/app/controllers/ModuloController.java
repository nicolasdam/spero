package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Modulo;
import com.bolsadeideas.springboot.app.models.service.IModuloService;

@Controller
@SessionAttributes("modulo")
public class ModuloController {

	@Autowired
	private IModuloService moduloService;
	
	String moduloMenu = "Administracion";
	String opcionMenu = "Modulos";
	
	@RequestMapping(value = "/modulo/modulo-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de modulos");
		model.addAttribute("modulos", moduloService.findAll());
		
		return "modulo/modulo-listar";
	}

	@RequestMapping(value = "/modulo/modulo-form")
	public String crear(Map<String, Object> model) {

		Modulo modulo = new Modulo();

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("modulo", modulo);
		model.put("titulo", "Registrar Modulo");

		return "modulo/modulo-form";
	}

	@RequestMapping(value = "/modulo/modulo-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Modulo modulo = null;
		if (id > 0) {
			modulo = moduloService.buscarUno(id);
		} else {
			return "redirect:/modulo/modulo-listar";
		}
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("modulo", modulo);
		model.put("titulo", "Editar Modulo");

		return "modulo/modulo-form";
	}

	@RequestMapping(value = "/modulo/modulo-form", method = RequestMethod.POST)
	public String guardar(@Valid Modulo modulo, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Modulos");
			return "modulo/modulo-form";
		}
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		moduloService.guardar(modulo);
		status.setComplete();
		flash.addFlashAttribute("success","Modulo actualizado con éxito");
		return "redirect:/modulo/modulo-listar";
	}

	@RequestMapping(value = "/modulo/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash,  Model model) {
		if (id > 0) {
			moduloService.eliminar(id);
		}
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		flash.addFlashAttribute("success","Modulo eliminado con éxito");
		return "redirect:/modulo/modulo-listar";
	}
}

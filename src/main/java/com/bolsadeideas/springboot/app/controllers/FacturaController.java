package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.EstadoFac;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.FacturaDet;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IClienteService;
import com.bolsadeideas.springboot.app.models.service.IConsecutivoService;
import com.bolsadeideas.springboot.app.models.service.IEstadoFacService;
import com.bolsadeideas.springboot.app.models.service.IFacturaService;
import com.bolsadeideas.springboot.app.models.service.IKardexMovService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("factura")
public class FacturaController {

	@Autowired
	IFacturaService facturaService;

	@Autowired
	IClienteService clienteService;

	@Autowired
	IUsuarioDao usuarioDao;
	
	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IEstadoFacService estadoFacService;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	IConsecutivoService consecutivoService;
	
	@Autowired
	IKardexMovService kardexMovService;
	
	String moduloMenu = "Ventas";
	String opcionMenu = "Ventas";
	
	String redirect = null;
	String salida = "S";

	@RequestMapping(value = "/ventas/ventas-listar", method = RequestMethod.GET)
	public String listar(Model model, Authentication authentication) {
		
		Long lEstadoFac = (long) 1;
		EstadoFac estado = estadoFacService.buscarUno(lEstadoFac);
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<Factura> facturas = new ArrayList<>();
		if(bodegaUsuario == null) {
			facturas = facturaService.findEstado(estado);
		} else {
			facturas = facturaService.findEstadoBod(estado, bodegaUsuario);
		}

		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de ventas");
		model.addAttribute("listfacturas", facturas);
		return "ventas/ventas-listar";
	}
	
	@RequestMapping(value = "/ventas/ventas-form")
	public String crear(Map<String, Object> model, Authentication authentication) {

		Factura factura = new Factura();
		
		List<Cliente> ListClientes = clienteService.findAll();
		Long cliDef = (long) 1;
		Cliente clienteDef = clienteService.buscarUno(cliDef);
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		List<Bodega> listBodegas = new ArrayList<>();
		if(bodegaUsuario == null) {
			listBodegas = bodegaService.findAll();
		} else {
			listBodegas.add(bodegaUsuario);
			factura.setBodega(bodegaUsuario);
		}

		factura.setUsuario(usuario);
		factura.setCliente(clienteDef);

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("factura", factura);
		model.put("ListClientes", ListClientes);
		model.put("listBodegas", listBodegas);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Registrar venta");

		return "ventas/ventas-form";
	}
	
	@RequestMapping(value = "/ventas/ventas-ver/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, Authentication authentication) {
		
		List<Cliente> ListClientes = clienteService.findAll();
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<Bodega> listBodegas = new ArrayList<>();
		if(bodegaUsuario == null) {
			listBodegas = bodegaService.findAll();
		} else {
			listBodegas.add(bodegaUsuario);
		}

		Factura factura = null;
		if (id > 0) {
			factura = facturaService.buscarUno(id);
		} else {
			return "redirect:/ventas/ventas-listar";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("factura", factura);
		model.put("ListClientes", ListClientes);
		model.put("listBodegas", listBodegas);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Editar venta");
		
		return "ventas/ventas-ver";

	}
	
	@RequestMapping(value = "/ventas/ventas-imp/{id}")
	public String mostrar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		List<Cliente> ListClientes = clienteService.findAll();
		List<Bodega> listBodegas = bodegaService.findAll();

		Factura factura = null;
		if (id > 0) {
			factura = facturaService.buscarUno(id);
		} else {
			return "redirect:/ventas/ventas-listar";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("factura", factura);
		model.put("ListClientes", ListClientes);
		model.put("listBodegas", listBodegas);
		model.put("listMaestras", maestraService.findAll());
		model.put("titulo", "Editar venta");
		
		return "ventas/ventas-imp";
		
	}
	
	@RequestMapping(value = "/ventas/ventas-form", method = RequestMethod.POST)
	public String guardar(@Valid Factura factura, BindingResult validacion, Model model,
			@RequestParam(name = "maestraID[]", required = false) Long[] maestraFormId,
			@RequestParam(name = "precioVenta[]", required = false) Double[] valorFormPrecio,
			@RequestParam(name = "cantidad[]", required = false) Double[] cantidadForm, RedirectAttributes flash,
			SessionStatus status) {
		
		List<Cliente> ListClientes = clienteService.findAll();
		List<Bodega> listBodegas = bodegaService.findAll();
		
		try {
			
			if (maestraFormId != null && maestraFormId.length > 0) {
				for (int i = 0; i < maestraFormId.length; i++) {
					Maestra maestraItem = maestraService.buscarUno(maestraFormId[i]);

					FacturaDet linea = new FacturaDet();
					linea.setMaestra(maestraItem);
					linea.setCantidad(cantidadForm[i]);
					linea.setPrecio(valorFormPrecio[i]);

					factura.addItemFac(linea);
				}
			}
			
			Long conFactura = (long) 4;
			factura.setNumero(consecutivoService.buscarConsecutivo(conFactura));

			model.addAttribute("moduloMenu", moduloMenu);
			model.addAttribute("opcionMenu", opcionMenu);
			model.addAttribute("ListClientes", ListClientes);
			model.addAttribute("listBodegas", listBodegas);
			
			Long lEstadoFac = (long) 1;
			EstadoFac estadoFac = estadoFacService.buscarUno(lEstadoFac);
			
			factura.setEstadoFac(estadoFac);
			
			/*
			if (validacion.hasErrors()) {
				model.addAttribute("movInventario", movInventario);
				model.addAttribute("titulo", "Registrar Entredas de Inventario");
				return "mov_inventarios/entradas/entradas-form";
			}
			*/
			if (maestraFormId == null || maestraFormId.length == 0) {
				model.addAttribute("error", "No registra productos");
				return "ventas/ventas-form";
			}
			
			model.addAttribute("titulo", "Lista de ventas");
			facturaService.guardar(factura);
			String numFac = factura.getNumero();
			status.setComplete();
			flash.addFlashAttribute("success", "Venta registrada con éxito. No. " + numFac);
			return "redirect:/ventas/ventas-listar";
			
		} catch (Exception e) {
			model.addAttribute("ListClientes", ListClientes);
			model.addAttribute("factura", factura);
			model.addAttribute("titulo", "Registrar venta");
			model.addAttribute("error", "No fue posible registrar la venta, Verifique los costos y cantidades de los productos, recuerde que no pueden ser nulos ni cero(0).");
			return "ventas/ventas-form";
		}
	}
	
	@RequestMapping(value = "/ventas/anular/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		String numFac = "";
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		if (id > 0) {
			
			Factura factura = facturaService.buscarUno(id);
			Long lEstadoFac = (long) 3;
			EstadoFac estadoFac = estadoFacService.buscarUno(lEstadoFac);
			factura.setEstadoFac(estadoFac);
			numFac = factura.getNumero();
			facturaService.guardar(factura);
			
			flash.addFlashAttribute("success", "Venta " + numFac + " fue Anulada con éxito");
			
		}else {
			model.addAttribute("error", "No fue posible Anular la venta.");
		}
		
		return "redirect:/ventas/ventas-listar";
		
	}
	
	@RequestMapping(value = "/ventas/ventas-form_id", method = RequestMethod.POST)
	public String guardarId(@Valid Factura factura, BindingResult validacion, Model model, RedirectAttributes flash,
			SessionStatus status) {
		
		List<Cliente> ListClientes = clienteService.findAll();
		List<Bodega> listBodegas = bodegaService.findAll();
		
		try {

			model.addAttribute("moduloMenu", moduloMenu);
			model.addAttribute("opcionMenu", opcionMenu);
			model.addAttribute("factura", factura);
			model.addAttribute("ListClientes", ListClientes);
			model.addAttribute("listBodegas", listBodegas);
			
			/*
			if (validacion.hasErrors()) {
				model.addAttribute("ordenCompra", ordenCompra);
				model.addAttribute("titulo", "Registrar Orden de Compra");
				return "ordencompra/ordencompra-form";
			}
			*/
			
			model.addAttribute("titulo", "Lista de Entradas de Inventario");
			facturaService.guardar(factura);
			String numFac = factura.getNumero();
			
			status.setComplete();
			flash.addFlashAttribute("success", "Venta " + numFac + " actualizado con éxito");
			return "redirect:/ventas/ventas-listar";
		} catch (Exception e) {
			model.addAttribute("moduloMenu", moduloMenu);
			model.addAttribute("opcionMenu", opcionMenu);
			model.addAttribute("factura", factura);
			model.addAttribute("ListClientes", ListClientes);
			model.addAttribute("listBodegas", listBodegas);
			model.addAttribute("titulo", "Editar venta");
			model.addAttribute("error", "No fue posible actualizar la venta");
			
			Long id = factura.getFacturaId();
			
			return "ventas/ventas-ver/" + id;
		}
	}
	
	@RequestMapping(value = "/ventas/imprimir/{id}")
	public String imprimir(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		Factura factura = null;
		if (id > 0) {
			factura = facturaService.buscarUno(id);
		} else {
			model.addAttribute("titulo", "Editar venta");
			model.addAttribute("error", "No fue posible actualizar la venta");
			
			return "ventas/ventas-ver/" + id;
		}
		
		if(factura.getEstadoFac().getEstadoId() == 1) {
			if(kardexMovService.guardarKardexFac(factura)) {
				Long lEstadoFac = (long) 2;
				EstadoFac estadoFac = estadoFacService.buscarUno(lEstadoFac);
				factura.setEstadoFac(estadoFac);
				
				String numMov = factura.getNumero();
				
				facturaService.guardar(factura);
				
				model.addAttribute("titulo", "Lista de ventas");
				flash.addFlashAttribute("success", "Venta " + numMov + " impresa con éxito.");
				
				return "redirect:/ventas/impresion/pdf/" + id + "?format=pdf";
				
			}else {
				model.addAttribute("titulo", "Editar venta");
				model.addAttribute("error", "No fue posible actualizar la venta");
				
				return "/ventas/ventas-ver/" + id;
			}
		}else {
			return "redirect:/ventas/impresion/pdf/" + id + "?format=pdf";
		}
		
	}

}

package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.FacturaDetNew;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.service.IFacturaDetService;
import com.bolsadeideas.springboot.app.models.service.IFacturaService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;

@Controller
@SessionAttributes("facturaDet")
public class FacturaDetController {
	
	@Autowired
	IFacturaDetService facturaDetService;
	
	@Autowired
	IFacturaService facturaService;
	
	@Autowired
	IMaestraService maestraService;
	
	String moduloMenu = "Ventas";
	String opcionMenu = "Ventas";
	
	String redirect = null;
	
	@RequestMapping(value = "/ventas/det-ventas-new/{id}")
	public String crear(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		FacturaDetNew facturaDet = new FacturaDetNew();
		Factura factura = facturaService.buscarUno(id);
		facturaDet.setFacturaId(factura);

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("facturaDet", facturaDet);
		model.put("titulo", "Registrar detalle de venta");

		return "ventas/det-ventas-new";
	}
	
	@RequestMapping(value = "/ventas/det-ventas-form/{id}")
	public String editarDet(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		FacturaDetNew facturaDet = null;
		
		if (id > 0) {
			facturaDet = facturaDetService.buscarUno(id);
		} else {
			return "redirect:/ventas/ventas-ver/{id}";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("facturaDet", facturaDet);
		model.put("titulo", "Editar Detalle Movimiento de inventario");

		return "ventas/det-ventas-form";
		
	}
	
	@RequestMapping(value = "/ventas/det-ventas-form", method = RequestMethod.POST)
	public String guardarDet(@Valid FacturaDetNew facturaDet, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name = "maestra", required = false) Long maestraFormId) {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		Maestra maestra = new Maestra();
		maestra = facturaDet.getMaestra();
		Long maId = (long) 1;
		
		if(maestra == null) {
			maestra = maestraService.buscarUno(maId);
			facturaDet.setMaestra(maestra);
		}
		
		if (validacion.hasErrors()) {
			model.addAttribute("facturaDet", facturaDet);
			model.addAttribute("titulo", "Editar detalle de venta");
			return "ventas/det-ventas-form";
		}
		
		Long id = facturaDet.getFacturaId().getFacturaId();
		model.addAttribute("titulo", "Edicion ventas");
		facturaDetService.guardar(facturaDet);
		status.setComplete();
		flash.addFlashAttribute("success", "Detalle de la venta actualizado con éxito");
		
		return "redirect:/ventas/ventas-ver/" + id;
	}
	
	@RequestMapping(value = "/ventas/det-ventas-new", method = RequestMethod.POST)
	public String guardarNew(@Valid FacturaDetNew facturaDet, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status,
			@RequestParam(name = "maestraId") String maestraFormId) {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		Maestra maestraNew = new Maestra();
		maestraNew = facturaDet.getMaestra();
		Long mFrom = null;
		
		if(maestraNew == null) {
			if(!maestraFormId.equals("") || maestraFormId != null) {
				//String cortado = maestraFormId.substring(1);
				mFrom = Long.parseLong(maestraFormId);
				//System.out.println("la maestra selecionada es: " + cortado);
			}
			maestraNew = maestraService.buscarUno(mFrom);
			facturaDet.setMaestra(maestraNew);
		}
		/*
		if (validacion.hasErrors()) {
			model.addAttribute("listMaestras", maestraService.findAll());
			model.addAttribute("ordenCompraDet", ordenCompraDet);
			model.addAttribute("titulo", "Editar Detalle Orden de Compra");
			return "ordencompra/det-ordencompra-new";
		}
		*/
		
		Long id = facturaDet.getFacturaId().getFacturaId();
		model.addAttribute("titulo", "Consultar venta");
		facturaDetService.guardar(facturaDet);
		status.setComplete();
		flash.addFlashAttribute("success", "Detalle de la venta actualizado con éxito");
		
		return "redirect:/ventas/ventas-ver/" + id;
	}
	
	@RequestMapping(value = "/ventas/det-ventas-eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		Long facId = null;
		
		if (id > 0) {
			FacturaDetNew facturaDet = facturaDetService.buscarUno(id);
			facId = facturaDet.getFacturaId().getFacturaId();
			facturaDetService.eliminar(id);
		}

		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		flash.addFlashAttribute("success", "Detalle de venta eliminado con éxito");
		
		return "redirect:/ventas/ventas-ver/" + facId;
	}
	
	@RequestMapping(value = "/ventas/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		return "redirect:/ventas/ventas-ver/" + id;
	}

}

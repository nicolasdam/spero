package com.bolsadeideas.springboot.app.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.BodegaFac;
import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.EstadoFac;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.FacturaRep;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IClienteService;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;
import com.bolsadeideas.springboot.app.models.service.IEstadoFacService;
import com.bolsadeideas.springboot.app.models.service.IFacturaService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("facturaRep")
public class FacturaRepController {
	
	@Autowired
	IFacturaService facturaService;
	
	@Autowired
	IClienteService clienteService;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	IUsuarioService usuarioService;
	
	@Autowired
	IEstadoFacService estadoFacService;
	
	@Autowired
	private IDatosEmpresaService datosEmpresaService;
	
	String moduloMenu = "Ventas";
	String opcionMenu = "Reportes Ventas";
	
	@RequestMapping(value = "/ventas/reportes/rep-ventas-list")
	public String listar(Model model, Authentication authentication) throws ParseException {
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reportes ventas");
		return "/ventas/reportes/rep-ventas-list";
	}
	
	@RequestMapping(value = "/ventas/reportes/rep-ventas-rango-form")
	public String formRango(Model model, Authentication authentication) throws ParseException {
		
		FacturaRep facturaRep = new FacturaRep();
		
		List<Cliente> ListCliente = clienteService.findAll();
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<Bodega> bodegas = new ArrayList<>();
		List<Usuario> usuarios = new ArrayList<>();
		
		if(bodegaUsuario == null) {
			bodegas = bodegaService.findAll();
			usuarios = usuarioService.findAll();
			facturaRep.setTodos(true);
		} else {
			Usuario usu = usuarioService.findByUsuario(usuarioLog);
			bodegas.add(bodegaUsuario);
			usuarios.add(usu);
			facturaRep.setBodega(bodegaUsuario);
			facturaRep.setUsuario(usu);
			facturaRep.setTodos(false);
		}
		
		java.util.Date fechaAct = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		String fechaActS = dateFormat.format(fechaAct);
		String sComplemento = fechaActS.substring(2);
		String fechaUno = "01" + sComplemento; 
	    Date fechaIni = new SimpleDateFormat("dd/MM/yyyy").parse(fechaUno);  
	    
	    facturaRep.setFechaInicial(fechaUno);
	    facturaRep.setFechaFinal(fechaActS);
	    
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de ventas por rango");
		model.addAttribute("facturaRep", facturaRep);
		model.addAttribute("ListCliente", ListCliente);
		model.addAttribute("listUsuarios", usuarios);
		model.addAttribute("listBodegas", bodegas);
		model.addAttribute("fechaInicial", fechaIni);
		model.addAttribute("fechaFinal", fechaAct);
		return "/ventas/reportes/rep-ventas-rango-form";
	}
	
	@RequestMapping(value = "/ventas/reportes/rep-ventas-rango-ver", method = RequestMethod.POST)
	public String verRango(@Valid FacturaRep facturaRep, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) throws ParseException {
		
		Long estFac = (long) 2;
		EstadoFac estadoFac = estadoFacService.buscarUno(estFac);
		Bodega bodega = facturaRep.getBodega();
		Cliente cliente = facturaRep.getCliente();
		Usuario usuario = facturaRep.getUsuario();
		String fechaInicialS = facturaRep.getFechaInicial() + " 00:00:00";
		String fechaFinalS = facturaRep.getFechaFinal() + " 23:59:59";
		
		Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
		Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
		
		List<Factura> listFacturas = new ArrayList<>();
		List<BodegaFac> listBodegaFac = new ArrayList<>();
		List<Bodega> listBodegas = new ArrayList<>();
		
		if(bodega == null && cliente == null && usuario == null) {
			listFacturas = facturaService.findFacFec(fechaInicial, fechaFinal, estadoFac);
		}else if(bodega != null && cliente == null && usuario == null) {
			listFacturas = facturaService.findFacBodFec(bodega, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega == null && cliente != null && usuario == null) {
			listFacturas = facturaService.findFacCliFec(cliente, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega == null && cliente == null && usuario != null) {
			listFacturas = facturaService.findFacUsuFec(usuario, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega != null && cliente != null && usuario == null) {
			listFacturas = facturaService.findFacBodCliFec(bodega, cliente, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega != null && cliente == null && usuario != null) {
			listFacturas = facturaService.findFacBodUsuFec(bodega, usuario, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega == null && cliente != null && usuario != null) {
			listFacturas = facturaService.findFacCliUsuFec(cliente, usuario, fechaInicial, fechaFinal, estadoFac);
		}
		
		Bodega bodAnt = null;
		
		for (Factura fac : listFacturas) {
			Bodega bodNueva = fac.getBodega();
			if(bodAnt != bodNueva) {
				listBodegas.add(bodNueva);
				bodAnt = bodNueva;
			}
		}
		
		for (Bodega bod : listBodegas) {
			BodegaFac bodegafac = new BodegaFac();
			bodegafac.setBodega(bod);
			for (Factura fac : listFacturas) {
				Bodega bodFac = fac.getBodega();
				if(bod == bodFac) {
					bodegafac.addItemMov(fac);
				}
			}
			listBodegaFac.add(bodegafac);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de ventas");
		model.addAttribute("listBodegaFac", listBodegaFac);
		return "/ventas/reportes/rep-ventas-rango-ver";
	}
	
	@RequestMapping(value = "/ventas/reportes/rep-ventas-imp/{id}")
	public String mostrarRango(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Factura factura = null;
		if (id > 0) {
			factura = facturaService.buscarUno(id);
		} else {
			return "redirect:/ventas/ventas-listar";
		}

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("factura", factura);
		model.put("titulo", "Mostrar venta");
		
		return "ventas/reportes/rep-ventas-imp-rango";
		
	}
	
	@RequestMapping(value = "/ventas/reportes/rep-ventas-dia-form")
	public String formDia(Model model, Authentication authentication) throws ParseException {
		
		FacturaRep facturaRep = new FacturaRep();
		
		List<Cliente> ListCliente = clienteService.findAll();
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<Bodega> bodegas = new ArrayList<>();
		List<Usuario> usuarios = new ArrayList<>();
		
		if(bodegaUsuario == null) {
			bodegas = bodegaService.findAll();
			usuarios = usuarioService.findAll();
			facturaRep.setTodos(true);
		} else {
			Usuario usu = usuarioService.findByUsuario(usuarioLog);
			bodegas.add(bodegaUsuario);
			facturaRep.setBodega(bodegaUsuario);
			usuarios.add(usu);
			facturaRep.setBodega(bodegaUsuario);
			facturaRep.setUsuario(usu);
			facturaRep.setTodos(false);
		}
		
		java.util.Date fechaAct = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		String fechaActS = dateFormat.format(fechaAct);
	    
	    facturaRep.setFechaInicial(fechaActS);
	    facturaRep.setFechaFinal(fechaActS);
	    
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de ventas por rango");
		model.addAttribute("facturaRep", facturaRep);
		model.addAttribute("ListCliente", ListCliente);
		model.addAttribute("listUsuarios", usuarios);
		model.addAttribute("listBodegas", bodegas);
		model.addAttribute("fechaInicial", fechaAct);
		return "/ventas/reportes/rep-ventas-dia-form";
	}

	@RequestMapping(value = "/ventas/reportes/rep-ventas-dia-ver", method = RequestMethod.POST)
	public String verDia(@Valid FacturaRep facturaRep, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) throws ParseException {
		
		Long estFac = (long) 2;
		EstadoFac estadoFac = estadoFacService.buscarUno(estFac);
		Bodega bodega = facturaRep.getBodega();
		Cliente cliente = facturaRep.getCliente();
		Usuario usuario = facturaRep.getUsuario();
		String fechaInicialS = facturaRep.getFechaInicial() + " 00:00:00";
		String fechaFinalS = facturaRep.getFechaInicial() + " 23:59:59";
		
		Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
		Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
		
		List<Factura> listFacturas = new ArrayList<>();
		List<BodegaFac> listBodegaFac = new ArrayList<>();
		List<Bodega> listBodegas = new ArrayList<>();
		
		if(bodega == null && cliente == null && usuario == null) {
			listFacturas = facturaService.findFacFec(fechaInicial, fechaFinal, estadoFac);
		}else if(bodega != null && cliente == null && usuario == null) {
			listFacturas = facturaService.findFacBodFec(bodega, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega == null && cliente != null && usuario == null) {
			listFacturas = facturaService.findFacCliFec(cliente, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega == null && cliente == null && usuario != null) {
			listFacturas = facturaService.findFacUsuFec(usuario, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega != null && cliente != null && usuario == null) {
			listFacturas = facturaService.findFacBodCliFec(bodega, cliente, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega != null && cliente == null && usuario != null) {
			listFacturas = facturaService.findFacBodUsuFec(bodega, usuario, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega == null && cliente != null && usuario != null) {
			listFacturas = facturaService.findFacCliUsuFec(cliente, usuario, fechaInicial, fechaFinal, estadoFac);
		}
		
		Bodega bodAnt = null;
		
		for (Factura fac : listFacturas) {
			Bodega bodNueva = fac.getBodega();
			if(bodAnt != bodNueva) {
				listBodegas.add(bodNueva);
				bodAnt = bodNueva;
			}
		}
		
		for (Bodega bod : listBodegas) {
			BodegaFac bodegafac = new BodegaFac();
			bodegafac.setBodega(bod);
			for (Factura fac : listFacturas) {
				Bodega bodFac = fac.getBodega();
				if(bod == bodFac) {
					bodegafac.addItemMov(fac);
				}
			}
			listBodegaFac.add(bodegafac);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de ventas");
		model.addAttribute("listBodegaFac", listBodegaFac);
		return "/ventas/reportes/rep-ventas-dia-ver";
	}
	
	//IMPRIMIR PDF VENTAS POR RANGO
	@RequestMapping(value = "/ventas/reportes/rep-ventas-rango-ver-pdf", method = RequestMethod.POST)
	public String verRangopDF(@Valid FacturaRep facturaRep, BindingResult validacion, Model model, 
			RedirectAttributes flash, SessionStatus status,
			Authentication authentication) throws ParseException {
		
		Long idEmpresa = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		
		Long estFac = (long) 2;
		EstadoFac estadoFac = estadoFacService.buscarUno(estFac);
		Bodega bodega = facturaRep.getBodega();
		Cliente cliente = facturaRep.getCliente();
		String fechaInicialS = facturaRep.getFechaInicial() + " 00:00:00";
		String fechaFinalS = facturaRep.getFechaFinal() + " 23:59:59";
		
		Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
		Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
		
		List<Factura> listFacturas = new ArrayList<>();
		List<BodegaFac> listBodegaFac = new ArrayList<>();
		List<Bodega> listBodegas = new ArrayList<>();
		
		if(bodega == null && cliente == null) {
			listFacturas = facturaService.findFacFec(fechaInicial, fechaFinal, estadoFac);
		}else if(cliente == null) {
			listFacturas = facturaService.findFacBodFec(bodega, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega == null) {
			listFacturas = facturaService.findFacCliFec(cliente, fechaInicial, fechaFinal, estadoFac);
		}else {
			listFacturas = facturaService.findFacBodCliFec(bodega, cliente, fechaInicial, fechaFinal, estadoFac);
		}
		
		Bodega bodAnt = null;
		
		for (Factura fac : listFacturas) {
			Bodega bodNueva = fac.getBodega();
			if(bodAnt != bodNueva) {
				listBodegas.add(bodNueva);
				bodAnt = bodNueva;
			}
		}
		
		for (Bodega bod : listBodegas) {
			BodegaFac bodegafac = new BodegaFac();
			bodegafac.setBodega(bod);
			for (Factura fac : listFacturas) {
				Bodega bodFac = fac.getBodega();
				if(bod == bodFac) {
					bodegafac.addItemMov(fac);
				}
			}
			listBodegaFac.add(bodegafac);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de ventas");
		model.addAttribute("listBodegaFac", listBodegaFac);
		model.addAttribute("datosEmpresa", datosEmpresa);
		model.addAttribute("facturaRep", facturaRep);
		model.addAttribute("usuario", usuario);
		
		return "/ventas/reportes/rep-ventas-rango-pdf";
	}
	
	@RequestMapping(value = "/ventas/reportes/rep-ventas-dia-ver-pdf", method = RequestMethod.POST)
	public String verDiaPdf(@Valid FacturaRep facturaRep, BindingResult validacion, Model model, 
			RedirectAttributes flash, SessionStatus status,
			Authentication authentication) throws ParseException {
		
		Long idEmpresa = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		
		Long estFac = (long) 2;
		EstadoFac estadoFac = estadoFacService.buscarUno(estFac);
		Bodega bodega = facturaRep.getBodega();
		Cliente cliente = facturaRep.getCliente();
		String fechaInicialS = facturaRep.getFechaInicial() + " 00:00:00";
		String fechaFinalS = facturaRep.getFechaInicial() + " 23:59:59";
		
		Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
		Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
		
		List<Factura> listFacturas = new ArrayList<>();
		List<BodegaFac> listBodegaFac = new ArrayList<>();
		List<Bodega> listBodegas = new ArrayList<>();
		
		if(bodega == null && cliente == null) {
			listFacturas = facturaService.findFacFec(fechaInicial, fechaFinal, estadoFac);
		}else if(cliente == null) {
			listFacturas = facturaService.findFacBodFec(bodega, fechaInicial, fechaFinal, estadoFac);
		}else if(bodega == null) {
			listFacturas = facturaService.findFacCliFec(cliente, fechaInicial, fechaFinal, estadoFac);
		}else {
			listFacturas = facturaService.findFacBodCliFec(bodega, cliente, fechaInicial, fechaFinal, estadoFac);
		}
		
		Bodega bodAnt = null;
		
		for (Factura fac : listFacturas) {
			Bodega bodNueva = fac.getBodega();
			if(bodAnt != bodNueva) {
				listBodegas.add(bodNueva);
				bodAnt = bodNueva;
			}
		}
		
		for (Bodega bod : listBodegas) {
			BodegaFac bodegafac = new BodegaFac();
			bodegafac.setBodega(bod);
			for (Factura fac : listFacturas) {
				Bodega bodFac = fac.getBodega();
				if(bod == bodFac) {
					bodegafac.addItemMov(fac);
				}
			}
			listBodegaFac.add(bodegafac);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de ventas");
		model.addAttribute("listBodegaFac", listBodegaFac);
		model.addAttribute("datosEmpresa", datosEmpresa);
		model.addAttribute("facturaRep", facturaRep);
		model.addAttribute("usuario", usuario);
		
		return "/ventas/reportes/rep-ventas-dia-pdf";
	}
	
}

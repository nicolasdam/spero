package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bolsadeideas.springboot.app.models.entity.Nivel;
import com.bolsadeideas.springboot.app.models.service.INivelService;
import com.bolsadeideas.springboot.app.models.service.IOpcionNivelService;

@Controller
public class OpcionNivelController {
	
	@Autowired
	private INivelService nivelService;
	
	@Autowired
	private IOpcionNivelService opcionNivelService;
	
	@RequestMapping(value = "/niveles/opciones-nivel-listar/{id}")
	public String listar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		Nivel nivel = null;
		if (id > 0) {
			nivel = nivelService.buscarUno(id);
		} else {
			return "redirect:/niveles/niveles-listar";
		}
		
		model.put("moduloMenu", "Administracion");
		model.put("opcionMenu", "Niveles");
		model.put("titulo", "Opciones por Nivel");
		model.put("opNivel", opcionNivelService.findByNivel(nivel));
		return "redirect:/niveles/opciones-nivel-listar";
		
	}

}

package com.bolsadeideas.springboot.app.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.KardexInventarioRep;
import com.bolsadeideas.springboot.app.models.entity.KardexMov;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IConsecutivoService;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;
import com.bolsadeideas.springboot.app.models.service.IEstadoMovService;
import com.bolsadeideas.springboot.app.models.service.IKardexMovService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;
import com.bolsadeideas.springboot.app.models.service.ISaldoBodegaService;
import com.bolsadeideas.springboot.app.models.service.ITipoMovService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("kardexInventarioRep")
public class KardexRepController {

	@Autowired
	IMovInventarioService movInventarioService;

	@Autowired
	IProveedorService proveedorService;

	@Autowired
	IUsuarioDao usuarioDao;
	
	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IEstadoMovService estadoMovService;
	
	@Autowired
	ITipoMovService tipoMovService;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	IConsecutivoService consecutivoService;
	
	@Autowired
	IKardexMovService kardexMovService;
	
	@Autowired
	ISaldoBodegaService saldoBodegaService;
	
	@Autowired
	private IDatosEmpresaService datosEmpresaService;
	
	String moduloMenu = "Inventarios";
	String opcionMenu = "Reportes Inventarios";
	
	@RequestMapping(value = "/mov_inventarios/reportes/rep-kardex-inventario-form")
	public String formKardex(Model model, Authentication authentication) throws ParseException {
		
		KardexInventarioRep kardexInventarioRep = new KardexInventarioRep();
		
		List<Maestra> ListMaestra = maestraService.sortMaestraNombre();
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<Bodega> bodegas = new ArrayList<>();
		
		if(bodegaUsuario == null) {
			bodegas = bodegaService.findAll();
		} else {
			bodegas.add(bodegaUsuario);
			kardexInventarioRep.setBodega(bodegaUsuario);
		}
		
		java.util.Date fechaAct = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		String fechaActS = dateFormat.format(fechaAct);
		String sComplemento = fechaActS.substring(2);
		String fechaUno = "01" + sComplemento; 
	    Date fechaIni = new SimpleDateFormat("dd/MM/yyyy").parse(fechaUno);  
	    
	    kardexInventarioRep.setFechaInicial(fechaUno);
	    kardexInventarioRep.setFechaFinal(fechaActS);
	    
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de Entradas de Inventario");
		model.addAttribute("kardexInventarioRep", kardexInventarioRep);
		model.addAttribute("listMaestras", ListMaestra);
		model.addAttribute("listBodegas", bodegas);
		model.addAttribute("fechaInicial", fechaIni);
		model.addAttribute("fechaFinal", fechaAct);
		return "/mov_inventarios/reportes/rep-kardex-inventarios";
	}
	
	
	@RequestMapping(value = "/mov_inventarios/reportes/rep-kardex-inventario-ver", method = RequestMethod.POST)
	public String verKarex(@Valid KardexInventarioRep kardexInventarioRep, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) throws ParseException {
		
		Bodega bodega = kardexInventarioRep.getBodega();
		Maestra maestra = kardexInventarioRep.getMaestra();
		String fechaInicialS = kardexInventarioRep.getFechaInicial() + " 00:00:00";
		String fechaFinalS = kardexInventarioRep.getFechaFinal() + " 23:59:59";
		
		Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
		Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
		
		List<KardexMov> listKardexMov = kardexMovService.findKardexByMaestra(bodega, maestra, fechaInicial, fechaFinal);
		
		KardexMov kardexFinal = null;
		
		for (KardexMov kardex : listKardexMov) {
			kardexFinal = kardex;
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de Kardex de Inventario");
		model.addAttribute("listKardexMov", listKardexMov);
		model.addAttribute("bodega", bodega);
		model.addAttribute("maestra", maestra);
		model.addAttribute("kardexFinal", kardexFinal);
		model.addAttribute("kardexInventarioRep", kardexInventarioRep);
		
		return "/mov_inventarios/reportes/rep-kardex-inventario-ver";
	}
	
	//IMPRESION PDF KARDEX DE INVENTARIO
	@RequestMapping(value = "/mov_inventarios/reportes/rep-kardex-inventario-ver-pdf", method = RequestMethod.POST)
	public String verKarexPdf(@Valid KardexInventarioRep kardexInventarioRep, BindingResult validacion, Model model, 
			RedirectAttributes flash, SessionStatus status,
			Authentication authentication) throws ParseException {
		
		Long idEmpresa = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		
		Bodega bodega = kardexInventarioRep.getBodega();
		Maestra maestra = kardexInventarioRep.getMaestra();
		String fechaInicialS = kardexInventarioRep.getFechaInicial() + " 00:00:00";
		String fechaFinalS = kardexInventarioRep.getFechaFinal() + " 23:59:59";
		
		Date fechaInicial = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaInicialS);
		Date fechaFinal = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(fechaFinalS);
		
		List<KardexMov> listKardexMov = kardexMovService.findKardexByMaestra(bodega, maestra, fechaInicial, fechaFinal);
		
		KardexMov kardexFinal = null;
		
		for (KardexMov kardex : listKardexMov) {
			kardexFinal = kardex;
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de Kardex de Inventario");
		model.addAttribute("listKardexMov", listKardexMov);
		model.addAttribute("bodega", bodega);
		model.addAttribute("maestra", maestra);
		model.addAttribute("kardexFinal", kardexFinal);
		model.addAttribute("datosEmpresa", datosEmpresa);
		model.addAttribute("usuario", usuario);
		
		return "/mov_inventarios/reportes/rep-kardex-pdf";
	}
	
}

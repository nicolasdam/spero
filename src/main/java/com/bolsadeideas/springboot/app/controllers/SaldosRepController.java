package com.bolsadeideas.springboot.app.controllers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.SaldoBodega;
import com.bolsadeideas.springboot.app.models.entity.SaldosInventarioRep;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.IConsecutivoService;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;
import com.bolsadeideas.springboot.app.models.service.IEstadoMovService;
import com.bolsadeideas.springboot.app.models.service.IKardexMovService;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IMovInventarioService;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;
import com.bolsadeideas.springboot.app.models.service.ISaldoBodegaService;
import com.bolsadeideas.springboot.app.models.service.ITipoMovService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("saldosInventarioRep")
public class SaldosRepController {

	@Autowired
	IMovInventarioService movInventarioService;

	@Autowired
	IProveedorService proveedorService;

	@Autowired
	IUsuarioDao usuarioDao;
	
	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IEstadoMovService estadoMovService;
	
	@Autowired
	ITipoMovService tipoMovService;
	
	@Autowired
	IBodegaService bodegaService;
	
	@Autowired
	IConsecutivoService consecutivoService;
	
	@Autowired
	IKardexMovService kardexMovService;
	
	@Autowired
	ISaldoBodegaService saldoBodegaService;
	
	@Autowired
	private IDatosEmpresaService datosEmpresaService;
	
	String moduloMenu = "Inventarios";
	String opcionMenu = "Reportes Inventarios";
	
	@RequestMapping(value = "/mov_inventarios/reportes/rep-saldos-inventario-form")
	public String formSaldos(Model model, Authentication authentication) throws ParseException {
		
		SaldosInventarioRep saldosInventarioRep = new SaldosInventarioRep();
		
		List<Maestra> ListMaestra = maestraService.sortMaestraNombre();
		
		String usuarioLog = authentication.getName();
		Bodega bodegaUsuario = usuarioService.findByUsuarioBod(usuarioLog);
		List<Bodega> bodegas = new ArrayList<>();
		
		if(bodegaUsuario == null) {
			bodegas = bodegaService.findAll();
			saldosInventarioRep.setTodos(true);
		} else {
			bodegas.add(bodegaUsuario);
			saldosInventarioRep.setBodega(bodegaUsuario);
			saldosInventarioRep.setTodos(false);
		}
	    
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de Entradas de Inventario");
		model.addAttribute("saldosInventarioRep", saldosInventarioRep);
		model.addAttribute("listMaestras", ListMaestra);
		model.addAttribute("listBodegas", bodegas);
		return "/mov_inventarios/reportes/rep-saldos-inventarios";
	}
	
	@RequestMapping(value = "/mov_inventarios/reportes/rep-saldos-inventario-ver", method = RequestMethod.POST)
	public String verSaldos(@Valid SaldosInventarioRep saldosInventarioRep, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) throws ParseException {
		
		Bodega bodega = saldosInventarioRep.getBodega();
		Maestra maestra = saldosInventarioRep.getMaestra();
		
		List<Bodega> listBodega = new ArrayList<>();
		
		if(bodega == null && maestra == null) {
			List<Bodega> bodegas = bodegaService.findAll();
			for (Bodega bod : bodegas) {
				List<SaldoBodega> saldoBodega = saldoBodegaService.saldoBodega(bod);
				if(saldoBodega != null && saldoBodega.size() > 0) {
					bod.setSaldoBodega(saldoBodega);
					listBodega.add(bod);
				}
			}
		} else if(bodega == null) {
			List<Bodega> bodegas = bodegaService.findAll();
			for (Bodega bod : bodegas) {
				SaldoBodega saldoBodegaMaestra = saldoBodegaService.saldoBodegaMaestra(bod, maestra);
				if(saldoBodegaMaestra != null) {
					List<SaldoBodega> saldoBodega = new ArrayList<>();
					saldoBodega.add(saldoBodegaMaestra);
					bod.setSaldoBodega(saldoBodega);
					listBodega.add(bod);
				}
			}
		} else if(maestra == null) {
			bodega.setSaldoBodega(saldoBodegaService.saldoBodega(bodega));
			listBodega.add(bodega);
			
		} else {
			SaldoBodega saldo = saldoBodegaService.saldoBodegaMaestra(bodega, maestra);
			List<SaldoBodega> saldoBodega = new ArrayList<>();
			saldoBodega.add(saldo);
			bodega.setSaldoBodega(saldoBodega);
			listBodega.add(bodega);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de Saldos de Inventario");
		model.addAttribute("listBodega", listBodega);
		
		return "/mov_inventarios/reportes/rep-saldos-inventario-ver";
	}
	
	//IMPRESION PDF MOVIMIENTOS DE INVENTARIO
	@RequestMapping(value = "/mov_inventarios/reportes/rep-saldos-inventario-ver-pdf", method = RequestMethod.POST)
	public String verSaldosPdf(@Valid SaldosInventarioRep saldosInventarioRep, BindingResult validacion, Model model, 
			RedirectAttributes flash, SessionStatus status, 
			Authentication authentication) throws ParseException {
		
		Long idEmpresa = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioService.findByUsuario(usuarioLog);
		
		Bodega bodega = saldosInventarioRep.getBodega();
		Maestra maestra = saldosInventarioRep.getMaestra();
		
		List<Bodega> listBodega = new ArrayList<>();
		
		if(bodega == null && maestra == null) {
			List<Bodega> bodegas = bodegaService.findAll();
			for (Bodega bod : bodegas) {
				List<SaldoBodega> saldoBodega = saldoBodegaService.saldoBodega(bod);
				if(saldoBodega != null) {
					bod.setSaldoBodega(saldoBodega);
					listBodega.add(bod);
				}
			}
		} else if(bodega == null) {
			List<Bodega> bodegas = bodegaService.findAll();
			for (Bodega bod : bodegas) {
				SaldoBodega saldoBodegaMaestra = saldoBodegaService.saldoBodegaMaestra(bod, maestra);
				if(saldoBodegaMaestra != null) {
					List<SaldoBodega> saldoBodega = new ArrayList<>();
					saldoBodega.add(saldoBodegaMaestra);
					bod.setSaldoBodega(saldoBodega);
					listBodega.add(bod);
				}
			}
		} else if(maestra == null) {
			bodega.setSaldoBodega(saldoBodegaService.saldoBodega(bodega));
			listBodega.add(bodega);
			
		} else {
			SaldoBodega saldo = saldoBodegaService.saldoBodegaMaestra(bodega, maestra);
			List<SaldoBodega> saldoBodega = new ArrayList<>();
			saldoBodega.add(saldo);
			bodega.setSaldoBodega(saldoBodega);
			listBodega.add(bodega);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Reporte de Saldos de Inventario");
		model.addAttribute("listBodega", listBodega);
		model.addAttribute("datosEmpresa", datosEmpresa);
		model.addAttribute("usuario", usuario);
		model.addAttribute("saldosInventarioRep", saldosInventarioRep);
		
		return "/mov_inventarios/reportes/rep-saldo-pdf";
	}
	
}

package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bolsadeideas.springboot.app.models.dao.IOpcionNivelDao;
import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.entity.Modulo;
import com.bolsadeideas.springboot.app.models.entity.ModuloMenu;
import com.bolsadeideas.springboot.app.models.entity.Nivel;
import com.bolsadeideas.springboot.app.models.entity.Opcion;
import com.bolsadeideas.springboot.app.models.entity.OpcionNivel;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;

@Controller
public class index {
	
	@Autowired
	IUsuarioDao usuarioDao;

	@Autowired
	IOpcionNivelDao opcionNivelDao;
	
	@Autowired
	private IDatosEmpresaService datosEmpresaService;
	
	@RequestMapping(value= {"/index","/"})
	public String listar(Model model, HttpSession session, Authentication authentication) {
		
		Long idEmpresa = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(idEmpresa);
		
		List<Modulo> modulosOP = new ArrayList<>();
		List<ModuloMenu> modulosMenu = new ArrayList<>();
		
		String usuarioLog = authentication.getName();
		Usuario usuarioSis = usuarioDao.findByUsuario(usuarioLog);
		
		Nivel nivelLog = usuarioSis.getNivel();
		
		List<OpcionNivel> opcionNivel = opcionNivelDao.findByNivel(nivelLog);
		
		//List<Opcion> opcionNivelUsuario = opcionService.findAll();
		Modulo moduloAnt = new Modulo();
		
		for (OpcionNivel opNivel : opcionNivel) {
			Modulo moduloAct = opNivel.getOpcion().getModulo();
			if(moduloAct != moduloAnt) {
				moduloAnt = moduloAct;
				modulosOP.add(moduloAct);
			}
		}
		
		for (Modulo modOp : modulosOP) {
			ModuloMenu moduloMenu = new ModuloMenu();
			moduloMenu.setModulo(modOp);

			for (OpcionNivel opNivel : opcionNivel) {
				Modulo modAct = opNivel.getOpcion().getModulo();
				if(modAct == modOp) {
					Opcion opAct = opNivel.getOpcion();
					moduloMenu.addItemMov(opAct);
				}
			}

			modulosMenu.add(moduloMenu);
		}
		
		session.setAttribute("modulosOpMenu", modulosMenu);
		session.setAttribute("datosEmpresa", datosEmpresa);
		session.setAttribute("usuarioSis", usuarioSis);
		model.addAttribute("titulo", datosEmpresa.getProyecto());
		
		return "index";
		
	}

}

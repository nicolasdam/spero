package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Fabricante;
import com.bolsadeideas.springboot.app.models.service.IFabricanteService;

@Controller
@SessionAttributes("fabricante")
public class FabricanteController {
	
	@Autowired
	private IFabricanteService fabricanteService;
	
	@RequestMapping(value = "/fabricante/fabricante-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Fabricantes");
		model.addAttribute("titulo", "Lista de fabricantes");
		model.addAttribute("fabricantes", fabricanteService.findAll());
		return "fabricante/fabricante-listar";
	}

	@RequestMapping(value = "/fabricante/fabricante-form")
	public String crear(Map<String, Object> model) {

		Fabricante fabricante = new Fabricante();

		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Fabricantes");
		model.put("fabricante", fabricante);
		model.put("titulo", "Registrar Fabricante");

		return "fabricante/fabricante-form";
	}

	@RequestMapping(value = "/fabricante/fabricante-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Fabricante fabricante = null;
		if (id > 0) {
			fabricante = fabricanteService.buscarUno(id);
		} else {
			return "redirect:/fabricante/fabricante-listar";
		}
		
		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Fabricantes");
		model.put("fabricante", fabricante);
		model.put("titulo", "Editar Bodega");

		return "fabricante/fabricante-form";
	}

	@RequestMapping(value = "/fabricante/fabricante-form", method = RequestMethod.POST)
	public String guardar(@Valid Fabricante fabricante, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Bodega");
			return "fabricante/fabricante-form";
		}
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Fabricantes");
		fabricanteService.guardar(fabricante);
		status.setComplete();
		flash.addFlashAttribute("success","Bodega actualizada con éxito");
		return "redirect:/fabricante/fabricante-listar";
	}

	@RequestMapping(value = "/fabricante/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash,  Model model) {
		if (id > 0) {
			fabricanteService.eliminar(id);
		}
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Fabricantes");
		flash.addFlashAttribute("success","Modulo eliminado con éxito");
		return "redirect:/fabricante/fabricante-listar";
	}

}

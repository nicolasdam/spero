package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Ciudad;
import com.bolsadeideas.springboot.app.models.service.ICiudadService;

@Controller
@SessionAttributes("ciudad")
public class CiudadController {

	@Autowired
	private ICiudadService ciudadService;
	
	@RequestMapping(value = "/ciudad/ciudad-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		
		model.addAttribute("moduloMenu", "Administracion");
		model.addAttribute("opcionMenu", "Ciudades");
		model.addAttribute("titulo", "Lista de Ciudades");
		model.addAttribute("ciudades", ciudadService.findAll());
		
		return "ciudad/ciudad-listar";
	}
	
	@RequestMapping(value = "/ciudad/ciudad-form")
	public String crear(Map<String, Object> model) {

		Ciudad ciudad = new Ciudad();

		model.put("moduloMenu", "Administracion");
		model.put("opcionMenu", "Ciudades");
		model.put("ciudad", ciudad);
		model.put("titulo", "Registrar Ciudad");

		return "ciudad/ciudad-form";
	}
	
	@RequestMapping(value = "/ciudad/ciudad-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Ciudad ciudad = null;
		if (id > 0) {
			ciudad = ciudadService.buscarUno(id);
		} else {
			return "redirect:/ciudad/ciudad-listar";
		}
		
		model.put("moduloMenu", "Administracion");
		model.put("opcionMenu", "Ciudades");
		model.put("ciudad", ciudad);
		model.put("titulo", "Editar Ciudad");

		return "ciudad/ciudad-form";
	}
	
	@RequestMapping(value = "/ciudad/ciudad-form", method = RequestMethod.POST)
	public String guardar(@Valid Ciudad ciudad, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Ciudad");
			return "ciudad/ciudad-form";
		}
		model.addAttribute("moduloMenu", "Administracion");
		model.addAttribute("opcionMenu", "Ciudades");
		ciudadService.guardar(ciudad);
		status.setComplete();
		flash.addFlashAttribute("success","Ciudad actualizada con éxito");
		return "redirect:/ciudad/ciudad-listar";
	}
	
	@RequestMapping(value = "/ciudad/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash,  Model model) {
		if (id > 0) {
			ciudadService.eliminar(id);
		}
		model.addAttribute("moduloMenu", "Administracion");
		model.addAttribute("opcionMenu", "Ciudades");
		flash.addFlashAttribute("success","Ciudad eliminado con éxito");
		return "redirect:/ciudad/ciudad-listar";
	}
}

package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.dao.IUsuarioDao;
import com.bolsadeideas.springboot.app.models.entity.Maestra;
import com.bolsadeideas.springboot.app.models.entity.MaestrasLista;
import com.bolsadeideas.springboot.app.models.entity.Presentacion;
import com.bolsadeideas.springboot.app.models.entity.Proveedor;
import com.bolsadeideas.springboot.app.models.entity.UnidadMedida;
import com.bolsadeideas.springboot.app.models.entity.Usuario;
import com.bolsadeideas.springboot.app.models.service.IMaestraService;
import com.bolsadeideas.springboot.app.models.service.IPresentacionService;
import com.bolsadeideas.springboot.app.models.service.IProveedorService;
import com.bolsadeideas.springboot.app.models.service.IUnidadMedidaService;
import com.bolsadeideas.springboot.app.models.service.IUsuarioService;

@Controller
@SessionAttributes("maestra")
public class MaestraController {
	
	@Autowired
	IMaestraService maestraService;
	
	@Autowired
	IPresentacionService presentacionService;
	
	@Autowired
	IProveedorService proveedorService;
	
	@Autowired
	IUnidadMedidaService unidadMedidaService;
	
	@Autowired
	IUsuarioService usuarioService;
	
	@Autowired
	IUsuarioDao usuarioDao;
	
	@RequestMapping(value = "/maestra/maestra-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Maestra Productos");
		model.addAttribute("titulo", "Lista de Maestras");
		model.addAttribute("maestras", maestraService.findAll());
		return "maestra/maestra-listar";
	}
	
	@RequestMapping(value = "/maestra/maestra-form")
	public String crear(Map<String, Object> model, Authentication authentication) {

		List<Presentacion> presentacion = presentacionService.findAll();
		List<Proveedor> proveedor = proveedorService.findAll();
		List<UnidadMedida> unidadMedida = unidadMedidaService.findAll();
		
		Maestra maestra = new Maestra();
		
		String usuarioLog = authentication.getName();
		Usuario usuario = usuarioDao.findByUsuario(usuarioLog);
		maestra.setUsuario(usuario);

		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Maestra Productos");
		model.put("maestra", maestra);
		model.put("listPresentacion", presentacion);
		model.put("listProveedor", proveedor);
		model.put("listUnidadMedida", unidadMedida);
		model.put("titulo", "Registrar Maestra");

		return "maestra/maestra-form";
	}
	
	@RequestMapping(value = "/maestra/maestra-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		
		List<Presentacion> presentacion = presentacionService.findAll();
		List<Proveedor> proveedor = proveedorService.findAll();
		List<UnidadMedida> unidadMedida = unidadMedidaService.findAll();

		Maestra maestra = null;
		if (id > 0) {
			maestra = maestraService.buscarUno(id);
		} else {
			return "redirect:/maestra/maestra-listar";
		}
		
		model.put("moduloMenu", "Inventarios");
		model.put("opcionMenu", "Maestra Productos");
		model.put("maestra", maestra);
		model.put("listPresentacion", presentacion);
		model.put("listProveedor", proveedor);
		model.put("listUnidadMedida", unidadMedida);
		model.put("titulo", "Editar Maestra");

		return "maestra/maestra-form";
	}
	
	@RequestMapping(value = "/maestra/maestra-form", method = RequestMethod.POST)
	public String guardar(@Valid Maestra maestra, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {
		
		List<Presentacion> presentacion = presentacionService.findAll();
		List<Proveedor> proveedor = proveedorService.findAll();
		List<UnidadMedida> unidadMedida = unidadMedidaService.findAll();
		
		if (validacion.hasErrors()) {
			model.addAttribute("moduloMenu", "Inventarios");
			model.addAttribute("opcionMenu", "Maestra Productos");
			
			model.addAttribute("listPresentacion", presentacion);
			model.addAttribute("listProveedor", proveedor);
			model.addAttribute("listUnidadMedida", unidadMedida);
			model.addAttribute("titulo", "Registrar Maestra");
			return "maestra/maestra-form";
		}

		maestraService.guardar(maestra);
		
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Maestra Productos");
		status.setComplete();
		flash.addFlashAttribute("success","Maestra actualizada con éxito");
		return "redirect:/maestra/maestra-listar";
	}
	
	@RequestMapping(value = "/maestra/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		if (id > 0) {
			maestraService.eliminar(id);
		}
		
		model.addAttribute("moduloMenu", "Inventarios");
		model.addAttribute("opcionMenu", "Maestra Productos");
		flash.addFlashAttribute("success","Maestra eliminada con éxito");
		return "redirect:/maestra/maestra-listar";
	}
	
	@GetMapping(value = "/maestra/cargar_maestra/{term}", produces = { "application/json" })
	public @ResponseBody List<MaestrasLista> cargarProducto(@PathVariable String term) {
		// System.out.println ("el valor de la variable term es:" + term + " este");
		List<MaestrasLista> listMaestra = maestraService.buscarPorNombreMaestra(term);
		int size = listMaestra.size();
		System.out.println("su longitud es:" + size + " este");
		/*
		 * for(int i = 1; i <= size; i++) { String nn = listMaestra.get(i).getNombre();
		 * System.out.println ("Listado de maestras: " + nn); }
		 */
		// listMaestra = null;
		return maestraService.buscarPorNombreMaestra(term);
	}
	
}

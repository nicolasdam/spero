package com.bolsadeideas.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.DatosEmpresa;
import com.bolsadeideas.springboot.app.models.service.IDatosEmpresaService;

@Controller
@SessionAttributes("datosEmpresa")
public class DatosEmpresaController {
	
	@Autowired
	private IDatosEmpresaService datosEmpresaService;
	
	String moduloMenu = "Administracion";
	String opcionMenu = "Datos Empresa";
	
	@RequestMapping(value = "/datos-empresa/empresa-form")
	public String ver(Map<String, Object> model) {
		
		Long id = (long) 1;
		DatosEmpresa datosEmpresa = datosEmpresaService.buscarUno(id);
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("datosEmpresa", datosEmpresa);
		model.put("titulo", "Datos Empresa");
		
		return "datos-empresa/empresa-form";
		
	}
	
	@RequestMapping(value = "/datos-empresa/empresa-edit", method = RequestMethod.POST)
	public String editar(@Valid DatosEmpresa datosEmpresa, Model model, SessionStatus status) {

		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		model.addAttribute("datosEmpresa", datosEmpresa);

		return "datos-empresa/empresa-edit";
	}
	
	@RequestMapping(value = "/datos-empresa/empresa-guardar", method = RequestMethod.POST)
	public String guardar(@Valid DatosEmpresa datosEmpresa, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Datos Empresa");
			return "/datos-empresa/empresa-edit";
		}
		
		datosEmpresaService.guardar(datosEmpresa);
		status.setComplete();
		flash.addFlashAttribute("success","Datos Empresa actualizada con éxito");
		return "redirect:/index";
		//return "/datos-empresa/empresa-form";
	}

}

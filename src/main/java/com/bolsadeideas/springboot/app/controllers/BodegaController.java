package com.bolsadeideas.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Bodega;
import com.bolsadeideas.springboot.app.models.entity.Ciudad;
import com.bolsadeideas.springboot.app.models.service.IBodegaService;
import com.bolsadeideas.springboot.app.models.service.ICiudadService;

@Controller
@SessionAttributes("bodega")
public class BodegaController {

	@Autowired
	private IBodegaService bodegaService;
	
	@Autowired
	private ICiudadService ciudadService;
	
	String moduloMenu = "Inventarios";
	String opcionMenu = "Bodegas";
	
	@RequestMapping(value = "/bodega/bodega-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de bodegas");
		model.addAttribute("bodegas", bodegaService.findAllOrder());
		return "bodega/bodega-listar";
	}

	@RequestMapping(value = "/bodega/bodega-form")
	public String crear(Map<String, Object> model) {

		Long est = (long) 1;
		Bodega bodega = new Bodega();
		bodega.setEstado(est);
		
		List<Ciudad> listCiudades = ciudadService.findAll();
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		
		model.put("bodega", bodega);
		model.put("listCiudades", listCiudades);
		model.put("titulo", "Registrar Bodega");

		return "bodega/bodega-form";
	}

	@RequestMapping(value = "/bodega/bodega-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		List<Ciudad> listCiudades = ciudadService.findAll();
		
		Bodega bodega = null;
		if (id >= 0) {
			bodega = bodegaService.buscarUno(id);
		} else {
			return "redirect:/bodega/bodega-listar";
		}
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		
		model.put("bodega", bodega);
		model.put("listCiudades", listCiudades);
		model.put("titulo", "Editar Bodega");

		return "bodega/bodega-form";
	}

	@RequestMapping(value = "/bodega/bodega-form", method = RequestMethod.POST)
	public String guardar(@Valid Bodega bodega, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		List<Ciudad> listCiudades = ciudadService.findAll();
		
		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Bodega");
			model.addAttribute("listCiudades", listCiudades);
			
			return "bodega/bodega-form";
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		bodegaService.guardar(bodega);
		status.setComplete();
		flash.addFlashAttribute("success","Bodega actualizada con éxito");
		return "redirect:/bodega/bodega-listar";
	}

	@RequestMapping(value = "/bodega/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash,  Model model) {
		if (id > 0) {
			bodegaService.eliminar(id);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		
		flash.addFlashAttribute("success","Bodega eliminado con éxito");
		return "redirect:/bodega/bodega-listar";
	}
}

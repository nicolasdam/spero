package com.bolsadeideas.springboot.app.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bolsadeideas.springboot.app.models.entity.Nivel;
import com.bolsadeideas.springboot.app.models.entity.Opcion;
import com.bolsadeideas.springboot.app.models.entity.OpcionNivel;
import com.bolsadeideas.springboot.app.models.service.INivelService;
import com.bolsadeideas.springboot.app.models.service.IOpcionNivelService;
import com.bolsadeideas.springboot.app.models.service.IOpcionService;

@Controller
@SessionAttributes(value={"nivel", "opcionNivel"})
public class NivelController {
	
	@Autowired
	private INivelService nivelService;
	
	@Autowired
	private IOpcionNivelService opcionNivelService;
	
	@Autowired
	private IOpcionService opcionService;
	
	String moduloMenu = "Administracion";
	String opcionMenu = "Niveles";
	
	@RequestMapping(value = "/niveles/niveles-listar", method = RequestMethod.GET)
	public String listar(Model model) {
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		model.addAttribute("titulo", "Lista de Niveles");
		model.addAttribute("niveles", nivelService.findAll());
		return "niveles/niveles-listar";
	}
	
	@RequestMapping(value = "/niveles/niveles-form")
	public String crear(Map<String, Object> model) {

		Nivel nivel = new Nivel();

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("nivel", nivel);
		model.put("titulo", "Registrar Nivel");

		return "niveles/niveles-form";
	}
	
	@RequestMapping(value = "/niveles/niveles-form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Nivel nivel = null;
		if (id > 0) {
			nivel = nivelService.buscarUno(id);
		} else {
			return "redirect:/niveles/niveles-listar";
		}
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("nivel", nivel);
		model.put("titulo", "Editar Nivel");

		return "niveles/niveles-form";
	}
	
	@RequestMapping(value = "/niveles/niveles-form", method = RequestMethod.POST)
	public String guardar(@Valid Nivel nivel, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Modulos");
			return "niveles/niveles-form";
		}

		nivelService.guardar(nivel);
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		status.setComplete();
		flash.addFlashAttribute("success","Nivel actualizado con éxito");
		return "redirect:/niveles/niveles-listar";
	}
	
	@RequestMapping(value = "/niveles/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Model model) {
		
		if (id > 0) {
			nivelService.eliminar(id);
		}
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		flash.addFlashAttribute("success","Nivel eliminado con éxito");
		return "redirect:/niveles/niveles-listar";
	}
	
	/*
	 * Controles de opciones nivel
	 */
	@RequestMapping(value = "/niveles/opciones/opciones-nivel-list/{id}")
	public String opcionesNivel(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Nivel nivel = null;
		List<OpcionNivel> opcionesNivel = null;
				
		if (id > 0) {
			nivel = nivelService.buscarUno(id);
			
			opcionesNivel = opcionNivelService.findByNivel(nivel);

		} else {
			return "redirect:/niveles/niveles-listar";
		}
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("nivelPrincipal", nivel);
		model.put("opcionesNivel", opcionesNivel);
		model.put("titulo", "Lista Opciones Nivel");

		return "niveles/opciones/opciones-nivel-listar";
	}
	
	@RequestMapping(value = "/niveles/opciones/opciones-nivel-form/{id}")
	public String opcionesNivelCrear(@PathVariable(value = "id") Long id, Map<String, Object> model) {

		Nivel nivel = null;
		List<Opcion> opciones = opcionService.findAllOrder();
		List<Opcion> opcionesPen = new ArrayList<>();
		OpcionNivel opcionNivel = new OpcionNivel();
				
		if (id > 0) {
			nivel = nivelService.buscarUno(id);
		} else {
			return "redirect:/niveles/niveles-listar";
		}
		
		
		for (Opcion op : opciones) {
			OpcionNivel opcionNivelFind = new OpcionNivel();
			opcionNivelFind = opcionNivelService.findOpNivel(nivel, op);
			if (opcionNivelFind == null) {
				opcionesPen.add(op);
			}
		}

		opcionNivel.setNivel(nivel);
		
		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		model.put("nivel", nivel);
		model.put("opciones", opcionesPen);
		model.put("opcionNivel", opcionNivel);
		model.put("titulo", "Registrar Opcion por Nivel");

		return "niveles/opciones/opciones-nivel-form";
	}
	
	@RequestMapping(value = "/niveles/opciones/opciones-nivel-form-update", method = RequestMethod.POST)
	public String guardar(@Valid OpcionNivel opcionNivel, BindingResult validacion, Model model, RedirectAttributes flash, SessionStatus status) {

		if (validacion.hasErrors()) {
			model.addAttribute("titulo", "Registro de Modulos");
			return "niveles/opciones/opciones-nivel-form";
		}
		
		Long nivelId = opcionNivel.getNivel().getNivelId();

		opcionNivelService.guardar(opcionNivel);
		
		model.addAttribute("moduloMenu", moduloMenu);
		model.addAttribute("opcionMenu", opcionMenu);
		status.setComplete();
		flash.addFlashAttribute("success","Opcion Nivel actualizado con éxito");
		return "redirect:/niveles/opciones/opciones-nivel-list/" + nivelId;
	}
	
	@RequestMapping(value = "/niveles/opciones/eliminar/{id}")
	public String opcionesNivelEliminar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {
		
		Long nivelId = null;
		
		if (id > 0) {
			OpcionNivel opcionNivel = opcionNivelService.buscarUno(id);
			nivelId = opcionNivel.getNivel().getNivelId();
			opcionNivelService.eliminar(id);
		} else {
			return "redirect:/niveles/niveles-listar";
		}
		

		model.put("moduloMenu", moduloMenu);
		model.put("opcionMenu", opcionMenu);
		flash.addFlashAttribute("success","Opcion de Nivel eliminado con éxito");
		return "redirect:/niveles/opciones/opciones-nivel-list/" + nivelId;

	}

}
